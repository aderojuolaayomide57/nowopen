<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 8/16/2018
 * Time: 10:20 PM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | View All Products
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Products</a></li>
            <li class="active">View All Products</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Products</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <b hidden id="database">products</b>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Category</th>
                                        <th width="30%">feature</th>
                                        <th width="30%">Status</th>
                                        <th width="30%">Action</th>
                                        <th width="30%">comments</th>
                                        <th width="30%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{ $product->id }}</td>
                                            <td valign="middle">{{ $product->name }}</td>
                                            <td valign="middle">{{ $product->price }} </td>
                                            <td valign="middle">{{ $product->category }} </td>
                                            <td valign="middle">
                                                <button id="feature"  class="btn  {{ $product->feature == 0  ? 'btn-success' : 'btn-danger'}} feature"  data-id="{{ $product->id}}">
                                                    {{ $product->feature == 0  ? 'activate' : 'X'}}
                                                </button>
                                            </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $product->status > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $product->id}}">
                                                    {{ $product->status > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $product->id,'database' => 'products']) }}"
                                                   class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('productcomment',['id' => $product->id]) }}"  class="btn btn-default btn-sm">View ({{ $product->comment }})</a>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('viewoneproduct',['id' => $product->id]) }}"  class="btn btn-default btn-sm">View</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $products->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection

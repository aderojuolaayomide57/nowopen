<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 5/12/2019
 * Time: 2:09 PM
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="product-description-tab">
            <!-- tab bar -->
            <ul class="nav nav-tabs" role="tablist">
                <li id="tabdes1" class="active"><a href="#product-des" data-toggle="tab" id="tabdes">Product Description</a></li>
                <li id="tabrev1"><a href="#product-rev1" data-toggle="tab" id="tabrev">Comment</a></li>
            </ul>
            <!-- Tab Content -->
            <div class="tab-content">
                <div class="tab-pane active" id="product-des">
                    <p>{{ $product->description }}</p>
                </div>
                <div class="tab-pane" id="product-rev1">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="clear"></div>
                            <div class="single-post-comments">
                                <div class="comments-area">
                                    <div class="comments-heading">
                                        <h3>{{ count($comments) }} comments</h3>
                                    </div>
                                    <div class="comments-list">
                                        <ul>
                                            @forelse($comments as $comment)
                                            <li>
                                                <div class="comments-details">
                                                    <div class="comments-list-img">
                                                        <img style="height: 50px;width: 50px;" src="{{ asset('nowopen/img/avatar.png') }}" alt="post-author">
                                                    </div>
                                                    <div class="comments-content-wrap">
                                                                        <span>
                                                                            <b><a href="#">{{ $comment->name }}</a></b>
                                                                            <span class="post-time">October 6, 2014 at 1:38 am</span>
                                                                        </span>
                                                        <p>{{ $comment->message }}</p>
                                                    </div>
                                                </div>
                                            </li>
                                            @empty

                                            @endforelse

                                        </ul>
                                    </div>
                                </div>

                            </div><!-- single-blog end -->
                            <div class="blog-pagination">
                                <ul class="pagination">
                                    <li><a href="#">&lt;</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">&gt;</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="comment-respond">
                                <h3 class="comment-reply-title">Leave a Reply </h3>
                                @if(Session::has('commentsuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('commentsuccess') !!}</h4>
                                </div>
                                @endif
                                @if(Session::has('commentfailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('commentfailed') !!}</h4>
                                </div>
                                @endif
                                <span class="email-notes">Your email address will not be published. Required fields are marked *</span>
                                <form method="post" action="{{ url('/savecomment',['id' => $product->id]) }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <p>Name *</p>
                                            <input type="text"  name="name" placeholder="Full Name" value="{{ old('name') }}" required/>
                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                  </span>
                                            @endif
                                        </div>
                                        <div class="col-md-6{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <p>Email *</p>
                                            <input type="email"  name="email" placeholder="Email Address" value="{{ old('email') }}" required/>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                  </span>
                                            @endif
                                        </div>
                                        <div class="col-md-12 comment-form-comment{{ $errors->has('message') ? ' has-error' : '' }}">
                                            <p>Website</p>
                                            <textarea id="message" cols="30" name="message" rows="10">{{ old('message') }}</textarea>
                                            @if ($errors->has('message'))
                                            <span class="help-block">
                                                                    <strong>{{ $errors->first('message') }}</strong>
                                                                  </span>
                                            @endif
                                            <input type="submit" value="Post Comment" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

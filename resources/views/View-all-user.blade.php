@extends('layouts.slave')

@section('title')
    Home | View All User
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">User & Admin</a></li>
            <li class="active">View All User</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>User</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>gender</th>
                                        <th width="30%">Status</th>
                                        <th width="30%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    <b hidden id="database"> users</b>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td valign="middle">{{ $user->name }}</td>
                                            <td valign="middle">{{ $user->last_name }}</td>
                                            <td valign="middle">{{ $user->email }} </td>
                                            <td valign="middle">{{ $user->gender }} </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $user->active > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $user->id}}">
                                                    {{ $user->active > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $user->id,'database' => 'users']) }}"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $users->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection
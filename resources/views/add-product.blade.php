
@extends('layouts.master')

@section('title')
    Profile Product || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <style>
        .space{
            margin-left: 40px;
        }
        .bluecolor{
            color: #1c94c4;
        }
        .logo{
            visibility: hidden;
        }
    </style>
    <div style="background-color: #edf2f6;">
    @include('includes.profiletopheader')


        <div class="container">
            <br><br>

            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <div class="panel panel-default">
                        @if(Session::has('productsuccess'))
                            <div class="alert-box">
                                <h4 style="color: green;">{!! Session::get('productsuccess') !!}</h4>
                            </div>
                            <br>
                        @endif
                        @if(Session::has('productfailed'))
                            <div class="alert-box">
                                <h4 style="color: green;">{!! Session::get('productfailed') !!}</h4>
                            </div>
                            <br>
                        @endif
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ url('/saveproduct',['id' => $company->id]) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-5 space">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name" class="control-label">Product Name*</label>

                                            <div class="">
                                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                            <label for="price" class="control-label">Price *</label>

                                            <div class="">
                                                <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required autofocus>

                                                @if ($errors->has('price'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                                            <label for="category" class="control-label">Brand Category*</label>

                                            <div class="">

                                                <select name="category" class="form-control">
                                                    @foreach($cats as $cat)
                                                        <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                                                    @endforeach
                                                </select>

                                                @if ($errors->has('category'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 space">
                                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">

                                            <div class="">

                                                <label for="logo" >
                                                    <p >Upload brand logo*</p>
                                                    <img  id="uploadPreview1" src="{{ asset('/nowopen/img/avatar.png') }}" style="width: 300px; height: 300px; ">
                                                </label>

                                                <input type="file" name="logo" id="logo" class="logo" onchange="UploadPreviewlogo();">



                                                @if ($errors->has('logo'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-10 space">
                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="about" class="control-label">Product Description (50 words max)*</label>

                                            <div class="">
                                                <textarea id="description"  class="form-control" name="description"  required >{{ old('description') }}</textarea>

                                                @if ($errors->has('description'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <br>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-5">
                                        <button type="submit" class="btn btn-primary">
                                            Continue
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>


    <script>

        function UploadPreviewlogo() {
            var oFReader = new FileReader();

            oFReader.readAsDataURL(document.getElementById("logo").files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById("uploadPreview1").src = oFREvent.target.result;
            };

        }

    </script>
@endsection
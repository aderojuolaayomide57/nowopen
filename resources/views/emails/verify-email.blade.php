@component('mail::message')
# Introduction


Welcome to our Nowopen!

Thank you for registering and for showing interest in becoming a part of our community . 

Please click the button below to verify your email address

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

if you did not create an account, no further action is required

Regard,<br>
Nowopen

@endcomponent

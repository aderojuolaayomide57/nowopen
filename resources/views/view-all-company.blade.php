<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 8/16/2018
 * Time: 10:20 PM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | View All Companies
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Company</a></li>
            <li class="active">View All Companies</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Companies</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <b hidden id="database">companies</b>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Manager</th>
                                        <th>Company Name</th>
                                        <th>Category</th>
                                        <th>Service type</th>
                                        <th width="15%">Status</th>
                                        <th width="15%">Best</th>
                                        <th width="15%">Feature</th>
                                        <th width="15%">Action</th>
                                        <th width="15%">View</th>
                                        <th width="15%">Products</th>
                                        <th width="15%">Services</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @foreach($companies as $company)
                                        <tr>
                                            <td>{{ $company->id }}</td>
                                            <td valign="middle">{{ $company->manager }}</td>
                                            <td valign="middle">{{ $company->company_name }}</td>
                                            <td valign="middle">{{ $company->category }} </td>
                                            <td valign="middle">{{ $company->type }} </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $company->active > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $company->id}}">
                                                    {{ $company->active > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td valign="middle">
                                                <button id="best"  class="btn  {{ $company->best == 0  ? 'btn-success' : 'btn-danger'}} best"  data-id="{{ $company->id}}">
                                                    {{ $company->best == 0  ? 'activate' : 'x'}}
                                                </button>
                                            </td>
                                            <td valign="middle">
                                                <button id="feature"  class="btn  {{ $company->feature == 0  ? 'btn-success' : 'btn-danger'}} feature"  data-id="{{ $company->id}}">
                                                    {{ $company->feature == 0  ? 'activate' : 'x'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $company->id,'database' => 'companies']) }}"  class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('viewonecompany',['id' => $company->id]) }}"  class="btn btn-default btn-sm">View</a>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('allproducts',['id' => $company->id]) }}"  class="btn btn-default btn-sm">View {{ $company->product }}</a>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('allservices',['id' => $company->id]) }}"  class="btn btn-default btn-sm">View {{ $company->service }}</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $companies->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection

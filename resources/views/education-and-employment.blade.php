<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/9/2018
 * Time: 8:51 AM
 */
?>


@extends('layouts.master')

@section('title')
    Profile Settings || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
        @include('includes.profiletopheader')
        <style>
            .form-control{
                height: auto;
            }
            h6 {
                font-size: .1075rem;
                color: #515365;
                font-family: inherit;
                font-weight: 700;
                line-height: 1.3;
            }
            .card {
                position: relative;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid #e6ecf5;
                border-radius: .25rem
            }

            .card > hr {
                margin-right: 0
            }

            .card > .list-group:first-child .list-group-item:first-child {
                border-top-left-radius: .25rem;
                border-top-right-radius: .25rem
            }

            .card > .list-group:last-child .list-group-item:last-child {
                border-bottom-right-radius: .25rem;
                border-bottom-left-radius: .25rem
            }

            .card-header, .card-subtitle, .card-text:last-child {
                margin-bottom: 0
            }

            .card-link:hover {
                text-decoration: none
            }

            .card-link + .card-link {
                margin-left: 1.25rem
            }

            .card-header {
                border-bottom: 1px solid #e6ecf5;
                padding: .15rem 1.25rem;
                background-color: #fff;
                height: 50px;
                padding-bottom: 50px;
            }

            .card-header:first-child {
                border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
            }

            .card-header + .list-group .list-group-item:first-child {
                border-top: 0
            }

            .card-footer:last-child {
                border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
            }

            .alert, .btn .badge, .page-link {
                position: relative
            }

            .card-deck .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-deck .card {
                    display: -ms-flexbox;
                    display: flex;
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    margin-right: 15px;
                    margin-bottom: 0;
                    margin-left: 15px
                }
            }

            .card-group .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-group .card {
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    margin-bottom: 0
                }

                .card-group .card + .card {
                    margin-left: 0;
                    border-left: 0
                }

                .card-group .card:first-child {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0
                }

                .card-group .card:first-child .card-img-top {
                    border-top-right-radius: 0
                }

                .card-group .card:first-child .card-img-bottom {
                    border-bottom-right-radius: 0
                }

                .card-group .card:last-child {
                    border-top-left-radius: 0;
                    border-bottom-left-radius: 0
                }

                .card-group .card:last-child .card-img-top {
                    border-top-left-radius: 0
                }

                .card-group .card:last-child .card-img-bottom {
                    border-bottom-left-radius: 0
                }

                .card-group .card:only-child {
                    border-radius: .25rem
                }

                .card-group .card:only-child .card-img-top {
                    border-top-left-radius: .25rem;
                    border-top-right-radius: .25rem
                }

                .card-group .card:only-child .card-img-bottom {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem
                }

                .card-group .card:not(:first-child):not(:last-child):not(:only-child), .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-bottom, .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-top {
                    border-radius: 0
                }

                .card-columns .card {
                    display: inline-block;
                    width: 100%
                }
            }

            .card-columns .card {
                margin-bottom: .75rem
            }
        </style>


        <!-- Your Account Personal Information -->

        <div class="container">
            <div class="row">
                <div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Your Education History</h6>

                        </div>
                        <div class="ui-block-content">

                            @if(Session::has('educationsuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('educationsuccess') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('educationfailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('educationfailed') !!}</h4>
                                </div>
                            @endif
                            <!-- Education History Form -->
                            @if(!empty($education))
                                @if(Session::has('educationupdatesuccess'))
                                    <br>
                                    <div class="alert-box">
                                        <h4 style="color: green;">{!! Session::get('educationupdatesuccess') !!}</h4>
                                    </div>
                                @endif
                                @if(Session::has('educationupdatefailed'))
                                    <br>
                                    <div class="alert-box">
                                        <h4 style="color: red;">{!! Session::get('educationupdatefailed') !!}</h4>
                                    </div>
                                @endif
                                <form method="post" action="{{ url('/updateEducation',['id' => $education->id]) }}">
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('school') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="The New College of Design" name="school" type="text"
                                                       value="{{ $education->school1 }}" required autofocus>
                                            </div>
                                            @if ($errors->has('school'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('school') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('period') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="period"  type="text"
                                                       value="{{ $education->period1 }}" required >
                                            </div>
                                            @if ($errors->has('period'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('period') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('description') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Bachelor of Interactive Design in the New College." name="description">{{ $education->about1 }}</textarea>
                                            </div>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('school2') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Rembrandt Institute" name="school2"  type="text"
                                                       value="{{ $education->school2 }}">
                                            </div>
                                            @if ($errors->has('school2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('school2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('period2') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="2001 - 2006" name="period2"  type="text"
                                                       value="{{ $education->period2 }}">
                                            </div>
                                            @if ($errors->has('period2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('period2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('description2') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control"
                                                          placeholder="Five months Digital Illustration course. Professor: Leonardo Stagg." name="description2"  >{{ $education->about2 }}</textarea>
                                            </div>
                                            @if ($errors->has('description2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-secondary btn-lg full-width">Cancel</button>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-lg full-width">Update all Changes</button>
                                        </div>
                                    </div>
                                </form>
                            @else

                                <form method="post" action="{{ url('SaveEducation',['id' => $company->user_id]) }}">
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('school') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="The New College of Design" name="school" type="text"
                                                       value="{{ old('school') }}" required autofocus>
                                            </div>
                                            @if ($errors->has('school'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('school') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('period') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="period"  type="text"
                                                       value="{{ old('period') }}" required >
                                            </div>
                                            @if ($errors->has('period'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('period') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('description') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Bachelor of Interactive Design in the New College.It was a five years intensive career. Average: A+" name="description">{{ old('description') }}</textarea>
                                            </div>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('school2') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Rembrandt Institute" name="school2"  type="text"
                                                       value="{{ old('school2') }}">
                                            </div>
                                            @if ($errors->has('school2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('school2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('period2') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="2001 - 2006" name="period2"  type="text"
                                                       value="{{ old('period2') }}">
                                            </div>
                                            @if ($errors->has('period2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('period2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('description2') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Five months Digital Illustration course. Professor: Leonardo Stagg." name="desc2"   >
													{{ old('description2') }}</textarea>
                                            </div>
                                            @if ($errors->has('description2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-secondary btn-lg full-width">Cancel</button>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
                                        </div>
                                    </div>
                                </form>
                            @endif

                            <!-- ... end Education History Form -->
                        </div>
                    </div>
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Your Employement History</h6>
                        </div>
                        <div class="ui-block-content">
                            @if(Session::has('jobsuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('jobsuccess') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('jobfailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('jobfailed') !!}</h4>
                                </div>
                            @endif
                            <!-- Employement History Form -->
                            @if(!empty($employment))
                                @if(Session::has('employmentupdatesuccess'))
                                    <br>
                                    <div class="alert-box">
                                        <h4 style="color: green;">{!! Session::get('employmentupdatesuccess') !!}</h4>
                                    </div>
                                @endif
                                @if(Session::has('employmentupdatefailed'))
                                    <br>
                                    <div class="alert-box">
                                        <h4 style="color: red;">{!! Session::get('employmentupdatefailed') !!}</h4>
                                    </div>
                                @endif
                                <form method="post" action="{{ url('/UpdateJob',['id' => $employment->id]) }}">
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('job') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Digital Design Intern" name="job" type="text"
                                                       value="{{ $employment->employment1 }}"  required autofocus>
                                            </div>
                                            @if ($errors->has('job'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('job') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobperiod') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="jobperiod"  type="text"
                                                       value="{{ $employment->period1 }}" required >
                                            </div>
                                            @if ($errors->has('jobperiod'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobperiod') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobdescription') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Digital Design Intern for the “Multimedz” agency.
                                                    Was in charge of the communication with the clients." name="jobdescription">{{ $employment->about1 }}</textarea>
                                            </div>
                                            @if ($errors->has('jobdescription'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobdescription') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('job2') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Digital Design Intern" name="job2" type="text"
                                                       value="{{ $employment->employment2 }}" >
                                            </div>
                                            @if ($errors->has('job2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('job2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobperiod2') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="jobperiod2"  type="text"
                                                       value="{{ $employment->period2 }}"  >
                                            </div>
                                            @if ($errors->has('jobperiod2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobperiod2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobdescription2') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control"
                                                placeholder="Digital Design Intern for the “Multimedz” agency.
                                                    Was in charge of the communication with the clients." name="jobdescription2">{{ $employment->about2 }}</textarea>
                                            </div>
                                            @if ($errors->has('jobdescription2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobdescription2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-secondary btn-lg full-width">Cancel</button>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-lg full-width">Update all Changes</button>
                                        </div>
                                    </div>
                                </form>
                            @else

                                <form method="post" action="{{ url('/SaveJob',['id' => $company->user_id]) }}">
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('job') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Digital Design Intern" name="job" type="text"
                                                       value="{{ old('job') }}" required autofocus>
                                            </div>
                                            @if ($errors->has('job'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('job') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobperiod') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="jobperiod"  type="text"
                                                       value="{{ old('jobperiod') }}" required >
                                            </div>
                                            @if ($errors->has('jobperiod'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobperiod') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobdescription') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Digital Design Intern for the “Multimedz” agency.
                                                    Was in charge of the communication with the clients." name="jobdescription">
                                                    {{ old('jobdescription') }}
													</textarea>
                                            </div>
                                            @if ($errors->has('jobdescription'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobdescription') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('job2') ? ' has-error' : '' }}">
                                                <label class="control-label">Title or Place</label>
                                                <input class="form-control" placeholder="Digital Design Intern" name="job2" type="text"
                                                       value="{{ old('job2') }}" >
                                            </div>
                                            @if ($errors->has('job2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('job2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobperiod2') ? ' has-error' : '' }}">
                                                <label class="control-label">Period of Time</label>
                                                <input class="form-control" placeholder="" name="jobperiod2"  type="text"
                                                       value="{{ old('jobperiod2') }}"  >
                                            </div>
                                            @if ($errors->has('jobperiod2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobperiod2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group label-floating {{ $errors->has('jobdescription2') ? ' has-error' : '' }}">
                                                <label class="control-label">Description</label>
                                                <textarea class="form-control" placeholder="Digital Design Intern for the “Multimedz” agency.
                                                    Was in charge of the communication with the clients." name="jobdescription2">
                                                    {{ old('jobdescription2') }}
													</textarea>
                                            </div>
                                            @if ($errors->has('jobdescription2'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('jobdescription2') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-secondary btn-lg full-width">Cancel</button>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
                                        </div>
                                    </div>
                                </form>
                            @endif


                            <!-- ... end Employement History Form -->
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
                    <div class="ui-block">



                        <!-- Your Profile  -->

                    @include('includes.settings-tab')

                    <!-- ... end Your Profile  -->


                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Your Account Personal Information -->

        <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

            <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
            <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
            <div class="content-bg-wrap" >
                <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
            </div>
        </section>

        <!-- ... end Section Call To Action Animation -->

        <!-- Window-popup Update Header Photo -->


    </div>




@endsection

@section('profilefooter')
    @include('includes.profilefooter')
@endsection

<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 4/4/2019
 * Time: 6:11 AM
 */
?>

@extends('layouts.master')

@section('title')
    Search Result || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')



    <!-- Breadcurb Area -->
    <div class="breadcurb-area">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li>Search</li>
            </ul>
        </div>
    </div><!-- End Breadcurb Area -->
    <!-- Shop Product Area -->
    <div class="shop-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <!-- Shop Product Left -->
                    <div class="shop-product-left">
                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="layout-title">
                                <h2>Category</h2>
                            </div>
                            <div class="layout-list">
                                <ul>
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}"><i class="fa fa-plus-square-o"></i>{{ $cat->name }} <span>(15)</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div><!-- End Shop Layout Area -->
                    </div><!-- End Shop Product Left -->
                </div>
                <div class="col-md-9 col-sm-12">
                    <!-- Shop Product Right -->
                    <div class="shop-product-right">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="mega-menu-ul">
                                    <div class="row">
                                        @foreach($cats as $cat)
                                            <li class="col-lg-4"><a href="#"><h4>{{ $cat->name }}</h4></a>
                                                <ul>
                                                    @foreach($subcats as $subcat)

                                                        @if($subcat->cat_id == $cat->id)
                                                            <li style="padding: 5px"><a href="{{ url('/company',['id' => $cat->name]) }}"><i class="fa fa-chevron-circle-right"></i>{{ $subcat->name }}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div><!-- End Shop Product Left -->
                </div>
            </div>
        </div>
    </div><!-- End Shop Product Area -->


@endsection


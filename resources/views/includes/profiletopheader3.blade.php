<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/12/2018
 * Time: 2:45 AM
 */
?>
<div class="container" >
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">
                        <?php $filepath = 'uploads/header/'.$company->header;?>

                        @if(glob($filepath) && ($company->header != Null))
                            <img src="{{ asset($filepath) }}"  alt="image">
                        @else
                            <img  src="{{ asset('/profile/img/top-header1.jpg') }}" alt="author">
                        @endif
                    </div>
                    <div class="profile-section">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 ">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="{{ url('/myprofile',['id' => $company->id]) }}">About</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/services',['id' => $company->id]) }}">Services</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-5 ml-auto col-md-5 col-lg-offset-2">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="{{ url('/product',['id' => $company->id]) }}">Products</a>
                                    </li>
                                    <li>
                                        <a href="#">Vendors</a>
                                    </li>
                                    <li>
                                        <div class="more">
                                            <svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="#">Report Profile</a>
                                                </li>
                                                <li>
                                                    <a href="#">Block Profile</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="top-header-author">
                        <label for="upload" class="author-thumb">
                            <?php $filepath = 'uploads/picture/'.$company->logo;?>

                            @if(glob($filepath) && ($company->logo != Null))
                                <img id="uploadPreview"  src="{{ asset($filepath) }}"  alt="image">
                            @else
                                <img id="uploadPreview" src="{{ asset('/nowopen/img/avatar-company1.png') }}" alt="author">
                            @endif

                        </label>
                        <input  name="upload" id="upload" onchange="UploadPreview();" type="file" hidden>
                        <div class="author-content">
                            <a href="" class="h4 author-name">{{ $company->company_name }}</a>
                            <div class="country">{{ $company->city }}, {{ $company->state }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

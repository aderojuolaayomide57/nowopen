
<script src="{{ asset('/profile/js/jquery-3.2.1.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.appear.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('/profile/js/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('/profile/js/svgxuse.js') }}"></script>
<script src="{{ asset('/profile/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('/profile/js/Headroom.js') }}"></script>
<script src="{{ asset('/profile/js/velocity.js') }}"></script>
<script src="{{ asset('/profile/js/ScrollMagic.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('/profile/js/popper.min.js') }}"></script>
<script src="{{ asset('/profile/js/material.min.js') }}"></script>
<script src="{{ asset('/profile/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('/profile/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('/profile/js/selectize.js') }}"></script>
<script src="{{ asset('/profile/js/swiper.jquery.js') }}"></script>
<script src="{{ asset('/profile/js/moment.js') }}"></script>
<script src="{{ asset('/profile/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/profile/js/simplecalendar.js') }}"></script>
<script src="{{ asset('/profile/js/fullcalendar.js') }}"></script>
<script src="{{ asset('/profile/js/isotope.pkgd.js') }}"></script>
<script src="{{ asset('/profile/js/ajax-pagination.js') }}"></script>
<script src="{{ asset('/profile/js/Chart.js') }}"></script>
<script src="{{ asset('/profile/js/chartjs-plugin-deferred.js') }}"></script>
<script src="{{ asset('/profile/js/circle-progress.js') }}"></script>
<script src="{{ asset('/profile/js/loader.js') }}"></script>
<script src="{{ asset('/profile/js/run-chart.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('/profile/js/jquery.gifplayer.js') }}"></script>
<script src="{{ asset('/profile/js/mediaelement-and-player.js') }}"></script>
<script src="{{ asset('/profile/js/mediaelement-playlist-plugin.min.js') }}"></script>

<script src="{{ asset('/profile/js/base-init.js') }}"></script>

<script src="{{ asset('/profile/Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.js"></script>

<script>
    $('#category').on('change',function(e){
        var category = $('#category option:selected').attr('value');
        $.ajax({
            data: {category : category ,_token: '{{ csrf_token() }}'},
            url: '{{ route('listsubcat') }}',
            method: 'POST',
            success: function(data){
                $('#type').find('option').remove().end();
                $('#type').append($('<option>').text('--- select service type --').attr('value', ''));
                $.each(data, function(i, value) {
                    $('#type').append($('<option>').text(value.name).attr('value', value.name));
                });

            },
            error: {}
        });

    });
</script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    } );
    $('#timepicker').timepicker({
        'minTime': '8:00am',
        'maxTime': '5:00pm',
        'showDuration': true
    });
    $('#timepicker1').timepicker({
        'minTime': '8:00am',
        'maxTime': '5:00pm',
        'showDuration': true
    });
</script>
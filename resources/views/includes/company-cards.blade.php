<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/22/2018
 * Time: 1:21 PM
 */
?>

<div class="ui-block">

    <!-- Friend Item -->

    <div class="friend-item">
        <div class="friend-header-thumb">
            <?php $filepath = 'uploads/header/'.$company->header;?>

            @if(glob($filepath) && ($company->header != Null))
                <img style="width: 246px; height: 93px"   src="{{ asset($filepath) }}"  alt="friend">
            @else
                <img src="{{ asset('/nowopen1/img/friend1.jpg') }}" alt="friend">
            @endif
        </div>

        <div class="friend-item-content" style="padding: 0 5px 5px;">

            <div class="more">
                <a href=""><i class="fa fa-info-circle"></i></a>
            </div>
            <div class="friend-avatar">
                <div class="author-thumb">
                    <?php $filepath = 'uploads/picture/'.$company->logo;?>

                    @if(glob($filepath) && ($company->logo != Null))
                        <img  style=" height: 100px; width: 100px;"  src="{{ asset($filepath) }}"  alt="author">
                    @else
                        <img style=" height: 100px; width: 100px;"   src="{{ asset('/nowopen/img/avatar-company1.png') }}"
                             alt="author">
                    @endif

                </div>
                <div class="author-content">
                    <a href="#" class="h5 author-name">{{ $company->company_name }}</a>
                    <div class="country">{{ $company->city }},{{ $company->state }},Nigeria.</div>
                </div>
            </div>

            <div class="swiper-container" data-slide="fade">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="friend-count" data-swiper-parallax="-500">
                            <a href="#" class="friend-count-item">
                                <div class="h6">{{ $company->product }}</div>
                                <div class="title">Products</div>
                            </a>
                            <a href="#" class="friend-count-item">
                                <div class="h6"><i style="color: yellow" class="fa fa-star"></i>
                                    <i style="color: yellow" class="fa fa-star"></i>
                                    <i style="color: yellow" class="fa fa-star"></i>
                                   </div>
                                <div class="title">Ratings</div>
                            </a>
                            <a href="#" class="friend-count-item">
                                <div class="h6"><i class="fa fa-clock-o"></i></div>
                                <div class="title">{{ str_limit($company->opening,1,'am') }} - {{ str_limit($company->closing,1,'pm') }}</div>
                            </a>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- ... end Friend Item -->
</div>
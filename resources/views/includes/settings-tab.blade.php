<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/9/2018
 * Time: 8:52 AM
 */
?>

<div class="your-profile">
    <div class="ui-block-title ui-block-title-small">
        <h6 class="title">Your PROFILE</h6>
    </div>

    <div id="accordion" role="tablist" aria-multiselectable="true">
        <div class="card">
            <div class="card-header" role="tab" id="headingOne">
                <h6 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Profile Settings
                        <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                    </a>
                </h6>
            </div>

            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                <ul class="your-profile-menu">
                    <li>
                        <a href="{{ url('/myaccount') }}">Business Information</a>
                    </li><!--
									<li>
										<a href="29-YourAccount-AccountSettings.html">Account Settings</a>
									</li> -->
                    <a href="{{ url('/changepassword',['id' => Auth::user()->id]) }}">Change Password</a>
                    <li>
                        <a href="#">Hobbies and Interests</a>
                    </li>

                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-header" role="tab" id="headingTwo">
                <h6 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        Education and Employement
                        <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                    </a>
                </h6>
            </div>
            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                <ul class="your-profile-menu">
                    <li>
                        <a href="{{ url('/educationemployment',['id' => $company->id]) }}">Education and Employement</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-header" role="tab" id="headingThree">
                <h6 class="mb-0">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        My Companies ( {{ count($company1s) }} )
                        <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                    </a>
                </h6>
            </div>
            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                <ul class="your-profile-menu">
                        @forelse($company1s as $company)
                            <li>
                                <a href="{{ url('/myprofile',['id' => $company->id]) }}">{{ $company->company_name }}</a>
                            </li>
                        @empty
                        <li>

                        </li>
                        @endforelse

                </ul>
            </div>
        </div>
    </div>

</div>

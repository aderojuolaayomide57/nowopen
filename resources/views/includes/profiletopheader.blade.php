@if(!empty(Auth::user()->id) && Auth::user()->id == $company->user_id)
    <div class="" style="background-color: #edf2f6;">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ui-block">
                    <div class="top-header">
                        <div class="top-header-thumb">
                            <?php $filepath = 'uploads/header/'.$company->header;?>

                            @if(glob($filepath) && ($company->header != Null))
                                <img id="uploadPreview2"  src="{{ asset($filepath) }}"  alt="image"  style="height: 150px">
                            @else
                                <img id="uploadPreview2" src="{{ asset('/profile/img/top-header1.jpg') }}" alt="author"  style="height: 150px">
                            @endif
                        </div>
                        <div class="profile-section">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 ">
                                    <ul class="profile-menu">
                                        <li>
                                            <a href="{{ url('/myprofile',['id' => $company->id]) }}">About</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/services',['id' => $company->id]) }}">Services</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-5 ml-auto col-md-5 col-lg-offset-2">
                                    <ul class="profile-menu">
                                        <li>
                                            <a href="{{ url('/product',['id' => $company->id]) }}">Products</a>
                                        </li>
                                        <li>
                                            <a href="#">Vendors</a>
                                        </li>
                                        <li>
                                            <div class="more">
                                                <svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                                                <ul class="more-dropdown more-with-triangle">
                                                    <li>
                                                        <a href="#">Report Profile</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Block Profile</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="control-block-button">

                                <div class="btn btn-control bg-primary more">
                                    <svg class="olymp-settings-icon"><use xlink:href="{{ asset('/profile/svg-icons/sprites/icons.svg#olymp-settings-icon')}}"></use></svg>
                                    <i class="fa fa-dashboard-o"></i>
                                    <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                        <li>
                                            <b style=" padding: 7px 0;font-size: 12px;color: #515365;display: block;font-weight: 700;">
                                                <label for="upload" class="author-thumb">Update Profile Photo</label>
                                                <input  name="upload" id="upload" onchange="UploadPreview();" type="file" hidden>
                                            </b>
                                        </li>
                                        <li>
                                            <b style=" padding: 7px 0;font-size: 12px;color: #515365;display: block;font-weight: 700;">
                                                <label for="upload2" class="author-thumb">Update Header Photo</label>
                                                <input  name="upload2" id="upload2" onchange="UploadPreview2();" type="file" hidden>
                                            </b>
                                        </li>
                                        <li>
                                            <a href="{{ url('/settings',['id' => $company->id]) }}">Account Settings</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-header-author">
                            <label for="upload" class="author-thumb">
                                <?php $filepath = 'uploads/picture/'.$company->logo;?>

                                @if(glob($filepath) && ($company->logo != Null))
                                    <img id="uploadPreview"  src="{{ asset($filepath) }}"  alt="image">
                                @else
                                    <img id="uploadPreview" src="{{ asset('/nowopen/img/avatar-company1.png') }}" alt="author">
                                @endif

                            </label>
                            <input  name="upload" id="upload" onchange="UploadPreview();" type="file" hidden>
                            <div class="author-content">
                                <a href="#" class="h4 author-name">{{ $company->company_name }}</a>
                                <div class="country">{{ $company->city }}, {{ $company->state }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="" style="background-color: #edf2f6;">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ui-block">
                    <div class="top-header">
                        <div class="top-header-thumb">
                            <?php $filepath = 'uploads/header/'.$company->header;?>

                            @if(glob($filepath) && ($company->header != Null))
                                <img src="{{ asset($filepath) }}"  alt="image" style="height: 150px">
                            @else
                                <img  src="{{ asset('/profile/img/top-header1.jpg') }}" alt="author" style="height: 150px">
                            @endif
                        </div>
                        <div class="profile-section">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 ">
                                    <ul class="profile-menu">
                                        <li>
                                            <a href="{{ url('/myprofile',['id' => $company->id]) }}">About</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/services',['id' => $company->id]) }}">Services</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-5 ml-auto col-md-5 col-lg-offset-2">
                                    <ul class="profile-menu">
                                        <li>
                                            <a href="{{ url('/product',['id' => $company->id]) }}">Products</a>
                                        </li>
                                        <li>
                                            <a href="#">Vendors</a>
                                        </li>
                                        <li>
                                            <div class="more">
                                                <svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('/profile/svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg>
                                                <ul class="more-dropdown more-with-triangle">
                                                    <li>
                                                        <a href="#">Report Profile</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Block Profile</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-header-author">
                            <label for="upload" class="author-thumb">
                                <?php $filepath = 'uploads/picture/'.$company->logo;?>

                                @if(glob($filepath) && ($company->logo != Null))
                                    <img id="uploadPreview"  src="{{ asset($filepath) }}"  alt="image">
                                @else
                                    <img id="uploadPreview" src="{{ asset('/nowopen/img/avatar-company1.png') }}" alt="author">
                                @endif

                            </label>
                            <div class="author-content">
                                <a href="#" class="h4 author-name">{{ $company->company_name }}</a>
                                <div class="country">{{ $company->city }}, {{ $company->state }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif




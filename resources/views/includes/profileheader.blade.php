<link rel="stylesheet" type="text/css" href="{{ asset('/profile/Bootstrap/dist/css/bootstrap-reboot.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/profile/Bootstrap/dist/css/bootstrap-grid.css') }}">

<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('/profile/css/main.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/profile/css/fonts.min.css') }}">

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.css">


<!-- Main Font -->
<script src="{{ asset('/profile/js/webfontloader.min.js') }}"></script>
<script>
    WebFont.load({
        google: {
            families: ['Roboto:300,400,500,700:latin']
        }
    });
</script>
<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 2/17/2019
 * Time: 9:52 AM
 */
?>
<div class="ui-block">

    <!-- Friend Item -->

    <div class="friend-item">
        <div class="friend-header-thumb">
            <?php $filepath = 'uploads/header/'.$company->header;?>

            @if(glob($filepath) && ($company->header != Null))
                <img style="width: 246px; height: 170px"   src="{{ asset($filepath) }}"  alt="friend">
            @else
                <img style="width: 246px; height: 170px" src="{{ asset('/nowopen1/img/friend1.jpg') }}" alt="friend">
            @endif
        </div>

        <div class="friend-item-content" style="padding: 0 0 0;margin-bottom: -20px">
            <div class="friend-avatar">

                <div class="author-content" style="margin-top: 150px;text-align: left; padding-left: 20px; padding-right: 20px">
                    <a href="{{ url('/myprofile',['id' =>  $company->id]) }}" class="h5" style=""><h4 style="font-weight: bold;font-size: 12px">{{ ucfirst($company->company_name) }}</h4></a>
                    <div class="country" style="font-size: 10px;">{{ $company->city }} {{ $company->state }}, Nigeria.</div>

                    <div class="country" style="margin-top: 10px; border-bottom: rgba(45,49,41,0.65) 0.04em solid">
                        <i class="" style="border-bottom: rgba(45,49,41,0.65) 0.04em solid;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                        <h4 style="padding-top: 10px;font-size: 9px">{{ $company->type }}</h4>
                    </div>
                    <div class="country" style="margin-top: 5px;">
                        <div class="row">
                            <div class="col-lg-7">
                                <div class="title" style="font-size: 10px"><i class="fa fa-clock-o"></i>
                                    {{ str_limit($company->opening,1,'am') }} - {{ str_limit($company->closing,1,'pm') }}</div>
                            </div>
                            <div class="col-lg-3 col-lg-offset-2">
                                <div class="title">
                                    <img style="height: 15px; width: 15px;" src="{{ asset('/nowopen/img/logo/fav.png') }}" alt="author"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ... end Friend Item -->
</div>

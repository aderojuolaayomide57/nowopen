<nav id="menu" data-search="close">
    <ul>
        <li><a href="{{ url('/admins') }}"><span><i class="icon  fa fa-laptop"></i> Dashboard</span></a></li>
        <li><span><i class="icon  fa fa-th-list"></i> Users & Admin</span>
            <ul>
                <li ><a href="{{ url('/adminreg') }}">Add Admin</a></li>
                <li><a href="{{ url('/viewalladmin') }}">View all Admin</a></li>
                <li><a href="{{ url('/viewalluser') }}"> View All User </a></li>
            </ul>
        </li>
        <li><span><i class="icon  fa fa-th-list"></i> Category</span>
            <ul>
                <li ><a href="{{ url('/category') }}">Add Category</a></li>
                <li><a href="{{ url('/subcategory') }}">Add Sub Category</a></li>
                <li><a href="{{ url('/viewallcategory') }}"> View All Category </a></li>
            </ul>
        </li>
        <li><span><i class="icon  fa fa-briefcase"></i> Company</span>
            <ul>
                <li><a href="{{ url('/viewallcompany') }}"> View All Company</a></li>
            </ul>
        </li>
        <li><span><i class="icon  fa fa-bar-chart-o"></i> Advert</span>
            <ul>
                <li><a href="{{ url('/addadvert') }}"> Add Advert Image</a></li>
                <li><a href="{{ url('/viewalladvert') }}"> View All Advert </a></li>
            </ul>
        </li>

    </ul>
</nav>
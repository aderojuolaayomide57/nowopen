<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 2/16/2019
 * Time: 7:30 PM
 */
?>
<!-- Header Area -->
<div class="" >
    <!-- Header top bar -->
    <!-- Header bottom -->
    <div class="header-bottom">
        <div class="">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-lg-offset-4">
                    <!-- Header logo -->
                    <div class="header-logo" style="padding-bottom: 10px;padding-top: 10px">
                        <a href="{{ url('/home') }}"><img style="height: 60px" src="{{ asset('/nowopen/img/logo/logo.png') }}" alt="logo"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><!-- End Header bottom -->
<!-- End Header Area -->
<!-- Main Menu Area -->
<div class="main-menu-area" style="border-bottom: 1px solid rgba(213,213,213,0.48)">
    <div class="">
        <div class="row">
            <div class="col-md-12">
                <!-- Main Menu -->

                <div class="main-menu hidden-sm hidden-xs">
                    <div class="row">
                        <div class="col-md-8 col-lg-offset-2">
                            <nav >
                                <ul class="main-ul" style="margin-bottom: 0em;">
                                    <li class="sub-menu-li"><a href="{{ url('/home') }}" class="new-arrivals active">Home</a>

                                    </li>
                                    <li class="sub-menu-li">
                                        <a href="{{ url('/categories') }}" class="new-arrivals">Categories</a>
                                        <!-- Sub Menu -->
                                    </li>
                                    <li ><a href="{{ url('/location') }}">Location</a></li>
                                    <li><a href="#">ADVERTISE</a></li>
                                    <li><a href="#">AGENCY</a></li>
                                    <li ><a href="{{ url('/contact') }}" class="new-arrivals">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-2 col-lg-offset-0">
                            <nav >
                                <ul class="main-ul" style="margin-bottom: 0em;">
                                    <li class="" ><a style="padding: 10px" href="{{ url('/home') }}" class="">
                                            <i style="font-size: 18px; margin-left: 0px; padding-top: 12px" class="fa fa-search"></i></a></li>
                                    <li class="" ><a style="padding:10px" href="{{ url('/home') }}" class="">
                                            <i style="font-size: 18px; margin-left: 0px; padding-top: 12px" class="fa fa-commenting-o"></i></a></li>
                                    <li class="" ><a style="padding:10px " href="{{ url('/home') }}" class="">
                                            <i style="font-size: 18px; margin-left: 0px; padding-top: 12px" class="fa fa-bell-o"></i></a></li>
                                    @if(!empty(Auth::user()->id))
                                    <li class="" >
                                        <a style="padding:10px " href="#" >
                                            <div class="author-thumb">
                                                    <img style="height: 30px; width: 30px;" src="{{ asset('/nowopen/img/avatar-company1.png') }}" alt="author">
                                            </div>
                                            <ul class="sub-menu">
                                                <li class="sub-menu-li" style="margin-top: -35px"><a href="{{ url('/myprofile') }}">My Profile</a></li>
                                                <li class="sub-menu-li" ><a href="{{ url('/regcompany')}}">Register Company</a></li>
                                                <li class="sub-menu-li">
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                   @else
                                        <li class="" >
                                            <a style="padding:10px " href="#" >
                                                <div class="author-thumb">
                                                    <img style="height: 30px; width: 30px;" src="{{ asset('/nowopen/img/avatar-company1.png') }}" alt="author">
                                                </div>
                                                <ul class="sub-menu">
                                                    <li class="sub-menu-li" style="margin-top: -35px"><a href="{{ url('/') }}">Login/Register</a></li>
                                                </ul>
                                            </a>
                                        </li>
                                   @endif
                                </ul>
                            </nav>
                        </div>
                    </div>

                </div><!-- End Main Menu -->
                <!-- Start Mobile Menu -->
                <div class="mobile-menu hidden-md hidden-lg">
                    <nav>
                        <ul>
                            <li class=""><a href="{{ url('/home') }}">Home</a></li>
                            <li class=""><a href="{{ url('/about') }}">About Us</a></li>
                            <li class=""><a href="{{ url('/contact') }}">Contact Us</a></li>
                            <li class="">
                                <a href="">Category</a>
                                <ul class="">
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}">{{ $cat->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="">Location</a>
                                <ul class="">
                                    <?php $i=1; ?>
                                    @foreach($locals as $local)
                                        @if($i==1 || $i==6 || $i==12 )
                                            <li><a href="#">Location</a>
                                                <ul>
                                                    @endif
                                                    <li><a href="{{ url('/company',['id' => $local->name]) }}">{{ $local->name }}</a></li>
                                                    @if($i==5 || $i==11 )
                                                </ul>
                                            </li>
                                        @endif
                                        <?php $i++; ?>
                                    @endforeach

                                </ul>
                            </li>
                            <li><a href="">Services</a>
                                <ul class="">
                                    @foreach($cats as $cat)
                                        <li><a href="#">{{ $cat->name }}</a>
                                            <ul>
                                                @foreach($subcats as $subcat)

                                                    @if($subcat->cat_id == $cat->id)
                                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}">{{ $subcat->name }}</a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div><!-- End Mobile Menu -->
            </div>
        </div>
    </div>
</div><!-- End Main Menu Area -->

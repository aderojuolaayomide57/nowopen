<?php
/**
 * Created by PhpStorm.
 * User: venoworks
 * Date: 6/9/2018
 * Time: 9:01 PM
 */
?>
<!-- Header Area -->
<div class="header-area">
    <!-- Header top bar -->
    <!-- Header bottom -->
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <!-- Header logo -->
                    <div class="header-logo">
                        <a href="{{ url('/home') }}"><img src="{{ asset('/nowopen/img/logo/logo.png') }}" alt="logo"></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><!-- End Header bottom -->
<!-- End Header Area -->
<!-- Main Menu Area -->
<div class="main-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Main Menu -->
                <div class="main-menu hidden-sm hidden-xs">
                    <nav >
                        <ul class="main-ul">
                            <li class="sub-menu-li"><a href="{{ url('/home') }}" class="active">Home</a>

                            </li>
                            <li class="sub-menu-li">
                                <a href="#" class="new-arrivals">Categories<i class="fa fa-chevron-down"></i></a>
                                <!-- Sub Menu -->
                                <ul class="sub-menu">
                                    @foreach($cats as $cat)
                                        <li>
                                        <a href="{{ url('/company',['id' => $cat->name]) }}">
                                            <i class="fa fa-chevron-circle-right"></i> <span>{{ $cat->name }}</span></a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="#">Location<i class="fa fa-chevron-down"></i></a>
                                <ul class="mega-menu-ul">
                                    <li>

                                        <?php $i=1; ?>
                                        <div class="mega-menu">
                                            @foreach($locals as $local)
                                                    @if($i==1 || $i==7 || $i==12 )
                                                        <div class="single-mega-menu">
                                                            <h2><a href="#">Location</a></h2>
                                                    @endif
                                                            <a href="{{ url('/company',['id' => $local->name]) }}"><i class="fa fa-chevron-circle-right"></i>
                                                                <span>{{ $local->name }}</span></a>
                                                    @if($i==6 || $i==11 )
                                                        </div>
                                                    @endif
                                                    <?php $i++; ?>
                                            @endforeach
                                        <!-- Mega Menu -->
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="">Services<i class="fa fa-chevron-down"></i></a>
                                <ul class="mega-menu-ul">
                                    <li>
                                        <!-- Mega Menu -->
                                        <div class="mega-menu">

                                            @foreach($cats as $cat)
                                                <div class="single-mega-menu">
                                                    <h2><a href="">{{ $cat->name }}</a></h2>
                                                    <?php $i =0; ?>
                                                @foreach($subcats as $subcat)

                                                        @if($subcat->cat_id == $cat->id && $i < 10)
                                                            <a href="{{ url('/company',['id' => $subcat->name]) }}">
                                                                <i class="fa fa-chevron-circle-right"></i> <span>{{ $subcat->name }}</span></a>
                                                            <?php $i++; ?>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            @endforeach

                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#">About Us</a></li>
                            <li ><a href="{{ url('/contact') }}" class="new-arrivals">Contact Us</a></li>
                        </ul>
                    </nav>
                </div><!-- End Main Menu -->
                <!-- Start Mobile Menu -->
                <div class="mobile-menu hidden-md hidden-lg">
                    <nav>
                        <ul>
                            <li class=""><a href="{{ url('/home') }}">Home</a></li>
                            <li class=""><a href="{{ url('/about') }}">About Us</a></li>
                            <li class=""><a href="{{ url('/contact') }}">Contact Us</a></li>
                            <li class="">
                                <a href="">Category</a>
                                <ul class="">
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}">{{ $cat->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li><a href="">Location</a>
                                <ul class="">
                                    <?php $i=1; ?>
                                        @foreach($locals as $local)
                                            @if($i==1 || $i==6 || $i==12 )
                                                <li><a href="#">Location</a>
                                                    <ul>
                                                    @endif
                                                        <li><a href="{{ url('/company',['id' => $local->name]) }}">{{ $local->name }}</a></li>
                                                    @if($i==5 || $i==11 )
                                                    </ul>
                                                </li>
                                            @endif
                                            <?php $i++; ?>
                                        @endforeach

                                </ul>
                            </li>
                            <li><a href="">Services</a>
                                <ul class="">
                                    @foreach($cats as $cat)
                                        <li><a href="#">{{ $cat->name }}</a>
                                            <ul>
                                            @foreach($subcats as $subcat)

                                                @if($subcat->cat_id == $cat->id)
                                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}">{{ $subcat->name }}</a></li>
                                                @endif
                                            @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div><!-- End Mobile Menu -->
            </div>
        </div>
    </div>
</div><!-- End Main Menu Area -->

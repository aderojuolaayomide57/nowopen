<div class="sidebar_widget">
    <div class="widget_heading">
        <h5>Coming Events</h5>
    </div>
    <div class="popular_post">
        <ul>
            @foreach($comings as $coming)
                <li>
                    <div class="popular_post_img">
                        <a href="{{ route('newseventdetails',['id' => $coming->id]) }}">
                            @if(Storage::disk('local')->has($coming->image))
                                <img  src="{{ route('AllImage', ['filename' => $coming->image]) }}"  class="img-responsive" alt="image">
                            @endif
                        </a>
                    </div>
                    <div class="popular_post_title">
                        <a href="{{ route('newseventdetails',['id' => $coming->id]) }}">{{ $coming->title }}</a>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
</div>
<div class="sidebar_widget">
    <div class="widget_heading">
        <h5>Categories</h5>
    </div>
    <div class="categories_list">
        <ul>
            <li><a href="{{ route('viewcarbytag',['type' => 'Jeep']) }}">Jeep</a></li>
            <li><a href="{{ route('viewcarbytag',['type' => 'Cars']) }}">Cars</a></li>
            <li><a href="{{ route('viewcarbytag',['type' => 'Trucks']) }}">Trucks</a></li>
            <li><a href="{{ route('viewcarbytag',['type' => 'Van']) }}">Van</a></li>
        </ul>
    </div>
</div>

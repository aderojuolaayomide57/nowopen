<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 2/17/2019
 * Time: 9:52 AM
 */
?>
 <template x-for="(company, index) in Object.values(companys)" :key="company.id">
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="ui-block">

            <!-- Friend Item -->

            <div class="friend-item">
                <div class="friend-header-thumb">
                    <template x-if="company.header != null">
                        <img style="width: 246px; height: 170px"  :src="company.image" :alt="company.name" />
                    </template>
                    <template x-if="company.header == null">
                        <img style="width: 246px; height: 170px" src="{{ asset('/nowopen1/img/friend1.jpg') }}" alt="friend">
                    </template>
                    
                </div>

                <div class="friend-item-content" style="padding: 0 0 0;margin-bottom: -20px">
                    <div class="friend-avatar">

                        <div class="author-content" style="margin-top: 150px;text-align: left; padding-left: 20px; padding-right: 20px">
                            <a :href="company.url" class="h5" style=""><h4 style="font-weight: bold;font-size: 12px" x-text="company.company_name"></h4></a>
                            <div class="country" style="font-size: 10px;" x-text="[company.city +' '+ company.state + ', ' + 'Nigeria.']"></div>

                            <div class="country" style="margin-top: 10px; border-bottom: rgba(45,49,41,0.65) 0.04em solid">
                                <i class="" style="border-bottom: rgba(45,49,41,0.65) 0.04em solid;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                <h4 style="padding-top: 10px;font-size: 9px" x-text="company.type"></h4>
                            </div>
                            <div class="country" style="margin-top: 5px;">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="title" style="font-size: 10px">
                                        <i class="fa fa-clock-o"></i>&nbsp;&nbsp;
                                        <p style="font-size: 9px; display: inline;" x-html="[company.opening + ' - ' + company.closing ]"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-lg-offset-1">
                                        <div class="title">
                                            <img style="height: 15px; width: 15px;" src="{{ asset('/nowopen/img/logo/fav.png') }}" alt="author"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ... end Friend Item -->
        </div>
        </div>
  
</template>        

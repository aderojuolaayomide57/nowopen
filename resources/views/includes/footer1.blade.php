<?php
/**
 * Created by PhpStorm.
 * User: venoworks
 * Date: 6/9/2018
 * Time: 9:07 PM
 */
?>
<style>
    .footer {
        width: calc(100% - 144px);
        margin: 0 auto;
        padding: 30px 25px 0;
        background-color: #fff;
        position: relative
    }

    .footer .widget .title {
        margin-bottom: 0;
    }

    .footer.footer-full-width {
        width: 100%
    }



    .footer--dark .socials i, .footer--dark .w-about .logo, .footer--dark .w-about .logo .logo-title, .footer--dark .w-about .logo:hover, .footer--dark .widget .title {
        color: #fff
    }

    .footer--dark .socials i:hover {
        color: #007bff
    }

    .sub-footer-copyright {
        padding: 25px 0;
        text-align: center;
        border-top: 1px solid #e6ecf5;
        margin-top: 20px
    }

    .sub-footer-copyright span {
        font-size: 12px
    }

    .sub-footer-copyright a {
        color: #007bff;
        opacity: .7;
        font-weight: 500
    }

    .sub-footer-copyright a:hover {
        opacity: 1
    }

    @media (max-width: 1024px) {
        .footer .widget {
            margin-bottom: 10px
        }

        .footer {
            padding: 40px 15px;
            width: 100%
        }
    }

    .logo {
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        color: #515365;
    }

    .logo img {
        transition: opacity .3s ease
    }

    .logo .logo-title {
        text-transform: uppercase;
        margin: 0;
        color: inherit;
        transition: all .3s ease
    }

    .logo .sub-title {
        font-size: 7px;
        text-transform: uppercase
    }


    .logo .img-wrap {
        position: relative
    }
    h6 {
        margin-bottom: .5rem;

        font-weight: 700;
        line-height: 1.3;
        color: #515365;
        display: block;
        -webkit-margin-before: 2.33em;
        -webkit-margin-after: 2.33em;
        -webkit-margin-start: 0px;
        -webkit-margin-end: 0px;
        font-size: 12px;
    }
    .widget + .widget {
        margin-top: 5px;
        margin-bottom: 5px;
    }

    .w-about .logo {
        color: inherit;
        overflow: hidden;
        position: relative;
        margin-bottom: 30px;
        padding: 0
    }

    .w-about ul, .w-list ul {
        margin-bottom: 0;
        padding: 10px;
        font-size: 12px;
    }

    .w-about img {
        margin-right: 17px
    }

    .w-about .logo-title {
        color: #515365
    }

    .w-list a {
        color: #888da8;
        padding: 8px 0;
        display: block;
        font-weight: 500;
        position: relative
    }

    .w-list a:hover {
        color: #ff5e3a
    }
</style>

<!-- Footer area -->
<div class="footer-area footer footer-full-width">
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 5px">


                    <!-- Widget About -->

                    <div class=" w-about" style="margin-top: 25px">

                        <a href="#" class="logo">
                            <div class="img-wrap">
                                <img style="height: 50px;width: 50px" src="{{ asset('/nowopen1/img/logo-colored.png') }}" alt="NowOpen">
                            </div>
                            <div class="title-block">
                                <h6 class="logo-title">NowOpen</h6>
                                <div class="sub-title">vendor NETWORK</div>
                            </div>
                        </a>
                        <p style="font-size: 12px">Share your Business | Advertise | Reach your Audience</p>
                    </div>

                    <!-- ... end Widget About -->

                </div>

                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <!-- Widget List -->

                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="{{ url('/') }}"><h6 class="title">Privacy</h6></a>
                            </li>
                            <li>
                                <a href="{{ url('/contact') }}"><h6 class="title">Contact Us</h6></a>
                            </li>
                        </ul>
                    </div>

                    <!-- ... end Widget List -->

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="#"><h6 class="title">About Us</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Offers</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">

                        <ul>
                            <li>
                                <a href="#"><h6 class="title">FAQ</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Advertising</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="#"><h6 class="title">Terms & Conditions</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Agency</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                    <!-- SUB Footer -->

                    <div class="sub-footer-copyright">
					<span style="font-size: 10px">
                        &copy; <a href="{{ url('/') }}">NOWOPEN | AEY International Limited </a>- All Rights Reserved 2018
					</span>
                    </div>

                    <!-- ... end SUB Footer -->

                </div>
            </div>
        </div>
    </div><!-- End Footer Top -->

</div><!-- End Footer area -->

<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/13/2018
 * Time: 4:23 AM
 */
?>
@extends('layouts.master')

@section('title')
    Search || NowOpen.ng
@endsection

@section('content')



    <!-- Breadcurb Area -->
    <div class="breadcurb-area">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li>Search</li>
            </ul>
        </div>
    </div><!-- End Breadcurb Area -->
    <!-- Shop Product Area -->
    <div class="shop-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <!-- Shop Product Left -->
                    <div class="shop-product-left">
                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="layout-title">
                                <h2>Category</h2>
                            </div>
                            <div class="layout-list">
                                <ul>
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}"><i class="fa fa-plus-square-o"></i>{{ $cat->name }} </a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div><!-- End Shop Layout Area -->

                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="popular-tag">
                                <h2>Popular Tags</h2>
                                <div class="tag-list">
                                    <ul>
                                        <li><a href="#">Clothing</a></li>
                                        <li><a href="#">accessories</a></li>
                                        <li><a href="#">fashion</a></li>
                                        <li><a href="#">footwear</a></li>
                                        <li><a href="#">good</a></li>
                                        <li><a href="#">kid</a></li>
                                        <li><a href="#">Men</a></li>
                                        <li><a href="#">Women</a></li>
                                    </ul>
                                </div>
                                <div class="tag-action">
                                    <ul>
                                        <li><a href="#">View all tags</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-layout-banner banner-add">
                                <a href="#">
                                    <img alt="banner" src="{{ asset('nowopen/img/product/sp4.jpg') }}">
                                </a>
                            </div>
                        </div><!-- End Shop Layout Area -->
                    </div><!-- End Shop Product Left -->
                </div>
                <div class="col-md-9 col-sm-12">
                    <!-- Shop Product Right -->
                    <div class="shop-product-right">
                        <div class="product-tab-area">
                            <!-- Tab Bar -->
                            <div class="tab-bar">
                                <div class="tab-bar-inner">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li><a href="#shop-product" data-toggle="tab"><i class="fa fa-th-large"></i>Grid</a></li>
                                        <li class="active"><a href="#shop-list" data-toggle="tab"><i class="fa fa-th-list"></i>List</a></li>
                                        @if($companys == 0)
                                            <li class=""><a href="" data-toggle="tab"><i class="fa fa-search"></i>0 Company Result Found {{ $search }}</a></li>
                                        @else
                                            <li class=""><a href="" data-toggle="tab"><i class="fa fa-search"></i>{{ count($companys) }} Company Result Found</a></li>
                                        @endif
                                        @if($products == 0)
                                            <li class=""><a href="" data-toggle="tab"><i class="fa fa-search"></i>0 Product Result Found For {{ $search }}</a></li>
                                        @else
                                            <li class=""><a href="" data-toggle="tab"><i class="fa fa-search"></i>{{ count($companys) }} Result Product Found</a></li>
                                        @endif
                                    </ul>
                                </div>

                            </div><!-- End Tab Bar -->
                            <!-- Tab Content -->
                            <div class="tab-content">
                                <div class="shop-layout-area">
                                    <div class="layout-title">
                                        <h2>Search Result For {{ $search }}</h2>
                                    </div>
                                    <div class="layout-list">
                                        <ul>
                                            <li><a href="{{ url('/companysearch',['search' => $search]) }}"><i class="fa fa-plus-square-o"></i>Companies {{ $companys }}</a></li>
                                            <li><a href="{{ url('/productsearch',['search' => $search]) }}"><i class="fa fa-plus-square-o"></i>Products {{ $products }}</a></li>
                                        </ul>
                                    </div>
                                </div><!-- End Shop Layout Area -->
                            </div><!-- End Tab Content -->
                            <!-- Tab Bar -->
                        </div>
                    </div><!-- End Shop Product Left -->
                </div>
            </div>
        </div>
    </div><!-- End Shop Product Area -->


@endsection


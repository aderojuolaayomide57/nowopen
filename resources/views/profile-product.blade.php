
@extends('layouts.master')

@section('title')
    Profile Product || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
    @include('includes.profiletopheader')

    <!-- ... end Top Header-Profile -->


        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <div class="h6 title">Products</div>

                            @if(Session::has('deletesuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif

                            <div class="align-right">
                                @if(!empty(Auth::user()->id) && Auth::user()->id == $company->user_id)
                                    <a href="{{ url('/addproduct',['id' => $company->id]) }}" class="btn btn-primary btn-md-2" style="font-weight: bold">Add Product  +</a>
                                @endif

                            </div>

                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                    @forelse($products as $product)
                        <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12 col-xs-12">

                            <div class="ui-block video-item">
                                <div class="video-player">
                                    @if(Storage::disk('local')->has($product->logo))
                                        <img  style="height: 150px;width: 250px;" src="{{ route('AllImage', ['filename' => $product->logo]) }}"   alt="product">
                                    @endif
                                    <div class="overlay overlay-dark"></div>

                                    <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></div>
                                </div>

                                <div class="ui-block-content video-content" style="padding-top: 0px;padding-left: 10px;height: 70px;padding-right: 10px">
                                    <div style="display: block">
                                        <a href="{{ url('productdetails', ['id' => $product->id]) }}" class="" style="float: left;width: 83%">
                                            <h4 style="color: #0b0b0b;font-weight: bold;font-size: 11px"> {{$product->name}}</h4></a>
                                        @if(!empty(Auth::user()->id) && Auth::user()->id == $company->user_id)
                                            <a href="{{ url('/delete',['id' => $product->id,'database' => 'products']) }}" class="" style="float: right; margin-top: 2px;padding-right: 0px"><i class="fa fa-trash"></i></a>
                                        @endif
                                    </div>
                                    <br>
                                </div>
                            </div>

                        </div>
                    @empty

                    @endforelse

            </div>
        </div>
    </div>


    <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

        <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
        <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
        <div class="content-bg-wrap" >
            <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
        </div>
    </section>

    <!-- ... end Section Call To Action Animation -->

    <!-- Window-popup Update Header Photo -->

@endsection
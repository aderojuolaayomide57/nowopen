<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/22/2018
 * Time: 5:35 AM
 */
?>

@extends('layouts.master')

@section('title')
    Test || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')

    <!-- Main Slider Area -->
    <div class="main-slider-area" id="app">
        <!-- Main Slider -->
        <div class="main-slider">

            <div class="slider">
                <div id="mainSlider" class="nivoSlider slider-image">
                    <?php $i=1; ?>
                    @forelse($headers as $header)
                        @if(Storage::disk('local')->has($header->picture))
                            <img src="{{ route('AllImage', ['filename' => $header->picture]) }}"  alt="main slider" title="#htmlcaption1"/>
                        @endif

                    @empty
                        <img src="{{ asset('/nowopen/img/slider/slider11.jpg') }}" alt="main slider" title="#htmlcaption2"/>
                        <img src="{{ asset('/nowopen/img/slider/slider12.jpg') }}" alt="main slider" title="#htmlcaption3"/>
                        <img src="{{ asset('/nowopen/img/slider/slider13.jpg') }}" alt="main slider" title="#htmlcaption4"/>
                        <img src="{{ asset('/nowopen/img/slider/slider14.jpg') }}" alt="main slider" title="#htmlcaption5"/>
                    @endforelse
                </div>

            </div>
            <div class="">
                <div class="" style="padding: 20px;font-size: 12px;height: 80px;margin-bottom: 0;">
                    <div class="row">
                        <div class="col-md-12">
                            <search-form></search-form>
                        </div>
                    </div>
                </div>
            </div><!-- End Main Menu Area -->

            <div class=""  style="background: #f9f9f9;">
                <br>

                <div class="brand-products-area" v-if="seen" id="hide">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="brand-products brand-product-shoes c-carousel-button">
                                    <div class="row">
                                        <div class="container" style="width: 100%; padding: 20px">

                                            <h3 style="float: left;width: 20%; margin-left: 15px;font-size: 12px">BESTSELLER BRANDS & SERVICES</h3>
                                            <hr style="float: right;width:78%;margin-bottom: -10px;border: 0.04em solid rgba(211,217,196,0.35)">
                                        </div>

                                    </div>

                                </div>

                            </div><!-- End Brand Product Column -->
                        </div>

                    </div>
                    <div class="container">
                        <div class="container"><!-- Single Product Carousel-->
                            <div id="" class="row">
                                @forelse($basics as $company)
                                    <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            @include('includes.company-cards2')
                                        </div>

                                    </a>
                                @empty

                                @endforelse
                            </div><!-- End Single Product Carousel-->
                        </div>
                    </div>
                </div><!-- End Brand Product area -->
                <div class="brand-products-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="brand-products brand-product-shoes c-carousel-button">
                                    <div class="row">
                                        <div class="container" style="width: 100%; padding-bottom: 20px">
                                            <h3 style="float: left;width: 15%; margin-left: 15px;font-size: 12px">NEW BRANDS & SERVICES</h3>
                                            <hr style="float: right;width:83%;border: 0.04em solid rgba(211,217,196,0.35)">
                                        </div>

                                    </div>

                                </div>

                            </div><!-- End Brand Product Column -->
                        </div>

                    </div>
                    <div class="container">
                        <div class="container"><!-- Single Product Carousel-->
                            <div id="" class="row">
                                @forelse($users as $company)
                                    <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            @include('includes.company-cards2')
                                        </div>

                                    </a>
                                @empty

                                @endforelse
                            </div><!-- End Single Product Carousel-->
                        </div>
                    </div>
                </div><!-- End Brand Product area -->


                <!-- Product area -->
                <div class="product-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Product Top Bar -->
                                <div class="row">
                                    <div class="container" style="width: 100%; padding-bottom: 20px">
                                        <h3 style="float: left;width: 17%; margin-left: 15px;font-size: 12px">All BRANDS AND SERVICES</h3>
                                        <hr style="float: right;width:81.5%;border: 0.02em solid rgba(211,217,196,0.35)">
                                    </div>

                                </div>
                            </div>
                            <div class="container">
                                <div class="container"><!-- Single Product Carousel-->
                                    <div id="" class="row">
                                        @forelse($users as $company)
                                            <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    @include('includes.company-cards2')
                                                </div>

                                            </a>
                                        @empty

                                        @endforelse
                                    </div><!-- End Single Product Carousel-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Product area -->

            </div>
        </div>
    </div>


@endsection

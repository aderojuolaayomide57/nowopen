<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>NowOpen - Sign Up</title>

    <!-- Required meta tags always come first -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Main Font -->
    <script src="{{ asset('/nowopen1/js/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>




    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/main.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/fonts.min.css') }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.css">


</head>
<body>

<!-- Header Standard -->

<div class="header--standard" id="header--standard">
    <div class="container">
        <div class="header--standard-wrap">

             <a href="{{ url('/') }}" class="logo">
                <div class="img-wrap">
                    <img src="{{ asset('/nowopen1/img/logo-colored-small.png') }}" alt="Olympus">
                </div>
                <div class="title-block">
                    <h6 class="logo-title">NOWOPEN</h6>
                    <div class="sub-title">VENDOR NETWORK</div>
                </div>
            </a>

            <a href="#" class="open-responsive-menu js-open-responsive-menu">
                <svg class="olymp-menu-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
            </a>

            <div class="nav nav-pills nav1 header-menu">
                <div class="mCustomScrollbar">
                    <ul>
                        <li class="nav-item">
                            <a href="{{ url('/home') }}" class="nav-link">Back to Home Page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Header Standard  --><br><br><br><br><br><br><br>


<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="presentation-margin">Register your Business or Brand</h2>
                @if(Session::has('companysuccess'))
                    <div class="alert-box">
                        <h4 style="color: green;">{!! Session::get('companysuccess') !!}</h4>
                    </div>
                @endif
                @if(Session::has('companyfailed'))
                    <div class="alert-box">
                        <h4 style="color: red;">{!! Session::get('companyfailed') !!}</h4>
                    </div>
                @endif
            <div class="ui-block">

                <div class="ui-block-content">

                    <!-- Personal Information Form  -->

                    <form method="post" action="{{ url('/savecompany') }}">
                        {{ csrf_field() }}
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                <div class="form-group label-floating {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                    <label class="control-label">Company Name</label>
                                    <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" required autofocus>
                                    @if ($errors->has('company_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="" type="email" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('rcnumber') ? ' has-error' : '' }}">
                                    <label class="control-label">RC Number (Optional)</label>
                                    <input id="rcnumber" type="text" class="form-control" name="rcnumber" value="{{ old('rcnumber') }}" >
                                    @if ($errors->has('rcnumber'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('rcnumber') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group date-time-picker label-floating {{ $errors->has('start_date') ? ' has-error' : '' }}">
                                    <label class="control-label">Establishement Year</label>
                                    <input id="datetimepicker"  name="start_date" value="{{ old('start_date') }}" required/>
                                    <span class="input-group-addon">
                                        <svg class="olymp-month-calendar-icon icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
                                    </span>
                                    @if ($errors->has('start_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                    <label class="control-label">Phone Number</label>
                                    <input id="mobile_no" type="number" class="form-control" name="mobile_no" value="{{ old('mobile_no') }}" required>
                                    @if ($errors->has('mobile_no'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('website') ? ' has-error' : '' }}">
                                    <label class="control-label">Website</label>
                                    <input class="form-control"  type="text" value="{{ old('website') }}" name="website" required>
                                    @if ($errors->has('website'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('fax') ? ' has-error' : '' }}">
                                    <label class="control-label">Fax</label>
                                    <input class="form-control"  type="text" name="fax" value="{{ old('fax') }}" required>
                                    @if ($errors->has('fax'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="form-group label-floating {{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label class="control-label">Address (Enter full address of your company.)</label>
                                    <input class="form-control" type="text" name="address" value="{{ old('address') }}" required>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group label-floating is-select {{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Country</label>
                                    <select class="selectpicker form-control" id="country" name="country" required>
                                        <option value="{{ old('country') }}">{{ old('country') }}</option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->name }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group label-floating is-select {{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label class="control-label">Your State / Province</label>
                                    <select id="state" class="form-control" name="state" required style="height: 58px;">
                                        <option value="{{ old('state') }}">{{ old('state') }}</option>
                                    </select>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group label-floating is-select {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label class="control-label">Your City</label>
                                    <select class="form-control" name="city" id="city" style="height: 58px;" required>
                                        <option value="{{ old('city') }}">{{ old('city') }}</option>
                                    </select>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating {{ $errors->has('about') ? ' has-error' : '' }}">
                                    <label class="control-label">About Company</label>
                                    <textarea class="form-control" placeholder="" name="about" required>{{ old('about') }}</textarea>
                                    @if ($errors->has('about'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating is-select {{ $errors->has('category') ? ' has-error' : '' }}">
                                    <label class="control-label">Business Category</label>
                                    <select class="selectpicker form-control" name="category" id="category" required>
                                        <option value="{{ old('category') }}">{{ old('category') }}</option>
                                        @foreach($cats as $cat)
                                            <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('opening') ? ' has-error' : '' }}">
                                    <label class="control-label">Opening Time</label>
                                    <input class="form-control" placeholder="" id="timepicker" type="text" name="opening" value="{{ old('opening') }}" required>
                                    @if ($errors->has('opening'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('opening') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating {{ $errors->has('employees') ? ' has-error' : '' }}">
                                    <label class="control-label">Employees</label>
                                    <select class="selectpicker form-control" name="employees" required>
                                        <option value="{{ old('employees') }}">{{ old('employees') }}</option>
                                        <option value="MA">Choose...</option>
                                        <option value="1-5">1-5</option>
                                        <option value="6-10">6-10</option>
                                        <option value="11-15">11-15</option>
                                        <option value="16-25">16-25</option>
                                        <option value="26-50">26-50</option>
                                        <option value="51-100">51-100</option>
                                        <option value="101-200">101-200</option>
                                        <option value="201-500">201-500</option>
                                    </select>
                                    @if ($errors->has('employees'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('employees') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('manager') ? ' has-error' : '' }}">
                                    <label class="control-label">Company Manager</label>
                                    <input class="form-control" name="manager" type="text" value="{{ old('manager') }}" required>
                                    @if ($errors->has('manager'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('manager') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating is-select {{ $errors->has('type') ? ' has-error' : '' }}">
                                    <label class="control-label">Service Type</label>
                                    <select class=" form-control" name="type" id="type" required style="height: 53px;">
                                        <option value="{{ old('type') }}">{{ old('type') }}</option>
                                        <option value="MA">Select...</option>
                                    </select>
                                    @if ($errors->has('type'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group label-floating {{ $errors->has('closing') ? ' has-error' : '' }}">
                                    <label class="control-label">Closing Time</label>
                                    <input class="form-control" placeholder="" id="timepicker1"  name="closing" type="text" value="{{ old('closing') }}">
                                    @if ($errors->has('closing'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('closing') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group with-icon label-floating {{ $errors->has('facebook') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Facebook Account</label>
                                    <input class="form-control" type="text" name="facebook" value="{{ old('facebook') }}" required>
                                    <i class="fa fa-facebook c-facebook" aria-hidden="true"></i>
                                    @if ($errors->has('facebook'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group with-icon label-floating {{ $errors->has('twitter') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Twitter Account</label>
                                    <input class="form-control" type="text" name="twitter" value="{{ old('twitter') }}" required>
                                    <i class="fa fa-twitter c-twitter" aria-hidden="true"></i>
                                    @if ($errors->has('twitter'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group with-icon label-floating {{ $errors->has('instagram') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Instagram Account</label>
                                    <input class="form-control" type="text" name="instagram" value="{{ old('instagram') }}" required>
                                    <i class="fa fa-instagram c-instagram" aria-hidden="true"></i>
                                    @if ($errors->has('instagram'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('instagram') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group with-icon label-floating {{ $errors->has('youtube') ? ' has-error' : '' }}">
                                    <label class="control-label">Your YouTube Account</label>
                                    <input class="form-control" type="text" name="youtube" value="{{ old('youtube') }}" required>
                                    <i class="fa fa-youtube c-youtube" aria-hidden="true"></i>
                                    @if ($errors->has('youtube'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('youtube') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-secondary btn-lg full-width">Return to Homepage</button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-lg full-width">Save & Continue</button>
                            </div>

                        </div>
                    </form>

                    <!-- ... end Personal Information Form  -->
                </div>
            </div>
        </div>
    </div>
</div>




<!-- Section Call To Action Animation -->

<section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme">
    <!-- <div class="container">
        <div class="row">
            <div class="col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#registration-login-form-popup">Register Now to NowOpen</a>
            </div>
        </div>
    </div> -->
    <img class="first-img" alt="guy" src="{{ asset('/nowopen1/img/guy.png') }}">
    <img class="second-img" alt="rocket" src="{{ asset('/nowopen1/img/rocket1.png') }}">
    <div class="content-bg-wrap">
        <div class="content-bg bg-section1"></div>
    </div>
</section>

<!-- ... end Section Call To Action Animation -->


<div class="modal fade" id="registration-login-form-popup">
    <div class="modal-dialog ui-block window-popup registration-login-form-popup">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
            <svg class="olymp-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
        </a>
        <div class="registration-login-form">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                        <svg class="olymp-login-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-login-icon"></use></svg>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                        <svg class="olymp-register-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-register-icon"></use></svg>
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home1" role="tabpanel" data-mh="log-tab">
                    <div class="title h6">Register to NowOpen</div>
                    <form class="content">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">First Name</label>
                                    <input class="form-control" placeholder="" type="text">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">Last Name</label>
                                    <input class="form-control" placeholder="" type="text">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="" type="email">
                                </div>
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">Your Password</label>
                                    <input class="form-control" placeholder="" type="password">
                                </div>

                                <div class="form-group date-time-picker label-floating">
                                    <label class="control-label">Your Birthday</label>
                                    <input name="datetimepicker" value="10/24/1984" />
                                    <span class="input-group-addon">
											<svg class="olymp-calendar-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-calendar-icon"></use></svg>
										</span>
                                </div>

                                <div class="form-group label-floating is-select">
                                    <label class="control-label">Your Gender</label>
                                    <select class="selectpicker form-control">
                                        <option value="MA">Male</option>
                                        <option value="FE">Female</option>
                                    </select>
                                </div>

                                <div class="remember">
                                    <div class="checkbox">
                                        <label>
                                            <input name="optionsCheckboxes" type="checkbox">
                                            I accept the <a href="#">Terms and Conditions</a> of the website
                                        </label>
                                    </div>
                                </div>

                                <a href="#" class="btn btn-purple btn-lg full-width">Complete Registration!</a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="profile1" role="tabpanel" data-mh="log-tab">
                    <div class="title h6">Login to your Account</div>
                    <form class="content">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="" type="email">
                                </div>
                                <div class="form-group label-floating is-empty">
                                    <label class="control-label">Your Password</label>
                                    <input class="form-control" placeholder="" type="password">
                                </div>

                                <div class="remember">

                                    <div class="checkbox">
                                        <label>
                                            <input name="optionsCheckboxes" type="checkbox">
                                            Remember Me
                                        </label>
                                    </div>
                                    <a href="#" class="forgot">Forgot my Password</a>
                                </div>

                                <a href="#" class="btn btn-lg btn-primary full-width">Login</a>

                                <div class="or"></div>

                                <a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fa fa-facebook" aria-hidden="true"></i>Login with Facebook</a>

                                <a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fa fa-twitter" aria-hidden="true"></i>Login with Twitter</a>


                                <p>Don’t you have an account? <a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer Full Width -->

@include('includes.footer')

<!-- ... end Footer Full Width -->



<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive">
    <div class="ui-block-title">
        <span class="icon-status online"></span>
        <h6 class="title" >Chat</h6>
        <div class="more">
            <svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
            <svg class="olymp-little-delete js-chat-open"><use xlink:href="svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
        </div>
    </div>
    <div class="mCustomScrollbar">
        <ul class="notification-list chat-message chat-message-field">
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                </div>
            </li>

            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/author-page.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Don’t worry Mathilda!</span>
                    <span class="chat-message-item">I already bought everything</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
                </div>
            </li>

            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                </div>
            </li>
        </ul>
    </div>

    <form class="need-validation">

        <div class="form-group label-floating is-empty">
            <label class="control-label">Press enter to post...</label>
            <textarea class="form-control" placeholder=""></textarea>
            <div class="add-options-message">
                <a href="#" class="options-message">
                    <svg class="olymp-computer-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
                </a>
                <div class="options-message smile-block">

                    <svg class="olymp-happy-sticker-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-happy-sticker-icon"></use></svg>

                    <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat1.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat2.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat3.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat4.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat5.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat6.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat7.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat8.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat9.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat10.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat11.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat12.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat13.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat14.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat15.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat16.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat17.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat18.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat19.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat20.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat21.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat22.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat23.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat24.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat25.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat26.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat27.png') }}" alt="icon">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </form>


</div>

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->



<a class="back-to-top" href="#">
    <img src="{{ asset('/nowopen1/svg-icons/back-to-top.svg') }}" alt="arrow" class="back-icon">
</a>



<!-- JS Scripts -->
<script src="{{ asset('/nowopen1/js/jquery-3.2.1.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.appear.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('/nowopen1/js/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('/nowopen1/js/svgxuse.js') }}"></script>
<script src="{{ asset('/nowopen1/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Headroom.js') }}"></script>
<script src="{{ asset('/nowopen1/js/velocity.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ScrollMagic.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('/nowopen1/js/popper.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/material.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('/nowopen1/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('/nowopen1/js/selectize.js') }}"></script>
<script src="{{ asset('/nowopen1/js/swiper.jquery.js') }}"></script>
<script src="{{ asset('/nowopen1/js/moment.js') }}"></script>
<script src="{{ asset('/nowopen1/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/nowopen1/js/simplecalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/fullcalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/isotope.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ajax-pagination.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/chartjs-plugin-deferred.js') }}"></script>
<script src="{{ asset('/nowopen1/js/circle-progress.js') }}"></script>
<script src="{{ asset('/nowopen1/js/loader.js') }}"></script>
<script src="{{ asset('/nowopen1/js/run-chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.gifplayer.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-and-player.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-playlist-plugin.min.js') }}"></script>

<script src="{{ asset('/nowopen1/js/base-init.js') }}"></script>

<script src="{{ asset('/nowopen1/Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.js"></script>

<script>
    $('#category').on('change',function(e){
        var category = $('#category option:selected').attr('value');
        $.ajax({
            data: {category : category ,_token: '{{ csrf_token() }}'},
            url: '{{ route('listsubcat') }}',
            method: 'POST',
            success: function(data){
                $('#type').find('option').remove().end();
                $('#type').append($('<option>').text('--- select service type --').attr('value', ''));
                $.each(data, function(i, value) {
                    $('#type').append($('<option>').text(value.name).attr('value', value.name));
                });

            },
            error: {}
        });

    });
    $('#country').on('change',function(e){
        var country = $('#country option:selected').attr('value');
        $.ajax({
            data: {country : country ,_token: '{{ csrf_token() }}'},
            url: '{{ route('liststate') }}',
            method: 'GET',
            success: function(data){
                $('#state').find('option').remove().end();
                $.each(data, function(i, value) {
                    $('#state').append($('<option>').text(value.name).attr('value', value.name));
                });

            },
            error: {}
        });

    });
    $('#state').on('change',function(e){
        var state = $('#state option:selected').attr('value');
        $.ajax({
            data: {state : state ,_token: '{{ csrf_token() }}'},
            url: '{{ route('listcity') }}',
            method: 'GET',
            success: function(data){
                $('#city').find('option').remove().end();
                $('#city').append($('<option>').text(' select city ').attr('value', ''));
                $.each(data, function(i, value) {
                    $('#city').append($('<option>').text(value.name).attr('value', value.name));
                });

            },
            error: {}
        });

    });
</script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    } );
    $('#timepicker').timepicker({
        'minTime': '8:00am',
        'maxTime': '5:00pm',
        'showDuration': true
    });
    $('#timepicker1').timepicker({
        'minTime': '8:00am',
        'maxTime': '5:00pm',
        'showDuration': true
    });
</script>

</body>

</html>

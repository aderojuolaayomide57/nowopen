<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/13/2018
 * Time: 4:57 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/12/2018
 * Time: 1:49 AM
 */
?>

@extends('layouts.master')

@section('title')
    Search || NowOpen.ng
@endsection

@section('content')



    <!-- Breadcurb Area -->
    <div class="breadcurb-area">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li>Search</li>
            </ul>
        </div>
    </div><!-- End Breadcurb Area -->
    <!-- Shop Product Area -->
    <div class="shop-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <!-- Shop Product Left -->
                    <div class="shop-product-left">
                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="layout-title">
                                <h2>Category</h2>
                            </div>
                            <div class="layout-list">
                                <ul>
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}"><i class="fa fa-plus-square-o"></i>{{ $cat->name }} <span>(15)</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div><!-- End Shop Layout Area -->

                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="popular-tag">
                                <h2>Popular Tags</h2>
                                <div class="tag-list">
                                    <ul>
                                        <li><a href="#">Clothing</a></li>
                                        <li><a href="#">accessories</a></li>
                                        <li><a href="#">fashion</a></li>
                                        <li><a href="#">footwear</a></li>
                                        <li><a href="#">good</a></li>
                                        <li><a href="#">kid</a></li>
                                        <li><a href="#">Men</a></li>
                                        <li><a href="#">Women</a></li>
                                    </ul>
                                </div>
                                <div class="tag-action">
                                    <ul>
                                        <li><a href="#">View all tags</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-layout-banner banner-add">
                                <a href="#">
                                    <img alt="banner" src="{{ asset('nowopen/img/product/sp4.jpg') }}">
                                </a>
                            </div>
                        </div><!-- End Shop Layout Area -->
                    </div><!-- End Shop Product Left -->
                </div>
                <div class="col-md-9 col-sm-12">
                    <!-- Shop Product Right -->
                    <div class="shop-product-right">
                        <div class="product-tab-area">
                            <!-- Tab Bar -->
                            <div class="tab-bar">
                                <div class="tab-bar-inner">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li><a href="#shop-product" data-toggle="tab"><i class="fa fa-th-large"></i>Grid</a></li>
                                        <li class="active"><a href="#shop-list" data-toggle="tab"><i class="fa fa-th-list"></i>List</a></li>
                                        @if(count($products) == 0)
                                            <li class=""><a href="#shop-list" data-toggle="tab"><i class="fa fa-search"></i>0 Result Found</a></li>
                                        @else
                                            <li class=""><a href="#shop-list" data-toggle="tab"><i class="fa fa-search"></i>{{ count($products) }} Result Found</a></li>
                                        @endif
                                    </ul>
                                </div>

                            </div><!-- End Tab Bar -->
                            <!-- Tab Content -->
                            <div class="tab-content">
                                <div class="tab-pane" id="shop-product">
                                    <div class="row tab-content-row">
                                        @forelse($products as $product)
                                            <div class="col-md-4 col-sm-4">
                                                <div class="single-product">
                                                    <div class="single-product-img">
                                                        <a href="{{ url('productdetails', ['id' => $product->id]) }}">
                                                            @if(Storage::disk('local')->has($product->logo))
                                                                <img class="primary-img" style="height: 262px;width: 329px;" src="{{ route('AllImage', ['filename' => $product->logo]) }}"   alt="product">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <div class="single-product-content">
                                                        <div class="product-content-head">
                                                            <h2 class="product-title"><a href="{{ url('productdetails', ['id' => $product->id]) }}">{{ $product->name }}</a></h2>
                                                            <p class="product-price">{{ $product->price }}</p>
                                                        </div>
                                                        <div class="product-bottom-action">
                                                            <div class="product-action">
                                                                <div class="action-view">
                                                                    <button type="button" class="btn" data-toggle="modal" data-target="#productModal"><i class="fa fa-search"></i>Quick view</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty

                                        @endforelse

                                    </div>

                                </div>
                                <div class="tab-pane active" id="shop-list">
                                @forelse($products as $product)
                                    <!-- Single Shop -->
                                        <div class="single-shop single-product">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="single-product-img">
                                                        @if(Storage::disk('local')->has($product->logo))
                                                            <img class="primary-img" style="height: 262px;width: 329px;" src="{{ route('AllImage', ['filename' => $product->logo]) }}"   alt="product">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-8">
                                                    <div class="single-shop-content">
                                                        <div class="shop-content-head fix">
                                                            <h1><a href="{{ url('productdetails', ['id' => $product->id]) }}">{{ $product->name }}</a></h1>
                                                        </div>
                                                        <div class="shop-content-bottom">
                                                            <div class="product-details">
                                                                <p>{{ $product->description }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="product-bottom-action">
                                                            <div class="product-action">
                                                                <div class="action-button">
                                                                    <a href="{{ url('productdetails', ['id' => $product->id]) }}"><button class="btn" type="button"><i class="fa fa-search"></i> <span>Quick view</span></button></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- End Single Shop -->
                                        <!-- Single Shop -->
                                    @empty

                                    @endforelse


                                </div>
                            </div><!-- End Tab Content -->
                            <!-- Tab Bar -->
                            <div class="tab-bar">
                                <div class="toolbar">
                                    <div class="pages">
                                        <strong>Page:</strong>
                                        <ol>
                                            @if(count($products) >10)
                                                {{ $products->render() }}
                                            @endif
                                        </ol>
                                    </div>
                                </div>
                            </div><!-- End Tab Bar -->
                        </div>
                    </div><!-- End Shop Product Left -->
                </div>
            </div>
        </div>
    </div><!-- End Shop Product Area -->


@endsection


<?php
/**
 * Created by PhpStorm.
 * User: venoworks
 * Date: 6/9/2018
 * Time: 8:56 PM
 */
?>
@extends('layouts.master')

@section('title')
    My Account || NowOpen.ng
@endsection

@section('content')
    <!-- My Account Area -->
    <div class="my-account-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="my-account-title">
                        <h2>Login or Create an Account</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="new-customers customer">
                        <div class="customer-inner">
                            <div class="user-title">
                                <h2><i class="fa fa-file"></i>New Customers</h2>
                            </div>
                            <div class="user-content">
                                <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                            </div>
                        </div>
                        <div class="user-bottom fix">
                            <button class="btn" type="button"><a href="{{ url('register') }}">Create an Account</a></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <form class="resestered-customers customer" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="customer-inner">
                            <div class="user-title">
                                <h2><i class="fa fa-file-text"></i>Registered  Customers</h2>
                            </div>
                            <div class="user-content">
                                <p>If you have an account with us, please log in.</p>
                            </div>
                            <div class="account-form">
                                <div class="form-horizontal product-form" >

                                    <div class="form-goroup{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Email Address <sup>*</sup></label>
                                        <input type="text" required="required" name="email" {{ old('email') }} class="form-control">
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="form-goroup{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label>Password <sup>*</sup></label>
                                        <input type="password" name="password" required="required" class="form-control">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <p class="reauired-fields floatright"><sup>*</sup> Required Fields</p>
                        </div>
                        <div class="user-bottom fix">
                            <a class="" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                            <button class="btn" type="submit">login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- End My Account Area -->
    <!-- Brand Logo area -->
    <div class="brand-logo-area">
        <div class="container">
            <div class="brand-logo">
                <div class="brand-logo-title">
                    <h2>Logo brands</h2>
                </div>
                <div id="brands-logo" class="owl-carousel">
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo1.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo5.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo2.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo3.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo4.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo1.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo5.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo3.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo4.png" alt="logo">
                        </a>
                    </div>
                    <div class="single-brand-logo">
                        <a href="#">
                            <img src="img/brand-logo/blogo2.png" alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Brand Logo area -->


@endsection

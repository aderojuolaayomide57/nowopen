<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta information -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <!-- Title-->
    <title>Gac Motors  |  Admin Login</title>
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/admin/assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/admin/assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/admin/assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/admin/assets/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('/admin/assets/ico/favicon.ico') }}">

    <!-- CSS Stylesheet-->
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin/assets/css/bootstrap/bootstrap.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin/assets/css/bootstrap/bootstrap-themes.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/admin/assets/css/style.css') }}" />

    <!-- Styleswitch if  you don't chang theme , you can delete -->
    <link type="text/css" rel="alternate stylesheet" media="screen" title="style1" href="{{ asset('/admin/assets/css/styleTheme1.css') }}" />
    <link type="text/css" rel="alternate stylesheet" media="screen" title="style2" href="{{ asset('/admin/assets/css/styleTheme2.css') }}" />
    <link type="text/css" rel="alternate stylesheet" media="screen" title="style3" href="{{ asset('/admin/assets/css/styleTheme3.css') }}" />
    <link type="text/css" rel="alternate stylesheet" media="screen" title="style4" href="{{ asset('/admin/assets/css/styleTheme4.css') }}" />

</head>
<body class="full-lg">
<div id="wrapper">

    <div id="loading-top">
        <div id="canvas_loading"></div>
        <span>Checking...</span>
    </div>

    <div id="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">

                    <div class="account-wall">
                        <section class="panel">
                            <header class="panel-heading">
                                <h2>Create an Account</h2>
                                @if(Session::has('Adminaccountcreated'))
                                    <br>
                                    <div class="alert-box">
                                        <h4 style="color: green">{!! Session::get('Adminaccountcreated') !!}</h4>
                                    </div>
                                @endif
                            </header>
                            <div class="panel-body">
                                <form class="form-horizontal" action="{{ url('/saveadmin') }}" method="post" data-collabel="3" data-alignlabel="left"  data-label="color">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="control-label">Name</label>
                                        <div>
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email  Address</label>
                                        <div>
                                            <input type="text" name="email" class="form-control" value="{{ old('email') }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <div>
                                            <input type="password" name="password" class="form-control">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group offset">
                                        <div>
                                            <button type="submit" class="btn btn-theme">Submit</button>
                                            <button type="reset" class="btn" >Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                    <!-- //account-wall-->

                </div>
                <!-- //col-sm-6 col-md-4 col-md-offset-4-->
            </div>
            <!-- //row-->
        </div>
        <!-- //container-->

    </div>
    <!-- //main-->


</div>
<!-- //wrapper-->


<!--
////////////////////////////////////////////////////////////////////////
//////////     JAVASCRIPT  LIBRARY     //////////
/////////////////////////////////////////////////////////////////////
-->

<!-- Jquery Library -->
<script type="text/javascript" src="{{ asset('/admin/assets/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin/assets/js/jquery.ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/bootstrap/bootstrap.min.js') }}"></script>
<!-- Modernizr Library For HTML5 And CSS3 -->
<script type="text/javascript" src="{{ asset('/admin/assets/js/modernizr/modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/mmenu/jquery.mmenu.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin/assets/js/styleswitch.js') }}"></script>
<!-- Library 10+ Form plugins-->
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/form/form.js') }}"></script>
<!-- Datetime plugins -->
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/datetime/datetime.js') }}"></script>
<!-- Library Chart-->
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/chart/chart.js') }}"></script>
<!-- Library  5+ plugins for bootstrap -->
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/pluginsForBS/pluginsForBS.js') }}"></script>
<!-- Library 10+ miscellaneous plugins -->
<script type="text/javascript" src="{{ asset('/admin/assets/plugins/miscellaneous/miscellaneous.js') }}"></script>
<!-- Library Themes Customize-->
<script type="text/javascript" src="{{ asset('/admin/assets/js/caplet.custom.js') }}"></script>
<script type="text/javascript">
    $(function() {
        //Login animation to center
        function toCenter(){
            var mainH=$("#main").outerHeight();
            var accountH=$(".account-wall").outerHeight();
            var marginT=(mainH-accountH)/2;
            if(marginT>30){
                $(".account-wall").css("margin-top",marginT-15);
            }else{
                $(".account-wall").css("margin-top",30);
            }
        }
        toCenter();
        var toResize;
        $(window).resize(function(e) {
            clearTimeout(toResize);
            toResize = setTimeout(toCenter(), 500);
        });

        //Canvas Loading
        var throbber = new Throbber({  size: 32, padding: 17,  strokewidth: 2.8,  lines: 12, rotationspeed: 0, fps: 15 });
        throbber.appendTo(document.getElementById('canvas_loading'));
        throbber.start();

        //Set note alert
        setTimeout(function () {
            $.notific8('Hi Guest , you can use Username : <strong>demo</strong> and Password: <strong>demo</strong> to  access account.',{ sticky:true, horizontalEdge:"top", theme:"inverse" ,heading:"LOGIN DEMO"})
        }, 1000);


        $("#form-signin").submit(function(event){
            event.preventDefault();
            var main=$("#main");
            //scroll to top
            main.animate({
                scrollTop: 0
            }, 500);
            main.addClass("slideDown");

            // send username and password to php check login
            $.ajax({
                url: "data/checklogin.php", data: $(this).serialize(), type: "POST", dataType: 'json',
                success: function (e) {
                    setTimeout(function () { main.removeClass("slideDown") }, !e.status ? 500:3000);
                    if (!e.status) {
                        $.notific8('Check Username or Password again !! ',{ life:5000,horizontalEdge:"bottom", theme:"danger" ,heading:" ERROR :); "});
                        return false;
                    }
                    setTimeout(function () { $("#loading-top span").text("Yes, account is access...") }, 500);
                    setTimeout(function () { $("#loading-top span").text("Redirect to account page...")  }, 1500);
                    setTimeout( "window.location.href='dashboard.html'", 3100 );
                }
            });

        });
    });
</script>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/11/2018
 * Time: 5:19 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/9/2018
 * Time: 8:51 AM
 */
?>


@extends('layouts.master')

@section('title')
    My Profile Settings || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')

    <div style="background-color: #edf2f6;">

        @include('includes.profiletopheader2')
        <style>
            .form-control{
                height: auto;
            }

            h6 {
                font-size: .1075rem;
                color: #515365;
                font-family: inherit;
                font-weight: 700;
                line-height: 1.3;
            }
            .card {
                position: relative;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid #e6ecf5;
                border-radius: .25rem
            }

            .card > hr {
                margin-right: 0
            }

            .card > .list-group:first-child .list-group-item:first-child {
                border-top-left-radius: .25rem;
                border-top-right-radius: .25rem
            }

            .card > .list-group:last-child .list-group-item:last-child {
                border-bottom-right-radius: .25rem;
                border-bottom-left-radius: .25rem
            }

            .card-header, .card-subtitle, .card-text:last-child {
                margin-bottom: 0
            }

            .card-link:hover {
                text-decoration: none
            }

            .card-link + .card-link {
                margin-left: 1.25rem
            }

            .card-header {
                border-bottom: 1px solid #e6ecf5;
                padding: .15rem 1.25rem;
                background-color: #fff;
                height: 50px;
                padding-bottom: 50px;
            }

            .card-header:first-child {
                border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
            }

            .card-header + .list-group .list-group-item:first-child {
                border-top: 0
            }

            .card-footer:last-child {
                border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
            }

            .alert, .btn .badge, .page-link {
                position: relative
            }

            .card-deck .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-deck .card {
                    display: -ms-flexbox;
                    display: flex;
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    margin-right: 15px;
                    margin-bottom: 0;
                    margin-left: 15px
                }
            }

            .card-group .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-group .card {
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    margin-bottom: 0
                }

                .card-group .card + .card {
                    margin-left: 0;
                    border-left: 0
                }

                .card-group .card:first-child {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0
                }

                .card-group .card:first-child .card-img-top {
                    border-top-right-radius: 0
                }

                .card-group .card:first-child .card-img-bottom {
                    border-bottom-right-radius: 0
                }

                .card-group .card:last-child {
                    border-top-left-radius: 0;
                    border-bottom-left-radius: 0
                }

                .card-group .card:last-child .card-img-top {
                    border-top-left-radius: 0
                }

                .card-group .card:last-child .card-img-bottom {
                    border-bottom-left-radius: 0
                }

                .card-group .card:only-child {
                    border-radius: .25rem
                }

                .card-group .card:only-child .card-img-top {
                    border-top-left-radius: .25rem;
                    border-top-right-radius: .25rem
                }

                .card-group .card:only-child .card-img-bottom {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem
                }

                .card-group .card:not(:first-child):not(:last-child):not(:only-child), .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-bottom, .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-top {
                    border-radius: 0
                }

                .card-columns .card {
                    display: inline-block;
                    width: 100%
                }
            }

            .card-columns .card {
                margin-bottom: .75rem
            }
        </style>


        <!-- Your Account Personal Information -->

        <div class="container">
            <div class="row">
                <div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Edit Profile</h6>

                            @if(Session::has('userupdatesuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('userupdatesuccess') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('userupdatefailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('userupdatefailed') !!}</h4>
                                </div>
                            @endif
                        </div>
                        <div class="ui-block-content">


                            <!-- Change Password Form -->

                            <form method="post" action="{{ url('/updateaccount') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty {{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label class="control-label">First Name</label>
                                            <input class="form-control" placeholder="*******" type="text" name="name" value="{{ $user->name }}" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <label class="control-label">Last Name</label>
                                            <input class="form-control" placeholder="" type="text" name="last_name" value="{{ $user->last_name }}" required>
                                        </div>
                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Email</label>
                                            <input class="form-control" placeholder="" type="email" name="email" value="{{ $user->email }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating {{ $errors->has('dob') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Birthday</label>
                                            <input  class="form-control" name="dob" value="{{ $user->dob }}" id="datetimepicker"/>
                                            @if ($errors->has('dob'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                            @endif
                                            <span class="input-group-addon">
											<svg class="olymp-calendar-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-calendar-icon"></use></svg>
										</span>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-select {{ $errors->has('gender') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Gender</label>
                                            <select  name="gender" class="form-control">
                                                <option value="{{ $user->gender }}">{{ $user->gender }}</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                            @if ($errors->has('gender'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <button class="btn btn-primary btn-lg full-width">Update Account</button>
                                    </div>

                                </div>
                            </form>

                            <!-- ... end Change Password Form -->
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
                    <div class="ui-block">



                        <!-- Your Profile  -->

                        <div class="your-profile">
                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">Your PROFILE</h6>
                            </div>

                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h6 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Profile Settings
                                                <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                                            </a>
                                        </h6>
                                    </div>

                                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                        <ul class="your-profile-menu">
                                            <li>
                                                <a href="{{ url('/myaccount') }}">Personal Information</a>
                                            </li><!--
									<li>
										<a href="29-YourAccount-AccountSettings.html">Account Settings</a>
									</li> -->
                                            <a href="{{ url('/changepassword',['id' => Auth::user()->id]) }}">Change Password</a>
                                            <li>
                                                <a href="#">Hobbies and Interests</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h6 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                Education and Employement
                                                <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <ul class="your-profile-menu">
                                            <li>
                                                <a href="{{ url('/educationemployment',['id' => Auth::user()->id]) }}">Education and Employement</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/changepassword',['id' => Auth::user()->id]) }}">Change Password</a>
                                            </li>
                                            <li>
                                                <a href="#">Hobbies and Interests</a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h6 class="mb-0">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                                My Companies ( {{ count($company1s) }} )
                                                <svg class="olymp-dropdown-arrow-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
                                            </a>
                                        </h6>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <ul class="your-profile-menu">
                                            @forelse($company1s as $comp)
                                                <li>
                                                    <a href="{{ url('/myprofile',['id' => $comp->id]) }}">{{ $comp->company_name }}</a>
                                                </li>
                                            @empty
                                                <li>

                                                </li>
                                            @endforelse

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- ... end Your Profile  -->


                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Your Account Personal Information -->

        <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

            <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
            <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
            <div class="content-bg-wrap" >
                <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
            </div>
        </section>

        <!-- ... end Section Call To Action Animation -->

        <!-- Window-popup Update Header Photo -->


    </div>




@endsection

@section('profilefooter')
    @include('includes.profilefooter')
@endsection



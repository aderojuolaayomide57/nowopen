
@extends('layouts.master')

@section('title')
    Profile || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
    @include('includes.profiletopheader')

    <!-- ... end Top Header-Profile -->

        <div class="container" >
            <div class="row">
                <div class="col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h5 class="title" style="color: #515365"><b>About</b></h5>
                            <p id="getid" hidden>{{ $company->id }}</p>
                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                        </div>
                        <div class="ui-block-content">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">


                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block">
                                        <li>
                                            <span class="title">About Us:</span>
                                            <span class="text">{{ $company->about }} </span>
                                        </li>
                                    </ul>

                                    <!-- ... end W-Personal-Info -->
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">


                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block">
                                        <li>
                                            <span class="title">Other Interests:</span>
                                            <span class="text">Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.</span>
                                        </li>
                                    </ul>

                                    <!-- ... end W-Personal-Info -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h5 class="title" style="color: #515365">Business Information</h5>
                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                        </div>
                        <div class="ui-block-content">


                            <!-- W-Personal-Info -->

                            <ul class="widget w-personal-info">
                                <li>
                                    <span class="title">About Us:</span>
                                    <span class="text">{{ $company->about }}</span>
                                </li>
                                <li>
                                    <span class="title">Established:</span>
                                    <span class="text">{{ date('M', strtotime($company->start_date))}} {{ date('d', strtotime($company->start_date))}}, {{date('Y', strtotime($company->start_date))}}</span>
                                </li>
                                <li>
                                    <span class="title">Company Name:</span>
                                    <span class="text">{{ $company->company_name }}</span>
                                </li>
                                <li>
                                    <span class="title">Birth Place:</span>
                                    <span class="text">{{ $company->state }}, {{ $company->city }}</span>
                                </li>
                                <li>
                                    <span class="title">Headquaters:</span>
                                    <span class="text">{{ $company->address }}</span>
                                </li>
                                <li>
                                    <span class="title">RC Number:</span>
                                    <span class="text">{{ $company->rcnumber }}</span>
                                </li>
                                <li>
                                    <span class="title">Join Nowopens:</span>
                                    <span class="text">{{ date('M', strtotime($company->created_at))}}  {{ date('d', strtotime($company->created_at))}}, {{ date('Y', strtotime($company->created_at))}}</span>
                                </li>
                                <li>
                                    <span class="title">Manager:</span>
                                    <a href="#" class="text">{{ $company->manager }}</a>
                                </li>
                                <li>
                                    <span class="title">Status:</span>
                                    <a href="#" class="text">{{ $company->active == 1 ? 'Available' : 'Closed' }}</a>
                                </li>
                                <li>
                                    <span class="title">Email:</span>
                                    <a href="#" class="text">{{ $company->email }}</a>
                                </li>
                                <li>
                                    <span class="title">Website:</span>
                                    <a href="#" class="text">{{ $company->website }}</a>
                                </li>
                                <li>
                                    <span class="title">Phone Number:</span>
                                    <span class="text">{{ $company->mobile_no }}</span>
                                </li>
                                <li>
                                    <span class="title">Fax:</span>
                                    <span class="text">{{ $company->fax }}</span>
                                </li>
                                <li>
                                    <span class="title">Hours:</span>
                                    <span class="text">{{ $company->opening }} - {{ $company->closing }}</span>
                                </li>
                            </ul>
                            <div class="ui-block-content">
                                <h6 class="title">Other Social Networks:</h6>
                                <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="widget w-socials">
                                            <a href="#" class="social-item bg-facebook">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                Facebook
                                            </a>
                                            <a href="#" class="social-item bg-twitter">
                                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                                Twitter
                                            </a>
                                            <a href="#" class="social-item bg-dribbble">
                                                <i class="fa fa-dribbble" aria-hidden="true"></i>
                                                Dribbble
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- ... end W-Personal-Info -->
                            <!-- W-Socials -->




                            <!-- ... end W-Socials -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

        <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
        <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
        <div class="content-bg-wrap" >
            <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
        </div>
    </section>

    <!-- ... end Section Call To Action Animation -->
@endsection

@section('profilefooter')
    @include('includes.profilefooter')
@endsection
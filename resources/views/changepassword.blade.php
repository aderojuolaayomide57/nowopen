<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/10/2018
 * Time: 1:04 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/9/2018
 * Time: 8:51 AM
 */
?>


@extends('layouts.master')

@section('title')
    Profile Settings || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
        @include('includes.profiletopheader')
        <style>
            .form-control{
                height: auto;
            }
            h6 {
                font-size: .1075rem;
                color: #515365;
                font-family: inherit;
                font-weight: 700;
                line-height: 1.3;
            }
            .card {
                position: relative;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid #e6ecf5;
                border-radius: .25rem
            }

            .card > hr {
                margin-right: 0
            }

            .card > .list-group:first-child .list-group-item:first-child {
                border-top-left-radius: .25rem;
                border-top-right-radius: .25rem
            }

            .card > .list-group:last-child .list-group-item:last-child {
                border-bottom-right-radius: .25rem;
                border-bottom-left-radius: .25rem
            }

            .card-header, .card-subtitle, .card-text:last-child {
                margin-bottom: 0
            }

            .card-link:hover {
                text-decoration: none
            }

            .card-link + .card-link {
                margin-left: 1.25rem
            }

            .card-header {
                border-bottom: 1px solid #e6ecf5;
                padding: .15rem 1.25rem;
                background-color: #fff;
                height: 50px;
                padding-bottom: 50px;
            }

            .card-header:first-child {
                border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
            }

            .card-header + .list-group .list-group-item:first-child {
                border-top: 0
            }

            .card-footer:last-child {
                border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
            }

            .alert, .btn .badge, .page-link {
                position: relative
            }

            .card-deck .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-deck .card {
                    display: -ms-flexbox;
                    display: flex;
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    margin-right: 15px;
                    margin-bottom: 0;
                    margin-left: 15px
                }
            }

            .card-group .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-group .card {
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    margin-bottom: 0
                }

                .card-group .card + .card {
                    margin-left: 0;
                    border-left: 0
                }

                .card-group .card:first-child {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0
                }

                .card-group .card:first-child .card-img-top {
                    border-top-right-radius: 0
                }

                .card-group .card:first-child .card-img-bottom {
                    border-bottom-right-radius: 0
                }

                .card-group .card:last-child {
                    border-top-left-radius: 0;
                    border-bottom-left-radius: 0
                }

                .card-group .card:last-child .card-img-top {
                    border-top-left-radius: 0
                }

                .card-group .card:last-child .card-img-bottom {
                    border-bottom-left-radius: 0
                }

                .card-group .card:only-child {
                    border-radius: .25rem
                }

                .card-group .card:only-child .card-img-top {
                    border-top-left-radius: .25rem;
                    border-top-right-radius: .25rem
                }

                .card-group .card:only-child .card-img-bottom {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem
                }

                .card-group .card:not(:first-child):not(:last-child):not(:only-child), .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-bottom, .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-top {
                    border-radius: 0
                }

                .card-columns .card {
                    display: inline-block;
                    width: 100%
                }
            }

            .card-columns .card {
                margin-bottom: .75rem
            }
        </style>


        <!-- Your Account Personal Information -->

        <div class="container">
            <div class="row">
                <div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Change Password</h6>

                            @if(Session::has('passwordsuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('passwordsuccess') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('passwordupdatefailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('passwordupdatefailed') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('passwordfail'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('passwordfail') !!}</h4>
                                </div>
                            @endif
                        </div>
                        <div class="ui-block-content">


                            <!-- Change Password Form -->

                            <form method="post" action="{{ url('/updatepassword',['id' => $company->user_id]) }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating {{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                                            <label class="control-label">Confirm Current Password</label>
                                            <input class="form-control" placeholder="*****" type="password" name="oldpassword" value="" required>
                                        </div>
                                        @if ($errors->has('oldpassword'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('oldpassword') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty {{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label class="control-label">Your New Password</label>
                                            <input class="form-control" placeholder="*******" type="password" name="password" required>
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-empty ">
                                            <label class="control-label">Confirm New Password</label>
                                            <input class="form-control" id="password_confirmation" placeholder="*******"
                                                   type="password" name="password_confirmation">
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12">
                                        <div class="remember">

                                            <div class="checkbox">
                                                <label>
                                                    <input name="optionsCheckboxes" type="checkbox">
                                                    Remember Me
                                                </label>
                                            </div>

                                            <a href="#" class="forgot">Forgot my Password</a>
                                        </div>
                                    </div>

                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <button class="btn btn-primary btn-lg full-width">Change Password Now!</button>
                                    </div>

                                </div>
                            </form>

                            <!-- ... end Change Password Form -->
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
                    <div class="ui-block">



                        <!-- Your Profile  -->

                    @include('includes.settings-tab')

                    <!-- ... end Your Profile  -->


                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Your Account Personal Information -->

        <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

            <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
            <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
            <div class="content-bg-wrap" >
                <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
            </div>
        </section>

        <!-- ... end Section Call To Action Animation -->

        <!-- Window-popup Update Header Photo -->


    </div>




@endsection

@section('profilefooter')
    @include('includes.profilefooter')
@endsection


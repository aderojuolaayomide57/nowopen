<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/16/2018
 * Time: 2:52 AM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | View All Advert
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Advert</a></li>
            <li class="active">View All Advert</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Advert</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <b hidden id="database">adverts</b>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Sponsor</th>
                                        <th>Type</th>
                                        <th width="40%">logo</th>
                                        <th width="20%">Status</th>
                                        <th width="20%">Action</th>
                                        <th width="20%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @forelse($adverts as $advert)
                                        <tr>
                                            <td>{{ $advert->id }}</td>
                                            <td valign="middle">{{ $advert->sponsor }}</td>
                                            <td valign="middle">{{ $advert->type }}</td>
                                            <td >
                                                @if(Storage::disk('local')->has($advert->picture))
                                                    <img style="width: 250px;height: 150px;"  src="{{ route('AllImage', ['filename' => $advert->picture]) }}"   alt="page">
                                                @endif
                                            </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $advert->active > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $advert->id}}">
                                                    {{ $advert->active > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $advert->id,'database' => 'adverts']) }}"
                                                   class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('viewallsubcat',['id' => $advert->id]) }}"
                                                   class="" title="Delete"><i class="fa fa-eye-o"></i>view all subcategory</a>
                                            </span>
                                            </td>
                                        </tr>
                                    @empty

                                    @endforelse

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $adverts->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection

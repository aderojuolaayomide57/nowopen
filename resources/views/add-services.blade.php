<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 7/18/2019
 * Time: 3:04 AM
 */
?>

@extends('layouts.master')

@section('title')
    Profile Product || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <style>
        .space{
            margin-left: 40px;
        }
        .bluecolor{
            color: #1c94c4;
        }
    </style>
    <div style="background-color: #edf2f6;">
        @include('includes.profiletopheader')

        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">

                    <div class="ui-block">
                        <div class="ui-block-title bg-blue">
                            <h6 class="title c-white">Create a New Services</h6>
                        </div>
                        <div class="ui-block-content">
                            @if(Session::has('servicesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('servicesuccess') !!}</h4>
                                </div>
                                <br>
                            @endif
                            @if(Session::has('servicefailed'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('servicefailed') !!}</h4>
                                </div>
                                <br>
                            @endif
                            <form class="form-horizontal" method="POST" action="{{ url('/saveservices',['id' => $company->id]) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 {{ $errors->has('title') ? ' has-error' : '' }}" >
                                        <div class="form-group ">
                                            <label class="control-label">Service Title</label>
                                            <input class="form-control" type="text" name="title" placeholder="" value="{{ old('title') }}" required autofocus>
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group is-select {{ $errors->has('category') ? ' has-error' : '' }}">
                                            <label class="control-label">Select Category</label>
                                            <select name="category" class="form-control" style="margin-right: 5px;width: 99%">
                                                <option value="">All Categories</option>
                                                <option value="Test">Test</option>
                                            </select>
                                            @if ($errors->has('category'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12" >
                                        <div class="form-group is-select {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label class="control-label">Select Service Type</label>
                                            <select name="type" class="form-control" style="margin-left: 5px;width: 99%">
                                                <option value="">All Services</option>
                                                <option value="test">Test</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label class="control-label">Service Description</label>
                                            <textarea class="form-control" name="description" style="height: 240px">{{ old('description') }}</textarea>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="col-lg-12">
                                            <h6>Upload Service Photo (720x360px) 100kb Maximum</h6>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                            <div class="file-upload">
                                                <p id="file_selected"></p>
                                                <label for="upload221" class="file-upload__label">Upload Button</label>
                                                <input id="upload221" class="file-upload__input" type="file"  name="picture">
                                            </div>
                                            @if ($errors->has('file_upload'))
                                                <strong>{{ $errors->first('file_upload') }}</strong>
                                                </span>
                                                <span class="help-block">
                                            @endif
                                        </div><br><br>
                                    </div>

                                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 col-lg-offset-9">
                                        <button type="submit" class="btn btn-blue btn-lg full-width">Post Topic</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>




@endsection

<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/13/2018
 * Time: 5:34 AM
 */
?>
@extends('layouts.master')

@section('title')
    Contact Us || NowOpen.ng
@endsection

@section('content')

    <!-- Contact area -->
    <div class="contact-area">
        <div class="container">
            <div class="row">
                <!-- contact info -->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="contact-info">
                        <h3>Contact info</h3>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker"></i> <strong>Address</strong>
                                08a, Reeve Road Ikoyi, Lagos Nigeria
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i> <strong>Phone</strong>
                                (+234) 7081547726
                            </li>
                            <li>
                                <i class="fa fa-mobile"></i> <strong>Email</strong>
                                <a href="#">nowopen@nowopen.ng</a>
                            </li>
                        </ul>
                    </div>
                </div><!-- End contact info -->
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="contact-form">
                        <h3><i class="fa fa-envelope-o"></i> Leave a Message</h3>
                        <div class="row">
                            <form action="{{ url('/sendmail') }}" method="post">
                                {{ csrf_field() }}
                                <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input name="name" type="text" placeholder="Name (required)" value="{{ old('name') }}" required/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input name="email" type="email" placeholder="Email (required)" value="{{ old('email') }}" required/>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 {{ $errors->has('subject') ? ' has-error' : '' }}">
                                    <input name="subject" type="text" placeholder="Subject (required)" value="{{ old('subject') }}" />
                                    @if ($errors->has('subject'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 {{ $errors->has('message') ? ' has-error' : '' }}">
                                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Message" required>{{ old('message') }}</textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                    <input type="submit" value="Submit Form" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Eontact area -->
    <!-- Brand Logo area -->

@endsection

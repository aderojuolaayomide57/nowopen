<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 4/4/2019
 * Time: 6:29 AM
 */
?>

@extends('layouts.master')

@section('title')
    Search Result || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')



    <!-- Breadcurb Area -->
    <div class="breadcurb-area">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{ url('/home') }}">Home</a></li>
                <li>Search</li>
            </ul>
        </div>
    </div><!-- End Breadcurb Area -->
    <!-- Shop Product Area -->
    <div class="shop-product-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <!-- Shop Product Left -->
                    <div class="shop-product-left">
                        <!-- Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="layout-title">
                                <h2>Category</h2>
                            </div>
                            <div class="layout-list">
                                <ul>
                                    @foreach($cats as $cat)
                                        <li><a href="{{ url('/company',['id' => $cat->name]) }}"><i class="fa fa-plus-square-o"></i>{{ $cat->name }} <span>(15)</span></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div><!-- End Shop Layout Area -->
                        <div class="shop-layout-area">
                            <div class="popular-tag">
                                <h2>Popular Tags</h2>
                                <div class="tag-list">
                                    <ul>
                                        <li><a href="#">Clothing</a></li>
                                        <li><a href="#">accessories</a></li>
                                        <li><a href="#">fashion</a></li>
                                        <li><a href="#">footwear</a></li>
                                        <li><a href="#">good</a></li>
                                        <li><a href="#">kid</a></li>
                                        <li><a href="#">Men</a></li>
                                        <li><a href="#">Women</a></li>
                                    </ul>
                                </div>
                                <div class="tag-action">
                                    <ul>
                                        <li><a href="#">View all tags</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-layout-banner banner-add">
                                <a href="#">
                                    <img alt="banner" src="{{ asset('nowopen/img/product/sp4.jpg') }}">
                                </a>
                            </div>
                        </div><!-- End Shop Layout Area -->
                    </div><!-- End Shop Product Left -->
                </div>
                <div class="col-md-9 col-sm-12">
                    <!-- Shop Product Right -->
                    <div class="shop-product-right">
                        <ul class="mega-menu-ul">
                                @foreach($states as $local)
                                <li style="padding-bottom: 10px">
                                    <a href="{{ url('/company',['id' => $local->name]) }}" style="padding-left: 20px"><i class="fa fa-chevron-circle-right"></i>
                                        <span>{{ $local->name }}</span></a>
                                </li>
                                @endforeach
                        </ul>
                    </div><!-- End Shop Product Left -->
                </div>
            </div>
        </div>
    </div><!-- End Shop Product Area -->


@endsection



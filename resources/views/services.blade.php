<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 7/18/2019
 * Time: 1:26 AM
 */
?>

@extends('layouts.master')

@section('title')
    Profile Services || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
    @include('includes.profiletopheader')

        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <div class="h6 title">Serveices</div>

                            @if(Session::has('deletesuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif

                            <div class="align-right">
                                @if(!empty(Auth::user()->id) && Auth::user()->id == $company->user_id)
                                    <a href="{{ url('/addservices',['id' => $company->id]) }}" class="btn btn-primary btn-md-2" style="font-weight: bold">Add Services +</a>
                                @endif

                            </div>

                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="blog-post-wrap medium-padding80">
            <div class="container">
                <div class="row">
                    @foreach($services as $service)
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="ui-block">


                            <!-- Post -->

                            <article class="hentry blog-post">

                                <div class="post-thumb">
                                    @if(Storage::disk('local')->has($service->picture))
                                        <img  src="{{ route('AllImage', ['filename' => $service->picture]) }}"   alt="page">
                                    @else
                                        <img src="{{ asset('nowopen/img/post1.jpg') }}" alt="photo">
                                    @endif

                                </div>

                                <div class="post-content">
                                    <a href="{{ url('/servicedetails',['id' => $service->id]) }}" class="post-category bg-blue-light">TRAVELS & TOURISM</a>
                                    <a href="{{ url('/servicedetails',['id' => $service->id]) }}" class="h4 post-title">{{ $service->title }}</a>
                                    <p>{{ $service->description }}</p>
                                    <div class="row" style="margin-left: 2px;margin-right: 2px; border-top: rgba(45,49,41,0.35) 0.16em solid">
                                        <div class="col-lg-3"></div>
                                        <div class="col-lg-3 col-lg-offset-3"></div>
                                    </div>
                                </div>

                            </article>

                            <!-- ... end Post -->
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>


            <!-- Pagination -->

            <nav aria-label="Page navigation">
                {{ $services->render() }}
            </nav>

            <!-- ... end Pagination -->

        </section>

    </div>


@endsection


@extends('layouts.master')

@section('title')
    Product Details || NowOpen.ng
@endsection
@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <style>
        .ui-block {
            margin-bottom: 0;
        }
    </style>
    @include('includes.profiletopheader')
    <!-- Breadcurb Area -->

    <!-- Single Product details Area -->
    <div class="single-product-detaisl-area"  style="background: #edf2f6;width: ">
        <!-- Single Product View Area -->
        <div class="product-view-area" style="background: #edf2f6;">
            <br>
            <div class="container" style="background: white;max-width: 1000px">
                <div class="row" >
                    <div class="col-md-5 col-sm-11 col-xs-11">
                        <!-- Single Product View -->
                        <div class="single-procuct-view">
                            <!-- Simple Lence Gallery Container -->
                            <div class="simpleLens-gallery-container" id="p-view">
                                <div class="simpleLens-container tab-content">
                                    <div class="tab-pane active" id="p-view-1">
                                        <div class="simpleLens-big-image-container">
                                            @if(Storage::disk('local')->has($product->logo))
                                                <a class="simpleLens-lens-image" data-lens-image="{{ route('AllImage', ['filename' => $product->logo]) }}">
                                                        <img class="simpleLens-big-image"  src="{{ route('AllImage', ['filename' => $product->logo]) }}"   alt="product">
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-2">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/2.jpg">
                                                <img src="img/product/single-product/medium/2.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-3">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/3.jpg">
                                                <img src="img/product/single-product/medium/3.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-4">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/4.jpg">
                                                <img src="img/product/single-product/medium/4.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-5">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/1.jpg">
                                                <img src="img/product/single-product/medium/1.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-6">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/2.jpg">
                                                <img src="img/product/single-product/medium/2.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-7">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/3.jpg">
                                                <img src="img/product/single-product/medium/3.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="p-view-8">
                                        <div class="simpleLens-big-image-container">
                                            <a class="simpleLens-lens-image" data-lens-image="img/product/single-product/large/4.jpg">
                                                <img src="img/product/single-product/medium/4.jpg" class="simpleLens-big-image" alt="productd">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End Simple Lence Gallery Container -->
                        </div><!-- End Single Product View -->
                    </div>
                    <div class="col-md-7 col-sm-8 col-xs-12" style="padding-left: 55px">
                        <!-- Single Product Content View -->
                        <div class="single-product-content-view" style="margin-left: 15px">
                            <br>
                            <b style="color: black;font-weight: bold">Product Information</b>
                            <br><br>
                            <div class="row">
                                <h3 style="color: black;font-weight: bold;" class="col-lg-8">{{ $product->name }}</h3>
                                <h3 style="float: right" class="col-lg-4">N {{ $product->price }}</h3>
                            </div>

                            <div class="row" style="margin-top: 70px">
                                <div class="col-lg-12">
                                    <p >
                                        {{ $product->description }}
                                    </p>
                                </div>
                                <div class="col-lg-12" style="margin-top: 40px">
                                    <form style="">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <input class="form-control" style="height: 40px">
                                            </div>
                                            <div class="col-lg-7">
                                                <input class="form-control" style="height: 40px">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <button style="height: 40px; width: 100%" class="btn btn-purple">order</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End Single Product Content View -->
                    </div>
                </div>

                <div class="row" style="margin-top: 100px;margin-bottom: 50px">
                    <div class="col-md-7 col-lg-offset-3">
                        <h3 style="color: black;font-weight: bold;text-align: center" >{{ $product->name }}</h3>
                        <!-- Tab Content -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="product-des">
                                <p>{{ $product->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Single Product View Area -->
        <!-- Single Description Tab -->
        <!-- Related Product area area -->
        <div class="product-area" >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Product Top Bar -->
                        <div class="row">
                            <div class="col-lg-11" style="padding-top: 20px;margin-left: 40px">
                                <h5 style="width: 17%; margin-left: 2px;font-size: 9px;margin-bottom: -2px;font-weight: bold;
                                border-bottom: 2px solid orange;height: 27px">RELATED PRODUCT</h5>
                                <hr style="width:95%;border: 0.02em solid rgba(211,217,196,0.35);margin-top: 0px">
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-11" style="margin-left: 20px">
                        <div class="container"><!-- Single Product Carousel-->
                            @forelse($products as $product)
                                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">

                                    <div class="ui-block video-item">
                                        <div class="video-player">
                                            @if(Storage::disk('local')->has($product->logo))
                                                <img  style="height: 150px;width: 250px;" src="{{ route('AllImage', ['filename' => $product->logo]) }}"   alt="product">
                                            @endif
                                            <div class="overlay overlay-dark"></div>

                                            <div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></div>
                                        </div>

                                        <div class="ui-block-content video-content" style="padding-top: 0px;padding-left: 10px;height: 70px;padding-right: 10px">
                                            <div style="display: block">
                                                <a href="{{ url('productdetails', ['id' => $product->id]) }}" class="" style="float: left;width: 83%">
                                                    <h4 style="color: #0b0b0b;font-weight: bold;font-size: 11px"> {{$product->name}}</h4></a>

                                            </div>
                                            <br>
                                        </div>
                                    </div>

                                </div>
                            @empty

                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Product area -->
        <br><br> <br><br> <br><br> <br><br>
    </div><!-- End Single Product details Area -->
    <!-- Brand Logo area -->



@endsection
<!DOCTYPE html>
<html lang="en">

<head>

    <title>NowOpen Main Landing Page</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Main Font -->
    <script src="{{ asset('/nowopen1/js/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/main.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/fonts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen/datepicker.css') }}">

   
    <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
    <script nomodule src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine-ie11.min.js" defer></script>

    

    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>

</head>

<body>



<div class="main-header main-landing">

    <div class="content-bg-wrap">
        <div class="content-bg bg-landing"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 m-auto col-md-12 col-sm-12 col-xs-12">
                <div class="main-header-content">

                    <a href="#" class="logo">
                        <div class="img-wrap">
                            <img src="{{ asset('/nowopen1/img/logo-landing.png') }}" alt="NowOpen">
                        </div>
                        <div class="title-block">
                            <h6 class="logo-title">NOWOPEN</h6>
                            <div class="sub-title">VENDOR NETWORK</div>
                        </div>
                    </a>

                    <h1>Welcome to the Worlds Biggest and Largest Vendor Network</h1>
                    <p>Share your Business | Advertise | Reach your Audience</p>
                    <p>NowOpen is the first personalized aggregated brands listing offering exposures and activations for businesses and services from anywhere on the web.
                    </p>

                    <!-- <a data-scroll href="#" class="btn btn-purple btn-lg">Log In | Sign Up</a> -->

                    <div class="col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-xs-12">
                        <a href="#" class="btn btn-purple btn-lg" data-toggle="modal" data-target="#registration-login-form-popup">Log In | Sign Up</a>
                    </div>
                    <a data-scroll href="{{ url('/home') }}" class="btn btn-purpler btn-lg">Enter as Guest</a>
                </div>
            </div>
        </div>
    </div>
    <!--
        <img class="img-bottom" src="{"{ asset('/nowopen1/img/group-bottom.png') }}""alt="friends"> 
    <img class="img-rocket" src="{{ asset('/nowopen1/img/rocket.png') }}" alt="rocket"> -->
</div>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="ui-block responsive-flex1200">
                    <form method="get" action="{{ url('search') }}" class="ui-block-title">
                        {{ csrf_field() }}
                        <div class="w-select">
                            <div class="title">Filter By:</div>
                            <fieldset class="form-group">
                                <select class="selectpicker form-control" id="category" name="category">
                                    <option value="">All Categories</option>
                                    @foreach($cats as $cat)
                                        <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>
                        <div class="w-select" >
                            <fieldset class="form-group">
                                <select class="" name="type" id="type" style="padding: 10px 15px;font-size: 12px;height: 40px;margin-bottom: 0;">
                                    <option value="">Services (Descending)</option>
                                </select>
                            </fieldset>
                        </div>

                        <div class="w-select">
                            <fieldset class="form-group" name="location" id="location">
                                <select class="selectpicker form-control">
                                    <option value="">All Location</option>
                                    @foreach($states as $state)
                                        <option value="{{ $state->name }}">{{ $state->name }}</option>
                                    @endforeach
                                </select>
                            </fieldset>
                        </div>

                        <button type="submit" class="btn btn-blue btn-md-2">Filter Vendors</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container">
    <div class="row">
        <div class="col-lg-6 m-auto col-md-12 col-sm-12 col-xs-12">
            <div class="landing-main-content" id="items">
                <!-- <a target="_blank" href="https://themeforest.net/item/NowOpen-html-vendor-network-toolkit/19755363" class="btn btn-primary btn-lg">Buy Premium Packages & Services</a> -->
                <h2 class="title">NowOpen Opportunities</h2>
                <p>Check nowopen opportunities, services and brands!</p>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($cats as $cat)
        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-xs-6">
            <div class="landing-item">
                <div class="landing-item-thumb">
                    @if(Storage::disk('local')->has($cat->logo))
                        <img  src="{{ route('AllImage', ['filename' => $cat->logo]) }}"   alt="page">
                    @endif
                    <div class="overlay overlay-dark"></div>
                    <a  href="{{ url('/company',['id' => $cat->name]) }}" class="btn btn-breez btn-sm">Go to the Page</a>
                    <a  href="{{ url('/company',['id' => $cat->name]) }}" class="full-block"></a>
                </div>
                <div class="title">{{ $cat->name }}</div>
            </div>
        </div>
        @endforeach


    </div>
</div>


<!-- Section Call To Action Animation -->

<section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-xs-12">
                <a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#registration-login-form-popup">Register Now to NowOpen</a>
            </div>
        </div>
    </div>
    <img class="first-img" alt="guy" src="{{ asset('/nowopen1/img/guy.png') }}">
    <img class="second-img" alt="rocket" src="{{ asset('/nowopen1/img/rocket1.png') }}">
    <div class="content-bg-wrap">
        <div class="content-bg bg-section1"></div>
    </div>
</section>

<!-- ... end Section Call To Action Animation -->


<div class="modal fade" id="registration-login-form-popup">
    <div class="modal-dialog ui-block window-popup registration-login-form-popup">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
            <svg class="olymp-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
        </a>
        <div class="registration-login-form">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                        <svg class="olymp-login-icon"><use xlink:href="{{ asset('/nowopen1/svg-icons/sprites/icons.svg#olymp-login-icon') }}"></use></svg>
                    </a>
                    <i class="fa "></i>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                        <svg class="olymp-register-icon"><use xlink:href="{{ asset('/nowopen1/svg-icons/sprites/icons.svg#olymp-register-icon') }}"></use></svg>
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="home1" role="tabpanel" data-mh="log-tab">
                    <div class="title h6">Register to NowOpen</div>
                    <form class="content" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group label-floating is-empty {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label class="control-label">First Name</label>
                                    <input class="form-control" placeholder="" type="text" name="name" value="{{ old('name') }}" required autofocus>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group label-floating is-empty {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label class="control-label">Last Name</label>
                                    <input class="form-control" placeholder="" type="text" name="last_name" value="{{ old('last_name') }}" required >
                                </div>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="" type="email" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group label-floating is-empty {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Password</label>
                                    <input class="form-control" placeholder="" type="password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group label-floating is-empty {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label class="control-label">Confirm Password</label>
                                    <input class="form-control" placeholder="" type="password" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group date-time-picker label-floating {{ $errors->has('dob') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Birthday</label>
                                    <input name="dob" value="{{ old('dob') }}" id="datepicker"/>
                                    @if ($errors->has('dob'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                    @endif
                                    <span class="input-group-addon">
											<svg class="olymp-calendar-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-calendar-icon"></use></svg>
										</span>
                                </div>

                                <div class="form-group label-floating is-select {{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Gender</label>
                                    <select name="gender" class="selectpicker form-control">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="remember">
                                    <div class="checkbox">
                                        <label>
                                            <input name="optionsCheckboxes" type="checkbox">
                                            I accept the <a href="#">Terms and Conditions</a> of the website
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-purple btn-lg full-width" >Complete Registration!</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tab-pane" id="profile1" role="tabpanel" data-mh="log-tab">
                    <div class="title h6">Login to your Account</div>
                    <form class="content" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Email</label>
                                    <input class="form-control" placeholder="" type="email" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group label-floating is-empty {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label">Your Password</label>
                                    <input class="form-control" placeholder="" type="password" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="remember">

                                    <div class="checkbox">
                                        <label>
                                            <input name="optionsCheckboxes" type="checkbox">
                                            Remember Me
                                        </label>
                                    </div>
                                    <a href="#" class="forgot">Forgot my Password</a>
                                </div>

                              <button type="submit" class="btn btn-lg btn-primary full-width">Login</button>

                                <div class="or"></div>

                                <a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fa fa-facebook" aria-hidden="true"></i>Login with Facebook</a>

                                <a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fa fa-twitter" aria-hidden="true"></i>Login with Twitter</a>


                                <p>Don’t you have an account? <a href="#">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer area -->
<div class="footer-area footer footer-full-width">
    <!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 5px">


                    <!-- Widget About -->

                    <div class=" w-about" style="margin-top: 25px">

                        <a href="#" class="logo">
                            <div class="img-wrap">
                                <img style="height: 50px;width: 50px" src="{{ asset('/nowopen1/img/logo-colored.png') }}" alt="NowOpen">
                            </div>
                            <div class="title-block">
                                <h6 class="logo-title">NowOpen</h6>
                                <div class="sub-title">vendor NETWORK</div>
                            </div>
                        </a>
                        <p style="font-size: 12px">Share your Business | Advertise | Reach your Audience</p>
                    </div>

                    <!-- ... end Widget About -->

                </div>

                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <!-- Widget List -->

                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="{{ url('/') }}"><h6 class="title">Privacy</h6></a>
                            </li>
                            <li>
                                <a href="{{ url('/contact') }}"><h6 class="title">Contact Us</h6></a>
                            </li>
                        </ul>
                    </div>

                    <!-- ... end Widget List -->

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="#"><h6 class="title">About Us</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Offers</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">

                        <ul>
                            <li>
                                <a href="#"><h6 class="title">FAQ</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Advertising</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">


                    <div class="widget w-list">
                        <ul>
                            <li>
                                <a href="#"><h6 class="title">Terms & Conditions</h6></a>
                            </li>
                            <li>
                                <a href="#"><h6 class="title">Agency</h6></a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                    <!-- SUB Footer -->

                    <div class="sub-footer-copyright">
					<span style="font-size: 10px">
                        &copy; <a href="{{ url('/') }}">NOWOPEN | AEY International Limited </a>- All Rights Reserved 2018
					</span>
                    </div>

                    <!-- ... end SUB Footer -->

                </div>
            </div>
        </div>
    </div><!-- End Footer Top -->

</div><!-- End Footer area -->

<!-- ... end Footer Full Width -->



<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive">
    <div class="ui-block-title">
        <span class="icon-status online"></span>
        <h6 class="title" >Chat</h6>
        <div class="more">
            <svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
            <svg class="olymp-little-delete js-chat-open"><use xlink:href="svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
        </div>
    </div>
    <div class="mCustomScrollbar">
        <ul class="notification-list chat-message chat-message-field">
            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                </div>
            </li>

            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/author-page.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Don’t worry Mathilda!</span>
                    <span class="chat-message-item">I already bought everything</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
                </div>
            </li>

            <li>
                <div class="author-thumb">
                    <img src="{{ asset('/nowopen1/img/avatar14-sm.jpg') }}" alt="author" class="mCS_img_loaded">
                </div>
                <div class="notification-event">
                    <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                    <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                </div>
            </li>
        </ul>
    </div>

    <form class="need-validation">

        <div class="form-group label-floating is-empty">
            <label class="control-label">Press enter to post...</label>
            <textarea class="form-control" placeholder=""></textarea>
            <div class="add-options-message">
                <a href="#" class="options-message">
                    <svg class="olymp-computer-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
                </a>
                <div class="options-message smile-block">

                    <svg class="olymp-happy-sticker-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-happy-sticker-icon"></use></svg>

                    <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat1.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat2.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat3.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat4.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat5.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat6.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat7.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat8.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat9.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat10.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat11.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat12.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat13.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat14.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat15.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat16.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat17.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat18.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat19.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat20.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat21.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat22.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat23.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat24.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat25.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat26.png') }}" alt="icon">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{ asset('/nowopen1/img/icon-chat27.png') }}" alt="icon">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </form>


</div>

<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->



<a class="back-to-top" href="#">
    <img src="{{ asset('/nowopen1/svg-icons/back-to-top.svg') }}" alt="arrow" class="back-icon">
</a>



<!-- JS Scripts -->
<script src="{{ asset('/nowopen1/js/jquery-3.2.1.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('/nowopen1/js/jquery.appear.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('/nowopen1/js/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('/nowopen1/js/svgxuse.js') }}"></script>
<script src="{{ asset('/nowopen1/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Headroom.js') }}"></script>
<script src="{{ asset('/nowopen1/js/velocity.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ScrollMagic.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('/nowopen1/js/popper.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/material.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('/nowopen1/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('/nowopen1/js/selectize.js') }}"></script>
<script src="{{ asset('/nowopen1/js/swiper.jquery.js') }}"></script>
<script src="{{ asset('/nowopen1/js/moment.js') }}"></script>
<script src="{{ asset('/nowopen1/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/nowopen1/js/simplecalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/fullcalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/isotope.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ajax-pagination.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/chartjs-plugin-deferred.js') }}"></script>
<script src="{{ asset('/nowopen1/js/circle-progress.js') }}"></script>
<script src="{{ asset('/nowopen1/js/loader.js') }}"></script>
<script src="{{ asset('/nowopen1/js/run-chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.gifplayer.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-and-player.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-playlist-plugin.min.js') }}"></script>

<script src="{{ asset('/nowopen1/js/base-init.js') }}"></script>

<script src="{{ asset('/nowopen1/Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script>
    $('#category').on('change',function(e){
        var category = $('#category option:selected').attr('value');
        $.ajax({
            data: {category : category ,_token: '{{ csrf_token() }}'},
            url: '{{ route('listsubcat2') }}',
            method: 'get',
            success: function(data){
                $('#type').find('option').remove().end();
                $('#type').append($('<option>').text('Services (Descending)').attr('value', ''));
                $.each(data, function(i, value) {
                    $('#type').append($('<option>').text(value.name).attr('value', value.name));
                });

            },
            error: {}
        });

    });
</script>

</body>

</html>

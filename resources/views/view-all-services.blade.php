<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 8/8/2019
 * Time: 5:23 PM
 */
?>
@extends('layouts.slave')

@section('title')
    Home | View All Products
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Services</a></li>
            <li class="active">View All Services</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Services</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <b hidden id="database">services</b>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Category</th>
                                        <th width="30%">Description</th>
                                        <th width="10%">Status</th>
                                        <th width="10%">Action</th>
                                        <th width="10%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @foreach($services as $service)
                                        <tr>
                                            <td>{{ $service->id }}</td>
                                            <td valign="middle">{{ $service->title }}</td>
                                            <td valign="middle">{{ $service->type }} </td>
                                            <td valign="middle">{{ $service->category }} </td>
                                            <td valign="middle">{{ $service->description }} </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $service->status > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $service->id}}">
                                                    {{ $service->status > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $service->id,'database' => 'services']) }}"
                                                   class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                            <td valign="middle">
                                                <a href="{{ url('viewoneproduct',['id' => $service->id]) }}"  class="btn btn-default btn-sm">View</a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $services->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection

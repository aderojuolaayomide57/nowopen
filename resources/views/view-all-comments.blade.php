<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/14/2018
 * Time: 4:55 PM
 */
?>

@extends('layouts.slave')

@section('title')
    Home | View All Comment
@endsection

@section('content')

    <div id="main">


        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Comment</a></li>
            <li class="active">View All Comment</li>
        </ol>
        <!-- //breadcrumb-->

        <div id="content">

            <div class="row">

                <div class="col-lg-12">

                    <section class="panel">
                        <header class="panel-heading">
                            <h2><strong>Comment</strong></h2>
                            @if(Session::has('deletesuccess'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('deletesuccess') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools fully color" align="right"  data-toolscolor="#6CC3A0">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <b hidden id="database">comments</b>
                            <div class="table-responsive">
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>email</th>
                                        <th>message</th>
                                        <th width="30%">Action</th>
                                        <th width="30%">View</th>
                                    </tr>
                                    </thead>
                                    <tbody align="center">
                                    @foreach($comments as $comment)
                                        <tr>
                                            <td>{{ $comment->id }}</td>
                                            <td valign="middle">{{ $comment->name }}</td>
                                            <td valign="middle">{{ $comment->email }} </td>
                                            <td valign="middle">{{ $comment->message }} </td>
                                            <td valign="middle">
                                                <button id="disable"  class="btn  {{ $comment->active > 0  ? 'btn-success' : 'btn-danger'}} disable"  data-id="{{ $comment->id}}">
                                                    {{ $comment->active > 0  ? 'activate' : 'Deactivate'}}
                                                </button>
                                            </td>
                                            <td>
                                            <span class="tooltip-area">
                                                <a href="{{ url('ultimatedelete',['id' => $admin->id,'database' => 'comments']) }}"
                                                   class="btn btn-default btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
                                            </span>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                {{ $comments->render() }}
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
            <!-- //content > row-->

        </div>
        <!-- //content-->


    </div>

@endsection

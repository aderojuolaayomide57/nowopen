<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 8/16/2018
 * Time: 11:33 PM
 */
?>
@extends('layouts.slave')

@section('title')
    Nowopen |  View Company Details
@endsection

@section('content')
    <style type="text/css">
        .help-block strong {
            color: red;
        }
    </style>
    <div id="main">

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Company</a></li>
        </ol>
        <!-- //breadcrumb-->

        <div class="real-border">
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-lg-offset-2">

                    <section class="panel corner-flip">
                        <header class="panel-heading sm" data-color="theme-inverse">
                            <h2><strong>{{ $company->company_name }}</strong>  </h2>

                        </header>
                        <div class="panel-tools color" align="right" data-toolscolor="#4EA582">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                               <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div><div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4"><label> Name</label></div>
                                <div class="col-lg-6"> <p>{{ $company->company_name }}</p></div>
                            </div>

                        </div>
                    </section>
                    <!-- //account-wall-->

                </div>
                <!-- //col-sm-6 col-md-4 col-md-offset-4-->
            </div>
            <!-- //row-->
        </div>
        <!-- //content-->

    </div>
@endsection

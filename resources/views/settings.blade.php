
@extends('layouts.master')

@section('title')
    Profile Settings || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection

@section('content')
    <div style="background-color: #edf2f6;">
    @include('includes.profiletopheader')
        <style>
            .form-control{
                height: auto;
            }
            h6 {
                font-size: .1075rem;
                color: #515365;
                font-family: inherit;
                font-weight: 700;
                line-height: 1.3;
            }
            .card {
                position: relative;
                display: flex;
                -ms-flex-direction: column;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 1px solid #e6ecf5;
                border-radius: .25rem
            }

            .card > hr {
                margin-right: 0
            }

            .card > .list-group:first-child .list-group-item:first-child {
                border-top-left-radius: .25rem;
                border-top-right-radius: .25rem
            }

            .card > .list-group:last-child .list-group-item:last-child {
                border-bottom-right-radius: .25rem;
                border-bottom-left-radius: .25rem
            }

            .card-header, .card-subtitle, .card-text:last-child {
                margin-bottom: 0
            }

            .card-link:hover {
                text-decoration: none
            }

            .card-link + .card-link {
                margin-left: 1.25rem
            }

            .card-header {
                border-bottom: 1px solid #e6ecf5;
                padding: .15rem 1.25rem;
                background-color: #fff;
                height: 50px;
                padding-bottom: 50px;
            }

            .card-header:first-child {
                border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0
            }

            .card-header + .list-group .list-group-item:first-child {
                border-top: 0
            }

            .card-footer:last-child {
                border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px)
            }

            .alert, .btn .badge, .page-link {
                position: relative
            }

            .card-deck .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-deck .card {
                    display: -ms-flexbox;
                    display: flex;
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    -ms-flex-direction: column;
                    flex-direction: column;
                    margin-right: 15px;
                    margin-bottom: 0;
                    margin-left: 15px
                }
            }

            .card-group .card {
                margin-bottom: 15px
            }

            @media (min-width: 540px) {

                .card-group .card {
                    -ms-flex: 1 0 0%;
                    flex: 1 0 0%;
                    margin-bottom: 0
                }

                .card-group .card + .card {
                    margin-left: 0;
                    border-left: 0
                }

                .card-group .card:first-child {
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 0
                }

                .card-group .card:first-child .card-img-top {
                    border-top-right-radius: 0
                }

                .card-group .card:first-child .card-img-bottom {
                    border-bottom-right-radius: 0
                }

                .card-group .card:last-child {
                    border-top-left-radius: 0;
                    border-bottom-left-radius: 0
                }

                .card-group .card:last-child .card-img-top {
                    border-top-left-radius: 0
                }

                .card-group .card:last-child .card-img-bottom {
                    border-bottom-left-radius: 0
                }

                .card-group .card:only-child {
                    border-radius: .25rem
                }

                .card-group .card:only-child .card-img-top {
                    border-top-left-radius: .25rem;
                    border-top-right-radius: .25rem
                }

                .card-group .card:only-child .card-img-bottom {
                    border-bottom-right-radius: .25rem;
                    border-bottom-left-radius: .25rem
                }

                .card-group .card:not(:first-child):not(:last-child):not(:only-child), .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-bottom, .card-group .card:not(:first-child):not(:last-child):not(:only-child) .card-img-top {
                    border-radius: 0
                }

                .card-columns .card {
                    display: inline-block;
                    width: 100%
                }
            }

            .card-columns .card {
                margin-bottom: .75rem
            }
        </style>


    <!-- Your Account Personal Information -->

        <div class="container">
            <div class="row">
                <div class="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Personal Information</h6>
                            @if(Session::has('companyupdatesuccess'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('companyupdatesuccess') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('companyupdatefailed'))
                                <br>
                                <div class="alert-box">
                                    <h4 style="color: red;">{!! Session::get('companyupdatefailed') !!}</h4>
                                </div>
                            @endif
                        </div>
                        <div class="ui-block-content">


                            <!-- Personal Information Form  -->

                            <form method="post" action="{{ url('/updatecompany',['id' => $company->id]) }}">
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <div class="form-group label-floating {{ $errors->has('company_name') ? ' has-error' : '' }}">
                                            <label class="control-label">Company Name</label>
                                            <input id="company_name" type="text" class="form-control" name="company_name" value="{{ $company->company_name }}" required autofocus>
                                            @if ($errors->has('company_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Email</label>
                                            <input class="form-control" placeholder="" type="email" name="email" value="{{ $company->email }}" required>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('rcnumber') ? ' has-error' : '' }}">
                                            <label class="control-label">RC Number (Optional)</label>
                                            <input id="rcnumber" type="text" class="form-control" name="rcnumber" value="{{ $company->rcnumber }}" required>
                                            @if ($errors->has('rcnumber'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('rcnumber') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group date-time-picker label-floating {{ $errors->has('start_date') ? ' has-error' : '' }}">
                                            <label class="control-label">Establishement Year</label>
                                            <input id="datetimepicker"  name="start_date" value="{{ $company->start_date }}" required/>
                                            <span class="input-group-addon">
                                        <svg class="olymp-month-calendar-icon icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
                                    </span>
                                            @if ($errors->has('start_date'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('start_date') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating {{ $errors->has('mobile_no') ? ' has-error' : '' }}">
                                            <label class="control-label">Phone Number</label>
                                            <input id="mobile_no" type="number" class="form-control" name="mobile_no" value="{{ $company->mobile_no }}" required>
                                            @if ($errors->has('mobile_no'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile_no') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('website') ? ' has-error' : '' }}">
                                            <label class="control-label">Website</label>
                                            <input class="form-control"  type="text" value="{{ $company->website }}" name="website" required>
                                            @if ($errors->has('website'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('fax') ? ' has-error' : '' }}">
                                            <label class="control-label">Fax</label>
                                            <input class="form-control"  type="text" name="fax" value="{{ $company->fax }}" required>
                                            @if ($errors->has('fax'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                            @endif
                                        </div>


                                        <div class="form-group label-floating {{ $errors->has('address') ? ' has-error' : '' }}">
                                            <label class="control-label">Address (Enter full address of your company.)</label>
                                            <input class="form-control" type="text" name="address" value="{{ $company->address }}" required>
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-select {{ $errors->has('country') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Country</label>
                                            <select class="selectpicker form-control" name="country" required>
                                                <option value="{{ $company->country }}">{{ $company->country }}</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="South Africa">South Africa</option>
                                            </select>
                                            @if ($errors->has('country'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-select {{ $errors->has('state') ? ' has-error' : '' }}">
                                            <label class="control-label">Your State / Province</label>
                                            <select class="selectpicker form-control" name="state" required>
                                                <option value="{{ $company->state }}">{{ $company->state }}</option>
                                                <option value="Lagos">Lagos</option>
                                                <option value="Osun">Osun</option>
                                            </select>
                                            @if ($errors->has('state'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating is-select {{ $errors->has('city') ? ' has-error' : '' }}">
                                            <label class="control-label">Your City</label>
                                            <select class="selectpicker form-control" name="city" required>
                                                <option value="{{ $company->city }}">{{ $company->city }}</option>
                                                <option value="Ikoyi">Ikoyi</option>
                                                <option value="Alimosho">Alimosho</option>
                                            </select>
                                            @if ($errors->has('city'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating {{ $errors->has('about') ? ' has-error' : '' }}">
                                            <label class="control-label">About Company</label>
                                            <textarea class="form-control" placeholder="" name="about" required>{{ $company->about }}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating is-select {{ $errors->has('category') ? ' has-error' : '' }}">
                                            <label class="control-label">Business Category</label>
                                            <select class="selectpicker form-control" name="category" id="category" required>
                                                <option value="{{ $company->category }}">{{ $company->category }}</option>
                                                @foreach($cats as $cat)
                                                    <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('category'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('opening') ? ' has-error' : '' }}">
                                            <label class="control-label">Opening Time</label>
                                            <input class="form-control" placeholder="" id="timepicker" type="text" name="opening" value="{{ $company->opening }}" required>
                                            @if ($errors->has('opening'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('opening') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group label-floating {{ $errors->has('employees') ? ' has-error' : '' }}">
                                            <label class="control-label">Employees</label>
                                            <select class="selectpicker form-control" name="employees" required>
                                                <option value="{{ $company->employees }}">{{$company->employees }}</option>
                                                <option value="MA">Choose...</option>
                                                <option value="1-5">1-5</option>
                                                <option value="6-10">6-10</option>
                                                <option value="11-15">11-15</option>
                                                <option value="16-25">16-25</option>
                                                <option value="26-50">26-50</option>
                                                <option value="51-100">51-100</option>
                                                <option value="101-200">101-200</option>
                                                <option value="201-500">201-500</option>
                                            </select>
                                            @if ($errors->has('employees'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('employees') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('manager') ? ' has-error' : '' }}">
                                            <label class="control-label">Company Manager</label>
                                            <input class="form-control" name="manager" type="text" value="{{ $company->manager }}" required>
                                            @if ($errors->has('manager'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('manager') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating is-select {{ $errors->has('type') ? ' has-error' : '' }}">
                                            <label class="control-label">Service Type</label>
                                            <select class=" form-control" name="type" id="type" required style="height: 53px;">
                                                <option value="{{ $company->type }}">{{ $company->type }}</option>
                                                <option value="MA">Select...</option>
                                            </select>
                                            @if ($errors->has('type'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group label-floating {{ $errors->has('closing') ? ' has-error' : '' }}">
                                            <label class="control-label">Closing Time</label>
                                            <input class="form-control" placeholder="" id="timepicker1"  name="closing" type="text" value="{{ $company->closing }}">
                                            @if ($errors->has('closing'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('closing') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group with-icon label-floating {{ $errors->has('facebook') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Facebook Account</label>
                                            <input class="form-control" type="text" name="facebook" value="{{ $company->facebook }}" required>
                                            <i class="fa fa-facebook c-facebook" aria-hidden="true"></i>
                                            @if ($errors->has('facebook'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group with-icon label-floating {{ $errors->has('twitter') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Twitter Account</label>
                                            <input class="form-control" type="text" name="twitter" value="{{ $company->twitter }}" required>
                                            <i class="fa fa-twitter c-twitter" aria-hidden="true"></i>
                                            @if ($errors->has('twitter'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group with-icon label-floating {{ $errors->has('instagram') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Instagram Account</label>
                                            <input class="form-control" type="text" name="instagram" value="{{ $company->instagram }}" required>
                                            <i class="fa fa-instagram c-instagram" aria-hidden="true"></i>
                                            @if ($errors->has('instagram'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('instagram') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group with-icon label-floating {{ $errors->has('youtube') ? ' has-error' : '' }}">
                                            <label class="control-label">Your YouTube Account</label>
                                            <input class="form-control" type="text" name="youtube" value="{{ $company->youtube }}" required>
                                            <i class="fa fa-youtube c-youtube" aria-hidden="true"></i>
                                            @if ($errors->has('youtube'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('youtube') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group with-icon label-floating {{ $errors->has('spotify') ? ' has-error' : '' }}" >
                                            <label class="control-label">Your Spotify Account</label>
                                            <input class="form-control" type="text" name="spotify" value="{{ $company->spotify }}" required>
                                            <i class="fa fa-spotify c-spotify" aria-hidden="true"></i>
                                            @if ($errors->has('spotify'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('spotify') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button type="submit" class="btn btn-secondary btn-lg full-width">Return to Homepage</button>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <button type="submit" class="btn btn-primary btn-lg full-width">Save & Continue</button>
                                    </div>

                                </div>
                            </form>

                            <!-- ... end Personal Information Form  -->
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
                    <div class="ui-block">



                        <!-- Your Profile  -->

                        @include('includes.settings-tab')

                        <!-- ... end Your Profile  -->


                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Your Account Personal Information -->

        <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme" >

            <img class="first-img" alt="guy" src="{{ asset('nowopen1/img/guy.png') }}">
            <img class="second-img" alt="rocket" src="{{ asset('nowopen1img/rocket1.png') }}">
            <div class="content-bg-wrap" >
                <div class="content-bg bg-section1" style="background-color: #edf2f6;"></div>
            </div>
        </section>

        <!-- ... end Section Call To Action Animation -->

        <!-- Window-popup Update Header Photo -->


    </div>




@endsection

@section('profilefooter')
    @include('includes.profilefooter')
@endsection
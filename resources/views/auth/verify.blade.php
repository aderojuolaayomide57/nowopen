<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>NowOpen - Sign Up</title>

    <!-- Required meta tags always come first -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Main Font -->
    <script src="{{ asset('/nowopen1/js/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>




    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/main.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen1/css/fonts.min.css') }}">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.css">


</head>
<body>

<!-- Header Standard -->

<div class="header--standard" id="header--standard">
    <div class="container">
        <div class="header--standard-wrap">

             <a href="{{ url('/') }}" class="logo">
                <div class="img-wrap">
                    <img src="{{ asset('/nowopen1/img/logo-colored-small.png') }}" alt="Olympus">
                </div>
                <div class="title-block">
                    <h6 class="logo-title">NOWOPEN</h6>
                    <div class="sub-title">VENDOR NETWORK</div>
                </div>
            </a>

            <a href="#" class="open-responsive-menu js-open-responsive-menu">
                <svg class="olymp-menu-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
            </a>

            <div class="nav nav-pills nav1 header-menu">
                <div class="mCustomScrollbar">
                    <ul>
                        <li class="nav-item">
                            <a href="{{ url('/home') }}" class="nav-link">Back to Home Page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Header Standard  --><br><br><br><br><br><br><br>


<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2 class="presentation-margin">Account Verification </h2>
                @if(Session::has('companysuccess'))
                    <div class="alert-box">
                        <h4 style="color: green;">{!! Session::get('companysuccess') !!}</h4>
                    </div>
                @endif
                @if(Session::has('companyfailed'))
                    <div class="alert-box">
                        <h4 style="color: red;">{!! Session::get('companyfailed') !!}</h4>
                    </div>
                @endif
            <div class="ui-block">

                <div class="ui-block-content">
                    
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                                    <div class="card-body">
                                        @if (session('resent'))
                                            <div class="alert alert-success" role="alert">
                                                {{ __('A fresh verification link has been sent to your email address.') }}
                                            </div>
                                        @endif

                                        {{ __('Before proceeding, please check your email for a verification link.') }}
                                        {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<a class="back-to-top" href="#">
    <img src="{{ asset('/nowopen1/svg-icons/back-to-top.svg') }}" alt="arrow" class="back-icon">
</a>



<!-- JS Scripts -->
<script src="{{ asset('/nowopen1/js/jquery-3.2.1.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.appear.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('/nowopen1/js/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('/nowopen1/js/svgxuse.js') }}"></script>
<script src="{{ asset('/nowopen1/js/imagesloaded.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Headroom.js') }}"></script>
<script src="{{ asset('/nowopen1/js/velocity.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ScrollMagic.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.waypoints.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.countTo.js') }}"></script>
<script src="{{ asset('/nowopen1/js/popper.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/material.min.js') }}"></script>
<script src="{{ asset('/nowopen1/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('/nowopen1/js/smooth-scroll.js') }}"></script>
<script src="{{ asset('/nowopen1/js/selectize.js') }}"></script>
<script src="{{ asset('/nowopen1/js/swiper.jquery.js') }}"></script>
<script src="{{ asset('/nowopen1/js/moment.js') }}"></script>
<script src="{{ asset('/nowopen1/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/nowopen1/js/simplecalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/fullcalendar.js') }}"></script>
<script src="{{ asset('/nowopen1/js/isotope.pkgd.js') }}"></script>
<script src="{{ asset('/nowopen1/js/ajax-pagination.js') }}"></script>
<script src="{{ asset('/nowopen1/js/Chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/chartjs-plugin-deferred.js') }}"></script>
<script src="{{ asset('/nowopen1/js/circle-progress.js') }}"></script>
<script src="{{ asset('/nowopen1/js/loader.js') }}"></script>
<script src="{{ asset('/nowopen1/js/run-chart.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.magnific-popup.js') }}"></script>
<script src="{{ asset('/nowopen1/js/jquery.gifplayer.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-and-player.js') }}"></script>
<script src="{{ asset('/nowopen1/js/mediaelement-playlist-plugin.min.js') }}"></script>

<script src="{{ asset('/nowopen1/js/base-init.js') }}"></script>

<script src="{{ asset('/nowopen1/Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/timepicker@1.11.12/jquery.timepicker.js"></script>

</body>

</html>





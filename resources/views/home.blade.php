
@extends('layouts.master')

@section('title')
    Home || NowOpen.ng
@endsection

@section('profileheader')
    @include('includes.profileheader')
@endsection


@section('content')

<style>
    .loader {
        height: 500px;
    }
    .loader >img {
        position: relative;
        left: 47%;
        margin-top: auto;
        margin-bottom: auto;
    }
</style>
<script>
  function companySearch() {
    return {
      // other default 
      category: '',
      service: '',
      state: '',
      isLoading: false,
      image: false,
      items: [{'name': 'ayo','name': 'ola', 'name': 'james'}],
      search: false,
      open: true,
      count: 0,
      formData: null,
      companys: {},
      fetchCompany() {
        this.isLoading = true;
        this.image = true;
        this.open = false;
        this.formData = { category: this.category, service: this.service, state: this.state};
        fetch(`http://localhost:8000/homesearch`,
            {
                headers: {
                    'Content-Type': 'application/json;charset=utf-8',
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                body: JSON.stringify(this.formData)
            })
          .then(response => response.json())
          .then(result => {
            this.search = true;
            this.isLoading = false;
            this.image = false;
            this.companys = result;
            
            console.log(this.companys);
            
          });
          //console.log(this.formData);
      }
    }
  }
</script>  
    <!-- Main Slider Area -->
    <div class="main-slider-area" x-data="companySearch()"  >
        <!-- Main Slider -->
        <div class="main-slider">

            <div class="slider">
                <div id="mainSlider" class="nivoSlider slider-image">
                    <?php $i=1; ?>
                    @forelse($headers as $header)
                        @if(Storage::disk('local')->has($header->picture))
                            <img src="{{ route('AllImage', ['filename' => $header->picture]) }}"  alt="main slider" title="#htmlcaption1"/>
                        @endif

                    @empty
                            <img src="{{ asset('/nowopen/img/slider/slider11.jpg') }}" alt="main slider" title="#htmlcaption2"/>
                            <img src="{{ asset('/nowopen/img/slider/slider12.jpg') }}" alt="main slider" title="#htmlcaption3"/>
                            <img src="{{ asset('/nowopen/img/slider/slider13.jpg') }}" alt="main slider" title="#htmlcaption4"/>
                            <img src="{{ asset('/nowopen/img/slider/slider14.jpg') }}" alt="main slider" title="#htmlcaption5"/>
                    @endforelse
                </div>

            </div>
            <div class="">
                <div class="onmedia" style="padding: 20px;font-size: 12px;height: 80px;margin-bottom: 0;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="responsive-flex1200">
                                <form method="get" action="" class="form-inline">
                                    <div class="row">
                                        <div class="col-lg-1" style="padding: 5px;margin-top: 10px">
                                            <div class="title" style="font-size: 10px;">FILTER SEARCH BY:</div>
                                        </div>
                                        <div class="col-lg-2 form-group" style="padding: 5px;">
                                            <select class="form-control" id="category" name="category" x-model="category" style="padding: 10px 15px;font-size: 10px;height: 40px;margin-bottom: 0;width: 100%;">
                                                <option value="">All Categories</option>
                                                @foreach($cats as $cat)
                                                    <option value="{{ $cat->name }}">{{ $cat->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="col-lg-2 form-group" style="padding: 5px;">
                                            <select class="form-control" name="service" id="service" x-model="service" style="padding: 10px 15px;font-size: 10px;height: 40px;margin-bottom: 0;width: 100%;">
                                                <option value="">All Services (Descending)</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 form-group" style="padding: 5px;">
                                            <select class="form-control" name="state" id="state" x-model="state"  style="padding: 10px 15px;font-size: 10px;height: 40px;margin-bottom: 0;width: 100%;">
                                                <option value="">State </option>
                                                @foreach($states as $state)
                                                    <option value="{{ $state->name }}">{{ $state->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-2 form-group" style="padding: 5px;">
                                            <select class="form-control" name="city" id="city" x-model="city" style="padding: 10px 15px;font-size: 12px;height: 40px;margin-bottom: 0;width: 100%;">
                                                <option value="">City</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 form-group" style="padding: 5px;">
                                            <select class="form-control" name="opening" id="opening" x-model="opening" style="padding: 10px 15px;font-size: 10px;height: 40px;width: 100%;margin-bottom: 0;">
                                                <option value="">Time (Available)</option>
                                                <option value="7:00am">7:00am</option>
                                                <option value="8:00am">8:00am</option>
                                                <option value="9:00am">9:00am</option>
                                                <option value="10:00am">10:00am</option>
                                                <option value="11:00am">11:00am</option>
                                                <option value="12:00pm">12:00pm</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1" style="padding: 5px;">
                                            <button  @click="{fetchCompany() }" :disabled="isLoading" type="button" style="font-weight: bold;font-size: 10px;height: 39px" class="btn btn-purple btn-md-1">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=""  style="background: #f9f9f9;"  >
                <br>
                <div class="loader" x-show="image">
                    
                    <img src="{{ asset('/nowopen/img/loading.gif') }}" alt="" title="#"/>
                </div>
                <div class="brand-products-area"  x-show="search">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="brand-products brand-product-shoes c-carousel-button">
                                    <div class="row">
                                        <div class="container" style="width: 100%; padding: 20px">

                                            <h3 style="float: left;width: 20%; margin-left: 15px;font-size: 12px">SEARCH RECORD (0)</h3>
                                            <hr style="float: right;width:78%;margin-bottom: -10px;border: 0.04em solid rgba(211,217,196,0.35)">
                                        </div>

                                    </div>

                                </div>

                            </div><!-- End Brand Product Column -->
                        </div>

                    </div>
                    <div class="container">
                        <div class="container"><!-- Single Product Carousel-->
                            <div id="" class="row">
                                  @include('includes.company-cardjs')
                            </div><!-- End Single Product Carousel-->
                        </div>
                    </div>
                </div><!-- Search history -->
                <div class="brand-products-area"  x-show="open">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="brand-products brand-product-shoes c-carousel-button">
                                    <div class="row">
                                        <div class="container" style="width: 100%; padding: 20px">

                                            <h3 style="float: left;width: 20%; margin-left: 15px;font-size: 12px">BESTSELLER BRANDS & SERVICES</h3>
                                            <hr style="float: right;width:78%;margin-bottom: -10px;border: 0.04em solid rgba(211,217,196,0.35)">
                                        </div>

                                    </div>

                                </div>

                            </div><!-- End Brand Product Column -->
                        </div>

                    </div>
                    <div class="container">
                        <div class="container"><!-- Single Product Carousel-->
                            <div id="" class="row">
                                @forelse($basics as $company)
                                    <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            @include('includes.company-cards2')
                                        </div>

                                    </a>
                                @empty

                                @endforelse
                            </div><!-- End Single Product Carousel-->
                        </div>
                    </div>
                </div><!-- End Brand Product area -->
                <div class="brand-products-area" x-show="open">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="brand-products brand-product-shoes c-carousel-button">
                                    <div class="row">
                                        <div class="container" style="width: 100%; padding-bottom: 20px">
                                            <h3 style="float: left;width: 15%; margin-left: 15px;font-size: 12px">NEW BRANDS & SERVICES</h3>
                                            <hr style="float: right;width:83%;border: 0.04em solid rgba(211,217,196,0.35)">
                                        </div>

                                    </div>

                                </div>

                            </div><!-- End Brand Product Column -->
                        </div>

                    </div>
                    <div class="container">
                        <div class="container"><!-- Single Product Carousel-->
                            <div id="" class="row">
                                @forelse($users as $company)
                                    <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            @include('includes.company-cards2')
                                        </div>

                                    </a>
                                @empty

                                @endforelse
                            </div><!-- End Single Product Carousel-->
                        </div>
                    </div>
                </div><!-- End Brand Product area -->


                <!-- Product area -->
                <div class="product-area" x-show="open">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Product Top Bar -->
                                <div class="row">
                                    <div class="container" style="width: 100%; padding-bottom: 20px">
                                        <h3 style="float: left;width: 17%; margin-left: 15px;font-size: 12px">All BRANDS AND SERVICES</h3>
                                        <hr style="float: right;width:81.5%;border: 0.02em solid rgba(211,217,196,0.35)">
                                    </div>

                                </div>
                            </div>
                            <div class="container">
                                <div class="container"><!-- Single Product Carousel-->
                                    <div id="" class="row">
                                        @forelse($users as $company)
                                            <a href="{{ url('myprofile',['id' => $company->id]) }}">
                                                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    @include('includes.company-cards2')
                                                </div>

                                            </a>
                                        @empty

                                        @endforelse
                                    </div><!-- End Single Product Carousel-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End Product area -->

            </div>

        </div>
    </div>


@endsection
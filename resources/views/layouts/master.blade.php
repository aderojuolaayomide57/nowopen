<?php
/**
 * Created by PhpStorm.
 * User: ayomide
 * Date: 6/9/2018
 * Time: 5:23 PM
 */
?><!doctype html>
<html class="no-js" lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon
    ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/nowopen/img/logo/fav.png') }}">

    <!-- Google Fonts
    ============================================ -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>

    <link href="{{ asset('/nowopen/css/mystyle.css') }}" rel="stylesheet" >

    <!-- Bootstrap CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/bootstrap.min.css') }}">
    <!-- Font Awesome CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/font-awesome.min.css') }}">
    <!-- Mean Menu CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/meanmenu.min.css') }}">


    <!-- owl.carousel CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('/nowopen/css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/nowopen/css/owl.transitions.css') }}">
    <!-- nivo-slider css
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/nivo-slider.css') }}">
    <!-- Price slider css
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/jquery-ui-slider.css') }}">
    <!-- Simple Lence css
    ============================================ -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen/css/jquery.simpleLens.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/nowopen/css/jquery.simpleGallery.css') }}">
    <!-- animate CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/animate.css') }}">
    <!-- normalize CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/normalize.css') }}">
    <!-- main CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/main.css') }}">
    <!-- style CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/style.css') }}">
    <!-- responsive CSS
    ============================================ -->
    <link rel="stylesheet" href="{{ asset('/nowopen/css/responsive.css') }}">
    <!-- modernizr JS

    ============================================ -->

    <script src="{{ asset('/nowopen/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.2/dist/alpine.min.js"></script>
    <script nomodule src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.0.2/dist/alpine-ie11.min.js" defer></script>


    @yield('profileheader')

</head>
<body >
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
            <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- MAIN -->
        
        @include('includes.header1')
        @yield('content')
        @include('includes.footer1')



        <script src="{{ asset('/nowopen1/js/jquery-3.2.1.js') }}"></script>

        <script src="{{ asset('/nowopen/js/myjs.js') }}"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- bootstrap JS
        ============================================ -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <!-- nivo slider js
        ============================================ -->
        <script src="{{ asset('/nowopen/js/jquery.nivo.slider.pack.js') }}"></script>
        <!-- Mean Menu js
        ============================================ -->
        <script src="{{ asset('/nowopen/js/jquery.meanmenu.min.js') }}"></script>
        <!-- wow JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/wow.min.js') }}"></script>
        <!-- price-slider JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/jquery-price-slider.js') }}"></script>
        <!-- Simple Lence JS
        ============================================ -->
        <script type="text/javascript" src="{{ asset('/nowopen/js/jquery.simpleGallery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/nowopen/js/jquery.simpleLens.min.js') }}"></script>
        <!-- owl.carousel JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/owl.carousel.min.js') }}"></script>
        <!-- scrollUp JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/jquery.scrollUp.min.js') }}"></script>
        <!-- jquery Collapse JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/jquery.collapse.js') }}"></script>
        <!-- plugins JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/plugins.js') }}"></script>
        <!-- main JS
        ============================================ -->
        <script src="{{ asset('/nowopen/js/main.js') }}"></script>



        @yield('profilefooter')
        <script>

            function UploadPreview() {
                var id=document.getElementById("getid").textContent;
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("upload").files[0]);


                oFReader.onload = function (oFREvent) {
                    document.getElementById("uploadPreview").src = oFREvent.target.result;
                    $.ajax({
                        data: {image : oFREvent.target.result ,id : id,_token: '{{ csrf_token() }}'},
                        url: '{{ route('uploadcompanyimage') }}',
                        method: 'POST',
                        success: function(data){
                            alert(data);
                        },
                        error: {}
                    });
                };


            }

            function UploadPreview2() {
                var id=document.getElementById("getid").textContent;
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("upload2").files[0]);


                oFReader.onload = function (oFREvent) {
                    document.getElementById("uploadPreview2").src = oFREvent.target.result;
                    $.ajax({
                        data: {image : oFREvent.target.result ,id : id,_token: '{{ csrf_token() }}'},
                        url: '{{ route('uploadcompanyheader') }}',
                        method: 'POST',
                        success: function(data){
                            alert(id);
                        },
                        error: {}
                    });
                };


            }

            $(document).ready(function() {
                //alert('hello')
                //Login animation to center
                $('#tabrev').on('click', function () {
                    $('#product-tag2').hide();
                    $('#product-des').hide();
                    $('#tabdes1').removeClass('active');
                    $('#tabtag1').removeClass('active');
                    $('#product-rev1').show();
                    $('#tabrev1').addClass('active');
                });
                $('#tabdes').on('click', function () {
                    $('#product-tag2').hide();
                    $('#product-des').show();
                    $('#product-rev1').hide();
                    $('#tabdes1').addClass('active');
                    $('#tabtag1').removeClass('active');
                    $('#tabrev1').removeClass('active');
                });

                $('#shop1').on('click', function () {
                    $('#shop-list').hide();
                    $('#shop-list').removeClass('active');
                    $('#shop-product').show();
                    $('#shop-product').addClass('active');
                });
                $('#shop2').on('click', function () {
                    $('#shop-list').show();
                    $('#shop-list').addClass('active');
                    $('#shop-product').hide();
                    $('#shop-product').removeClass('active');
                });

                $(document).ready(function(){

                    $('#upload221').change(function(e){

                        var fileName = e.target.files[0].name;

                        document.getElementById("file_selected").textContent = fileName;

                        //alert('The file "' + fileName +  '" has been selected.');

                    });

                });


            });

            $('#category').on('change',function(e){
                var category = $('#category option:selected').attr('value');
                $.ajax({
                    data: {category : category ,_token: '{{ csrf_token() }}'},
                    url: '{{ route('listsubcat2') }}',
                    method: 'get',
                    success: function(data){
                        $('#service').find('option').remove().end();
                        $('#service').append($('<option>').text('Services (Descending)').attr('value', ''));
                        $.each(data, function(i, value) {
                            $('#service').append($('<option>').text(value.name).attr('value', value.name));
                        });
                    },
                    error: {}
                });
            });

            $('#state').on('change',function(e){
                var state = $('#state option:selected').attr('value');
                $.ajax({
                    data: {state : state ,_token: '{{ csrf_token() }}'},
                    url: '{{ route('listcity') }}',
                    method: 'get',
                    success: function(data){
                        $('#city').find('option').remove().end();
                        $('#city').append($('<option>').text('city (Descending)').attr('value', ''));
                        $.each(data, function(i, value) {
                            $('#city').append($('<option>').text(value.name).attr('value', value.name));
                        });
                    },
                    error: {}
                });
            });


        </script>


</body>
</html>


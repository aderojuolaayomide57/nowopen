<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 9/13/2018
 * Time: 3:26 PM
 */
?>
@extends('layouts.slave')

@section('title')
    Nowopen |  Add Advert
@endsection

@section('content')
    <style type="text/css">
        .help-block strong {
            color: red;
        }
    </style>
    <div id="main">

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Add Advert</li>
        </ol>
        <!-- //breadcrumb-->

        <div class="real-border">
            <div class="row">
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
                <div class="col-xs-1"></div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-lg-offset-2">

                    <section class="panel corner-flip">
                        <header class="panel-heading sm" data-color="theme-inverse">
                            <h2><strong>Add</strong> Advert</h2>
                            @if(Session::has('saveadverts'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('saveadverts') !!}</h4>
                                </div>
                            @endif
                            @if(Session::has('advertfail'))
                                <div class="alert-box">
                                    <h4 style="color: green;">{!! Session::get('advertfail') !!}</h4>
                                </div>
                            @endif
                        </header>
                        <div class="panel-tools color" align="right" data-toolscolor="#4EA582">
                            <ul class="tooltip-area">
                                <li><a href="javascript:void(0)" class="btn btn-collapse" title="Collapse"><i class="fa fa-sort-amount-asc"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-reload"  title="Reload"><i class="fa fa-retweet"></i></a></li>
                                <li><a href="javascript:void(0)" class="btn btn-close" title="Close"><i class="fa fa-times"></i></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" data-collabel="3" data-alignlabel="left"
                                  method="post" action="{{ url('/saveadvert') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label">Sponsor Name</label>
                                    <div>
                                        <div class="input-icon right"> <i class="fa fa-keyboard-o ico "></i>
                                            <input class="form-control " name="sponsor" type="text" placeholder="Sponsor Name" value="{{ old('sponsor') }}">
                                            @if ($errors->has('sponsor'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('sponsor') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Choose Advert Page</label>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <select  class="selectpicker form-control" name="page" data-size="10" data-live-search="true">
                                                    <option value="">Live search </option>
                                                    <option value="1">Home</option>
                                                    <option value="2">Welcome</option>
                                                    <option value="3">Search page</option>
                                                    <option value="4">Product Detail</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                @if ($errors->has('page'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('page') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- //form-group-->
                                <div class="form-group">
                                    <label class="control-label">Choose Advert Type</label>
                                    <div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <select  class="selectpicker form-control" name="type" data-size="10" data-live-search="true">
                                                    <option value="">Live search </option>
                                                    <option value="Welcome">Welcome Image (1920w X 639h)</option>
                                                    <option value="Header">Header Image Home (1920w X 639h)</option>
                                                    <option value="Middle Left">Middle Left Image Home (281w X 311h)</option>
                                                    <option value="Middle Right Top">Middle Right Top Image Home (360w X 146h)</option>
                                                    <option value="Middle Right Bottom 1">Middle Right Bottom First Image Home (181w X 150h)</option>
                                                    <option value="Middle Right Bottom 2">Middle Right Bottom Second Image Home (181w X 150h)</option>
                                                    <option value="Middle Bottom">Middle Bottom Image Home (1140w X 180h)</option>
                                                    <option value="Search page">Side Image page (263w X 328h)</option>
                                                    <option value="Product Detail">Product Detail page (271w X 552h)</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                @if ($errors->has('type'))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- //form-group-->

                                <div class="form-group">
                                    <label class="control-label">Add Image</label>
                                    <div>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img data-src="assets/plugins/holder/holder.js/100%x100%/text:Preview" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                            <!-- //fileinput-preview-->
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="image" required>
                                                </span>
                                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-trash-o"></i> Remove
                                                </a>
                                            </div>
                                        </div><!-- //fileinput-->

                                    </div>
                                </div><!-- //form-group-->

                                <div class="form-group offset">
                                    <div>
                                        <button type="submit" class="btn btn-theme">Submit</button>
                                        <button type="reset" class="btn">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    <!-- //account-wall-->

                </div>

                <!-- //col-sm-6 col-md-4 col-md-offset-4-->
            </div>
            <!-- //row-->
        </div>
        <!-- //content-->

    </div>

@endsection


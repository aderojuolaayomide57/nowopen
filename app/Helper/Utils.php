<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 1/25/2019
 * Time: 5:10 PM
 */

namespace App\Helpers;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;

use App\Models\Invoice;

class Utils
{
    public function getVerificationLink(){
        return URL::temporarySignedRoute(
          'verification.verify',
          Carbon::now()->addMinutes(60),
          ['id' => $notifiable->getKey()]
      );
    }

    public function sendmail($user){
        $verifyurl = $this->getVerificationLink();
        $imageurl = url('/nowopen/img/logo/logo.png');
        $key = 'a83cbf8f1bb72bedec3063de4c1998cb-7bbbcb78-a2febff4';
        $domain= 'www.nowopen.com.ng';
        $msg .="<body>";
        $msg .= "<img src='{$imageurl}' alt='mostafid' style='height: 60px'>";
        $msg .="<h6> Welcome to Nowopen </h6>";
        $msg .= "<p>Dear ". $user->$name." </p>";
        $msg .= "<p>Thank you for taking your time to register, we can't wait to work with you </p>";
        $msg .= "<br>";
        $msg .= "<p>Kindly click on the button below to confirm your email address.</p>";
        $msg .="<button><a href=''>Comfirm Email Address</a>";
        $msg .="</button>";
        $msg .= "<p>NowOpen Team</p>";
        $msg .= "<br><br>";
        $msg .= "</body>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$domain.'/messages');
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => 'no-reply@nowopen.com.ng', 'to' => 'Aderojuolaayomide57@gmail.com', 'subject' => 'Nowopen Registration', 'html' => $msg));
    
            //Todo: Open up so emails can go through
        $result = curl_exec($ch);
    
        if(!$result) {
            return "Error: Mail Sending Failed.";
        }
        //echo $result;
        curl_close($ch);

    }


}
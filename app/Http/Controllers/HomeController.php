<?php

namespace App\Http\Controllers;

use App\Mail\Contacts;
use App\Mail\Registration;
use App\Models\comment;
use App\Models\companies;
use App\Models\education;
use App\Models\employment;

use App\Models\product;
use App\Models\User;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;




class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $url;
    public function __construct(UrlGenerator $url)
    {
        //$this->middleware('auth');
        //$this->middleware(['verified','guest']);
        $this->url = $url;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  
    public function index()
    {
       
        $companies = DB::table('companies')->inRandomOrder()->paginate(20);
        foreach ($companies as $company){
            $company->product = DB::table('products')->where('company_id',$company->id)->count();
        }
        $headers = DB::table('adverts')->where('type','Header')->paginate(10);
        $basics = DB::table('companies')->inRandomOrder()->where('best',1)->paginate(10);
        foreach ($basics as $basic){
            $basic->product = DB::table('products')->where('company_id',$basic->id)->count();
        }
        $premiums = DB::table('companies')->inRandomOrder()->where('feature',1)->paginate(10);
        foreach ($premiums as $premium){
            $premium->product = DB::table('products')->where('company_id',$premium->id)->count();
        }
        $products = DB::table('products')->inRandomOrder()->where('feature',1)->paginate(18);
        return view('home',['users' => $companies,'basics' => $basics,
            'premiums' => $premiums,'products' => $products,'headers' => $headers]);
    }




    public function getMyAccount()
    {
        return view('myaccount');
    }
    
    
    public function getProfile($id = null)
    {
        switch($id){
            case (is_numeric($id)):
                $id = $id;
                break;
            case (is_string($id)):
                $id = DB::table('companies')->where('company_name', $id)->value('id');
                break;
            default:
                $id = null;
                break;
        }

        if(!empty(Auth::user()->id) And ($id == null)){
            $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            if(!empty($company)){
                return view('profile',['company' => $company]);
            }else{
                return redirect('regcompany');
            }
        }elseif(!empty(Auth::user()->id) And $id != null){
            $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            if (empty(Auth::user()->id) And ($company->id == $id)){
                $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            }else{
                $company = DB::table('companies')->where('id',$id)->first();
            }
            return view('profile',['company' => $company]);
        }elseif(($id != null) And empty(Auth::user()->id)){
            $company = DB::table('companies')->where('id',$id)->first();
            return view('profile',['company' => $company]);
        }
    }

    public function getProfileByName($name)
    {

        
        if(!empty(Auth::user()->id) And ($companyid == null)){
            $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            if(!empty($company)){
                return view('profile',['company' => $company]);
            }else{
                return redirect('regcompany');
            }
        }elseif(!empty(Auth::user()->id) And $companyid != null){
            $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            if (empty(Auth::user()->id) And ($company->id == $companyid)){
                $company = DB::table('companies')->where('user_id',Auth::user()->id)->first();
            }else{
                $company = DB::table('companies')->where('id',$companyid)->first();
            }
            return view('profile',['company' => $company]);
        }elseif(($companyid != null) And empty(Auth::user()->id)){
            $company = DB::table('companies')->where('id',$companyid)->first();
            return view('profile',['company' => $company]);
        } 
    }

    public function getProfilePicture()
    {
        $user = DB::table('users')->where('id',Auth::user()->id)->first();
        return view('profile-picture',['user' => $user]);
    }

    public function getRegisterCompany()
    {
        $sub = DB::table('subcategories')->get();
        $cat = DB::table('company_categories')->get();
        return view('register-company',['subs' => $sub,'cats' => $cat]);
    }

    public function SaveCompany(Request $request)
    {
        $this->validate($request,[
            'company_name' => 'required|max:191',
            'email' => 'required|max:191',
            'start_date' => 'required|max:191',
            'mobile_no' => 'required|max:191',
            'website' => 'required|max:191',
            'fax' => 'required|max:191',
            'address' => 'required|max:191',
            'country' => 'required|max:191',
            'state' => 'required|max:191',
            'city' => 'required|max:191',
            'about' => 'required|max:191',
            'category' => 'required|max:191',
            'opening' => 'required|max:191',
            'employees' => 'required|max:191',
            'manager' => 'required|max:191',
            'type' => 'required|max:191',
            'closing' => 'required|max:191',

        ]);

        $id = 1 + DB::table('companies')->max('id');

        $companies = new companies();
        $companies->company_name = $request['company_name'];
        $companies->user_id = Auth::user()->id;
        $companies->email = $request['email'];
        $companies->rcnumber = $request['rcnumber'];
        $companies->start_date = $request['start_date'];
        $companies->mobile_no = $request['mobile_no'];
        $companies->website = $request['website'];
        $companies->fax = $request['fax'];
        $companies->address = $request['address'];
        $companies->country = $request['country'];
        $companies->state = $request['state'];
        $companies->city = $request['city'];
        $companies->about = $request['about'];
        $companies->category = $request['category'];
        $companies->opening = $request['opening'];
        $companies->employees = $request['employees'];
        $companies->manager = $request['manager'];
        $companies->type = $request['type'];
        $companies->closing = $request['closing'];
        $companies->facebook = $request['facebook'];
        $companies->twitter = $request['twitter'];
        $companies->instagram = $request['instagram'];
        $companies->youtube = $request['youtube'];
        if($companies->save()){

            Session::flash('companysuccess', 'Record has been successfully inserted');

            $company = ['company_name' => $request['company_name'], 'name' => Auth::user()->name];

            Mail::to(Auth::user()->email)->send(new Registration((object)$comp));

            $company = DB::table('companies')->where('id',$id)->first();

            return view('profile',['company' => $company]);
        }
        Session::flash('companyfailed', 'Failed');
        return redirect()->back();
    }

    public function UpdateCompany($id,Request $request)
    {
        $this->validate($request,[
            'company_name' => 'required|max:191',
            'email' => 'required|max:191',
            'start_date' => 'required|max:191',
            'mobile_no' => 'required|max:191',
            'website' => 'required|max:191',
            'fax' => 'required|max:191',
            'address' => 'required|max:191',
            'country' => 'required|max:191',
            'state' => 'required|max:191',
            'city' => 'required|max:191',
            'about' => 'required|max:191',
            'category' => 'required|max:191',
            'opening' => 'required|max:191',
            'employees' => 'required|max:191',
            'manager' => 'required|max:191',
            'type' => 'required|max:191',
            'closing' => 'required|max:191',
        ]);

        $update = DB::table('companies')
            ->where('id',$id)
            ->update(['company_name' => $request['company_name'],
                'email' => $request['email'],
                'rcnumber' => $request['rcnumber'],
                'start_date' => $request['start_date'],
                'mobile_no' => $request['mobile_no'],
                'website' => $request['website'],
                'fax' => $request['fax'],
                'address' => $request['address'],
                'country' => $request['country'],
                'state' => $request['state'],
                'city' => $request['city'],
                'about' => $request['about'],
                'category' => $request['category'],
                'opening' => $request['opening'],
                'employees' => $request['employees'],
                'manager' => $request['manager'],
                'type' => $request['type'],
                'closing' => $request['closing'],
                'facebook' => $request['facebook'],
                'twitter' => $request['closing'],
                'instagram' => $request['instagram'],
                'youtube' => $request['youtube'],
                'spotify' => $request['spotify'],
            ]);


        if($update) {

            Session::flash('companyupdatesuccess', 'Your Account has been successfully Updated');
            return redirect()->back();

        }
        Session::flash('companyupdatefailed', 'Failed');
        return redirect()->back();
    }

    public function getEducationAndEmployment($id)
    {
        $company = DB::table('companies')->where('id',$id)->first();

        $employment = DB::table('employments')->where('user_id',$company->user_id)->first();
        $education = DB::table('education')->where('user_id',$company->user_id)->first();

        return view('education-and-employment',['company' => $company,'employment' => $employment,'education' => $education]);
    }

    function PostSaveImage($pic){
        $picturekey = $this->generateRandomString(10);
        $filename = $picturekey .'-'. $picturekey.'.jpg';
        $file = $pic;
        if($file){
            Storage::disk('local')->put($filename, File::get($file));
            return  $filename;
        }else{
            return '';
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkstate($elements)
    {
        if($elements){
            return 1;
        }else{
            return 0;
        }
    }

    public function Listsubcategory(Request $request)
    {
        $cat = DB::table('company_categories')->where('name',$request->category)->first();
        $sub = DB::table('subcategories')->where('cat_id',$cat->id)->get();

        return $sub;
    }

    public function Listsubcategory2(Request $request)
    {
        $cat = DB::table('company_categories')->where('name',$request->category)->first();
        $sub = DB::table('subcategories')->where('cat_id',$cat->id)->get();

        return $sub;
    }

    public function ListState(Request $request)
    {
        $country = DB::table('countries')->where('name',$request->country)->first();
        $state = DB::table('states')->where('country_id',$country->id)->get();

        return $state;
    }

    public function ListCity(Request $request)
    {
        $state = DB::table('states')->where('name',$request->state)->first();
        $city = DB::table('local_governments')->where('state_id',$state->id)->get();

        return $city;
    }

    public function UploadCompanyPicture(Request $request)
    {
        $filename = 'profile'.'_0'.$request->id;
        $filelocation = 'uploads/picture/';
        $newfile = $this->save_base64_image($request->image, $filename, $filelocation );

        if($newfile){
            DB::table('companies')
                ->where('id',$request->id)
                ->update(['logo' => $newfile]);
            return 'Picture Upload Successful';
        }
        return 'Picture Upload Failed';
    }
    public function UploadCompanyHeader(Request $request)
    {
        $filename = 'header'.'_0'.$request->id;
        $filelocation = 'uploads/header/';
        $newfile = $this->save_base64_image($request->image, $filename, $filelocation );

        if($newfile){
            DB::table('companies')
                ->where('id',$request->id)
                ->update(['header' => $newfile]);
            return 'Picture Upload Successful';
        }
        return 'Picture Upload Failed';
    }

    public function getProfileProduct($id)
    {
        $company = DB::table('companies')->where('id',$id)->first();
        $products = DB::table('products')->where('company_id',$id)->paginate(15);

        return view('profile-product',['company' => $company,'products' => $products]);

    }

    public function getAddProduct($id)
    {
        $company = DB::table('companies')->where('id',$id)->first();

        return view('add-product',['company' => $company]);
    }

    public function SaveProduct(Request $request,$id)
    {
        $this->validate($request,[
            'name' => 'required|max:191',
            'price' => 'required|max:191',
            'category' => 'required|max:191',
            'description' => 'required|max:191',
            'logo' => 'required|max:191',
        ]);


        $product = new product();
        $product->name = $request['name'];
        $product->company_id = $id;
        $product->price = $request['price'];
        $product->category = $request['category'];
        $product->description = $request['description'];
        $product->logo = $this->PostSaveImage($request['logo']);
        if($product->save()){

            Session::flash('productsuccess', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('productfailed', 'Failed');
        return redirect()->back();
    }

    public function getResponseImage($filename)
    {
        $file = Storage::disk('local')->get($filename);
        return new Response($file);
    }

    public function getCompany($id)
    {


        $companies = DB::table('companies')->where('category',$id)
            ->orWhere('type', $id)
            ->orWhere('state', $id)
            ->orWhere('city', $id)
            ->paginate(12);

        foreach ($companies as $company){
            $company->product = DB::table('products')->where('company_id',$company->id)->count();
        }


        return view('company-list',['companys' => $companies,'category' => $id]);
    }

    public function getSettings($id)
    {
        $company = DB::table('companies')->where('id',$id)->first();

        return view('settings',['company' => $company]);
    }

    public function getProductDetails($id)
    {

        $product = DB::table('products')->where('id',$id)->first();
        $company = DB::table('companies')->where('id',$product->company_id)->first();
        $comments = DB::table('comments')->where('product_id',$id)->paginate(15);
        $products = DB::table('products')->inRandomOrder()->where('category',$product->category)->paginate(10);

        return view('product-details',['product' => $product,'products' => $products,
            'comments' => $comments,'company' => $company]);
    }

    public function SaveEducation(Request $request,$id)
    {
        $this->validate($request,[
            'school' => 'required|max:191',
            'period' => 'required|max:191',
            'description' => 'required|max:191',
        ]);


        $education = new education();
        $education->user_id = $id;
        $education->school1 = $request['school'];
        $education->period1 = $request['period'];
        $education->about1 = $request['description'];
        $education->school2 = $request['school2'];
        $education->period2 = $request['period2'];
        $education->about2 = $request['description2'];
        if($education->save()){

            Session::flash('educationsuccess', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('educationfailed', 'Failed');
        return redirect()->back();
    }

    public function UpdateEducation(Request $request,$id)
    {
        $this->validate($request,[
            'school' => 'required|max:191',
            'period' => 'required|max:191',
            'description' => 'required|max:191',
        ]);


        $update = DB::table('education')
            ->where('id',$id)
            ->update(['school1' => $request['school'],
                'period1' => $request['period'],
                'about1' => $request['description'],
                'school2' => $request['school2'],
                'period2' => $request['period2'],
                'about2' => $request['description2'],
            ]);


        if($update) {

            Session::flash('educationupdatesuccess', 'Your Account has been successfully Updated');
            return redirect()->back();

        }
        Session::flash('educationupdatefailed', 'Failed');
        return redirect()->back();
    }

    public function SaveJob(Request $request,$id)
    {
        $this->validate($request,[
            'job' => 'required|max:191',
            'jobperiod' => 'required|max:191',
            'jobdescription' => 'required|max:191',
        ]);

        $employment = new employment();
        $employment->user_id = $id;
        $employment->employment1 = $request['job'];
        $employment->period1 = $request['jobperiod'];
        $employment->about1 = $request['jobdescription'];
        $employment->employment2 = $request['job2'];
        $employment->period2 = $request['jobperiod2'];
        $employment->about2 = $request['jobdescription2'];
        if($employment->save()){

            Session::flash('jobsuccess', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('jobfailed', 'Failed');
        return redirect()->back();
    }

    public function UpdateJob(Request $request,$id)
    {
        $this->validate($request,[
            'job' => 'required|max:191',
            'jobperiod' => 'required|max:191',
            'jobdescription' => 'required|max:191',
        ]);

        $update = DB::table('employments')
            ->where('id',$id)
            ->update(['employment1' => $request['job'],
                'period1' => $request['jobperiod'],
                'about1' => $request['jobdescription'],
                'employment2' => $request['job2'],
                'period2' => $request['period2'],
                'about2' => $request['description2'],
            ]);


        if($update) {

            Session::flash('employmentupdatesuccess', 'Your Account has been successfully Updated');
            return redirect()->back();

        }
        Session::flash('employmentupdatefailed', 'Failed');
        return redirect()->back();
    }

    public function ChangePassword($id)
    {

        $company = DB::table('companies')->where('id',$id)->first();

        return view('changepassword',['company' => $company]);
    }

    public function UpdateChangePassword(Request $request,$id)
    {
        $this->validate($request,[
            'oldpassword' => 'required|max:191',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = DB::table('users')->where('id',$id)->first();

        if(hash::check($request['oldpassword'], $user->password))
        {
            $update = DB::table('users')
                ->where('id',$id)
                ->update(['password' => hash::make($request['password']),
                ]);
            if($update) {

                Session::flash('passwordsuccess', 'Your password has been successfully Updated');
                return redirect()->back();

            }
            Session::flash('passwordupdatefailed', 'Failed');
        }
        Session::flash('passwordfail', 'Wrong password');
        return redirect()->back();
    }

    public function getAccount()
    {
        $user = DB::table('users')->where('id',Auth::user()->id)->first();

        return view('user-profile',['user' => $user]);
    }

    public function UpdateAccount(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'last_name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'dob' => 'required|max:191',
            'gender' => 'required|string|max:191',
        ]);

        $update = DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['name' => $request['name'],
                'last_name' => $request['last_name'],
                'email' => $request['email'],
                'dob' => $request['dob'],
                'gender' => $request['gender'],
            ]);


        if($update) {

            Session::flash('userupdatesuccess', 'Your Account has been successfully Updated');
            return redirect()->back();

        }
        Session::flash('userupdatefailed', 'Failed');
        return redirect()->back();
    }

    public function getSearch(Request $request)
    {

        $companies = DB::table('companies')->where('category', 'LIKE','%'.$request['category'].'%' )
            ->orWhere('type', $request['type'])
            ->orWhere('city', $request['location'])
            ->orWhere('state', $request['location'])
            ->orWhere('address', $request['location'])
            ->paginate(10);

        foreach ($companies as $company){
            $company->product = DB::table('products')->where('company_id',$company->id)->count();
        }

        return View('search',['companys' => $companies]);
    }

    public function getSearch2(Request $request,$search)
    {

        $companies = DB::table('companies')->where('company_name', 'LIKE','%'.$search.'%' )
            ->orWhere('category', $request['search'])
            ->orWhere('type', $search)
            ->orWhere('city', $search)
            ->orWhere('state', $search)
            ->orWhere('address', $search)
            ->paginate(10);

        foreach ($companies as $company){
            $company->product = DB::table('products')->where('company_id',$company->id)->count();
        }

        return View('search',['companys' => $companies]);
    }

    public function getProductSearch(Request $request,$search)
    {
        $products = DB::table('products')->where('name', 'LIKE','%'.$search.'%' )
            ->orWhere('category', $search)
            ->paginate(10);

        return View('product-search',['products' => $products,]);
    }

    public function getAllSearch(Request $request)
    {

        $company = DB::table('companies')->where('company_name', 'LIKE','%'.$request['search'].'%' )
            ->orWhere('category', $request['search'])
            ->orWhere('type', $request['search'])
            ->orWhere('city', $request['search'])
            ->orWhere('state', $request['search'])
            ->orWhere('address', $request['search'])
            ->count();

        $products = DB::table('products')->where('name', 'LIKE','%'.$request['search'].'%' )
            ->orWhere('category', $request['search'])
            ->count();

        return View('general-search',['companys' => $company,'products' => $products,'search' => $request['search']]);
    }

    public function homeSearch(Request $request)
    {

        $companies = DB::table('companies')->where('category', Input::get('category'))
            ->orWhere('type', Input::get('service'))
            //->orWhere('type', $request['search'])
            //->orWhere('city', $request['search'])
            ->orWhere('state', Input::get('state'))
            //->orWhere('address', $request['search'])
            ->get();
            //->count();

        foreach($companies as $company){
            $company->url = $this->url->to('/myprofile/'.$company->id);
            $company->image = $this->url->to('/uploads/header/'.$company->header);
        }   
        return $companies;
    }

    public function getContact()
    {
        return view('contact');
    }

    public function contact(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:191',
            'email' => 'required|string|email|max:255',
            'message' => 'required',
            'subject' => 'required',
        ]);

        $comp = ['name' => $request['name'], 'subject' => $request['subject'],'email' => $request['email'], 'message' => $request['message']];

        
        //Mail::to(Auth::user()->email)->send(new Registration((object)$comp));
        Mail::to('Aderojuolaayomide57@gmail.com')->send(new Contacts((object)$comp));
        

        return redirect()->back();
    }

    public function getSaveComment(Request $request,$id)
    {
        $this->validate($request,[
            'message' => 'required',
        ]);

        $comment = new comment();
        $comment->name = $request['name'];
        $comment->product_id = $id;
        $comment->email = $request['email'];;
        $comment->message = $request['message'];

        if($comment->save()){

            Session::flash('commentsuccess', 'Comment successfully sent');
            return redirect()->back();
        }
        Session::flash('commentfailed', 'Failed');
        return redirect()->back();
    }

    public function UltimateDelete($id,$database)
    {
        if($database == 'products')
        {
            $products = DB::table('products')->where('id', $id)->first();
            if($products)
            {

                $this->DeleteImage($products->logo);

                DB::table('products')->where('id', $id)->delete();
                DB::table('comments')->where('product_id', $id)->delete();


                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }else{
            $company = DB::table('companies')->where('id', $id)->first();
            $products = DB::table('products')->where('company_id', $id)->get();
            if($company)
            {
                if($products){
                    foreach($products as $product)
                    {
                        $this->DeleteImage($product->logo);
                        DB::table('comments')->where('product_id', $product->id)->delete();
                    }
                    DB::table('products')->where('company_id', $id)->delete();
                }


                $this->DeleteUserImage($company->logo,'picture/');
                $this->DeleteUserImage($company->header,'header/');

                DB::table('companies')->where('id', $id)->delete();


                Session::flash('deletesuccess', 'Record has been Deleted');
                return view('home');
            }
        }

        return redirect()->back();
    }

    public function getTest()
    {
        $companies = DB::table('companies')->inRandomOrder()->paginate(20);
        foreach ($companies as $company){
            $company->product = DB::table('products')->where('company_id',$company->id)->count();
        }
        $headers = DB::table('adverts')->where('type','Header')->paginate(10);
        $basics = DB::table('companies')->inRandomOrder()->where('best',1)->paginate(10);
        foreach ($basics as $basic){
            $basic->product = DB::table('products')->where('company_id',$basic->id)->count();
        }
        $premiums = DB::table('companies')->inRandomOrder()->where('feature',1)->paginate(10);
        foreach ($premiums as $premium){
            $premium->product = DB::table('products')->where('company_id',$premium->id)->count();
        }
        $products = DB::table('products')->inRandomOrder()->where('feature',1)->paginate(18);
        return view('test',['users' => $companies,'basics' => $basics,
            'premiums' => $premiums,'products' => $products,'headers' => $headers]);
    }
    public function getCategories()
    {
        return View('categories');
    }

    public function getServices($id)
    {
        $services = DB::table('services')->where('id',$id)->paginate(50);
        $company = DB::table('companies')->where('id',$id)->first();


        return view('services',['services' => $services,'company' => $company]);
    }

    public function getAddServices($id)
    {
        $company = DB::table('companies')->where('id',$id)->first();

        return view('add-services',['company' => $company]);
    }

    public function getServiceDetails($id)
    {

        $service = DB::table('services')->where('id',$id)->first();
        $company = DB::table('companies')->where('id',$service->company_id)->first();
        //$comments = DB::table('comments')->where('product_id',$id)->paginate(15);
        $services = DB::table('services')->inRandomOrder()->where('category',$service->category)->paginate(10);

        return view('service-details',['service' => $service,'services' => $services, 'company' => $company]);
    }

    public function getLocation()
    {
        return view('location');
    }

    public function SaveServices(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required|max:191',
            'type' => 'required|max:191',
            'category' => 'required|max:191',
            'description' => 'required|max:191',
            'picture' => 'required|max:191',
        ]);

        $result = DB::table('services')->insert([
            'company_id' => $id,
            'title' => $request['title'],
            'type' => $request['type'],
            'category' => $request['category'],
            'picture' => $this->PostSaveImage($request['picture']),
            'description' => $request['description'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        if($result){

            Session::flash('servicesuccess', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('servicefailed', 'Failed');
        return redirect()->back();
    }

    public function DeleteUserImage($filename,$type){
        $filepath = 'uploads/'.$type.$filename;
        if(glob($filepath)){
            unlink($filepath);
        }
    }

    public function DeleteImage($filename)
    {
        if(Storage::disk('local')->has($filename)){
            Storage::delete($filename);
        }
    }

    function save_base64_image($base64_image_string, $output_file_without_extentnion, $path_with_end_slash="" ) {
        //usage:  if( substr( $img_src, 0, 5 ) === "data:" ) {  $filename=save_base64_image($base64_image_string, $output_file_without_extentnion, getcwd() . "/application/assets/pins/$user_id/"); }
        //
        //data is like:    data:image/png;base64,asdfasdfasdf
        $output_file_with_extentnion='';
        $splited = explode(',', substr( $base64_image_string , 5 ) , 2);
        $mime=$splited[0];
        $data=$splited[1];

        $mime_split_without_base64=explode(';', $mime,2);
        $mime_split=explode('/', $mime_split_without_base64[0],2);
        if(count($mime_split)==2)
        {
            $extension=$mime_split[1];
            if($extension=='jpeg')$extension='jpg';
            //if($extension=='javascript')$extension='js';
            //if($extension=='text')$extension='txt';
            $output_file_with_extentnion.=$output_file_without_extentnion.'.'.$extension;
        }
        file_put_contents( $path_with_end_slash . $output_file_with_extentnion, base64_decode($data) );
        return $output_file_with_extentnion;
    }

    public function sendmail()
    {
        /* $this->validate($request,[
            'name' => 'required|max:191',
            'email' => 'required|string|email|max:255',
            'message' => 'required',
            'subject' => 'required',
        ]); */

        $comp = ['name' => 'name', 'email' => 'Aderojuolaayomide57@gmail.com', 'message' => 'Aderojuolaayomide57@gmail.com'];

        
        //Mail::to(Auth::user()->email)->send(new Registration((object)$comp));
        Mail::to('Aderojuolaayomide57@gmail.com')->send(new Contact((object)$comp));
        

        echo "done";
        //return redirect()->back();
    }

    public function contact2()
    {

        $key = 'a83cbf8f1bb72bedec3063de4c1998cb-7bbbcb78-a2febff4';
        $domain= 'www.nowopen.com.ng';
        $msg = "Hello"."<br>";
        $msg .="<p>i just took the covid-19 Digital Test and here is the result.</p>";
        $msg .="<p>What should i do next ?</p>";
        $msg .="<p> See my details </p>";
        $msg .="<p> Thank You </p>";
        

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$domain.'/messages');
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => 'Aderojuolaayomide57@gmail.com', 'to' => 'Aderojuolalekan57@gmail.com', 'subject' => 'Covid19 Test', 'html' => $msg));
    
            //Todo: Open up so emails can go through
        $result = curl_exec($ch);
    
        if(!$result) {
            return "Error: Mail Sending Failed.";
        }
        //echo $result;
        curl_close($ch);
    }

    

}

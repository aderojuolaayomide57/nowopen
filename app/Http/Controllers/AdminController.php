<?php

namespace App\Http\Controllers;

use App\Models\admins;
use App\Models\advert;
use App\Models\company_category;
use App\Models\subcategory;
use Illuminate\Auth\Recaller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getadmin()
    {
        if (session()->has('adminname')) {
            return View('admin');
        }
        return redirect("/adminlogin");
    }

    public function adminRegistration(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required'
        ]);

        $admin = new admins();
        $admin->name = $request['name'];
        $admin->email = $request['email'];
        $admin->password = hash::make($request['password']);

        if ($admin->save()) {
            Session::flash('Adminaccountcreated', 'Account has been successfully created');
            return redirect()->back();
        }
    }

    public function getadminReg()
    {
        if(session()->has('adminname')){
            return view('adminregistration');
        }
        return redirect("/adminlogin");
    }

    public function getadminLogin()
    {
        return view('adminlogin');

    }

    public function Loginadmin(Request $request)
    {
        $this->validate($request, [
            'adminemails' => 'required|email',
            'adminpasswords' => 'required'
        ]);

        $email = $request['adminemails'];

        $admin = DB::table('admins')->where('email', $email)->first();


        if ($admin) {
            $pass = hash::check($request['adminpasswords'], $admin->password);
            if ($pass) {
                session(['adminname' => $admin->name]);
                session(['levels' => $admin->level]);
                return redirect("/admins");
            } else {
                Session::flash('Adminerror', 'Email or Password is wrong, please try again');
                return redirect()->back();
            }
        }
        Session::flash('Adminerror', 'Email does not exist, please try again');
        return redirect()->back();

    }

    public function getViewAllUser()
    {
        $users = DB::table('users')->paginate(50);
        return View('View-all-user',['users' => $users]);
    }

    public function getViewAllAdmin()
    {
        $admins = DB::table('admins')->paginate(50);

        return View('View-all-admin',['admins' => $admins]);
    }

    public function getViewAllCompany()
    {
        $companies = DB::table('companies')->paginate(50);
         foreach($companies as $company)
         {
             $company->product= DB::table('products')->where('company_id',$company->id)->count();
             $company->service= DB::table('services')->where('company_id',$company->id)->count();
         }

        return View('view-all-company',['companies' => $companies]);
    }

    public function getViewOneCompany($id)
    {
        $company = DB::table('companies')->where('id', $id)->first();

        return View('view-one-company',['company' => $company]);
    }

    public function getCategory()
    {
        return View('add-category');
    }

    public function getSubCategory()
    {
        $cats = DB::table('company_categories')->get();
        return View('add-subcategory',['cats' => $cats]);
    }

    public function SaveCategory(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'logo' => 'required',
        ]);

        $company_category = new company_category();
        $company_category->name = $request['name'];
        $company_category->logo = $this->PostSaveImage($request['logo'],10);
        if($company_category->save()){

            Session::flash('companycategory', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('companyfailed', 'Failed');
        return redirect()->back();
    }

    public function SaveSubCategory(Request $request)
    {
        $this->validate($request,[
            'cat_id' => 'required',
            'name' => 'required',
        ]);

        $subcategory = new subcategory();
        $subcategory->cat_id = $request['cat_id'];
        $subcategory->name = $request['name'];
        if($subcategory->save()){

            Session::flash('subcatsuccess', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('subcatfailed', 'Failed');
        return redirect()->back();
    }

    public function getAddAdvert()
    {
        return view('add-advert');
    }

    public function getSaveAdvert(Request $request)
    {
        $this->validate($request,[
            'sponsor' => 'required',
            'type' => 'required',
            'page' => 'required',
            'image' => 'required',
        ]);

        $advert = new advert();
        $advert->sponsor = $request['sponsor'];
        $advert->page = $request['page'];
        $advert->type = $request['type'];
        $advert->picture = $this->PostSaveImage($request['image'],12);
        if($advert->save()){

            Session::flash('saveadverts', 'Record has been successfully inserted');
            return redirect()->back();
        }
        Session::flash('advertfail', 'Failed');
        return redirect()->back();
    }

    public function getAllProduct($id)
    {
        $products = DB::table('products')->where('company_id', $id)->paginate(25);
        foreach($products as $product)
        {
            $product->comment= DB::table('comments')->where('product_id',$product->id)->count();
        }
        return view('view-all-product',['products' => $products]);
    }

    public function getAllProductComment($id)
    {
        $comments = DB::table('comments')->where('product_id', $id)->paginate(25);

        return view('view-all-comments',['comments' => $comments]);
    }

    public function getAllServices($id)
    {
        $services = DB::table('services')->where('company_id', $id)->paginate(25);

        return view('view-all-services',['services' => $services]);
    }

    public function getAllSubCategory($id)
    {
        $subcats = DB::table('subcategories')->where('cat_id', $id)->paginate(25);

        return view('view-all-subcategory',['subcategorys' => $subcats]);
    }

    public function getAllCategory()
    {
        return view('view-all-category');
    }
    public function getAllAdvert()
    {
        $adverts = DB::table('adverts')->paginate(15);

        return view('view-all-advert',['adverts' => $adverts]);
    }

    function PostSaveImage($pic,$size){
        $picturekey = $this->generateRandomString($size);
        $filename = $picturekey .'-'. $picturekey.'.jpg';
        $file = $pic;
        if($file){
            Storage::disk('local')->put($filename, File::get($file));
            return  $filename;
        }else{
            return '';
        }
    }

    public function UltimateDelete($id,$database)
    {

        if($database == 'products')
        {
            $products = DB::table('products')->where('id', $id)->first();
            if($products)
            {

                $this->DeleteImage($products->logo);

                DB::table('products')->where('id', $id)->delete();
                DB::table('comments')->where('product_id', $id)->delete();


                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }elseif($database == 'admins')
        {
            DB::table('admins')->where('id', $id)->delete();

            Session::flash('deletesuccess', 'Record has been Deleted');
            return redirect()->back();
        }
        elseif($database == 'users')
        {

            $companys = DB::table('companies')->where('user_id', $id)->get();

            if($companys)
            {
                foreach ($companys as $company)
                {
                    $products = DB::table('products')->where('company_id', $company->id)->get();
                    if($products){
                        foreach($products as $product)
                        {
                            $this->DeleteImage($product->logo);
                            DB::table('comments')->where('product_id', $product->id)->delete();
                        }
                        DB::table('products')->where('company_id', $company->id)->delete();

                    }

                    $this->DeleteUserImage($company->logo,'picture/');
                    $this->DeleteUserImage($company->header,'header/');

                    DB::table('companies')->where('id', $id)->delete();
                }

                DB::table('users')->where('id', $id)->delete();

                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }

        }elseif($database == 'comments')
        {
            $comments = DB::table('comments')->where('id', $id)->first();
            if($comments)
            {
                DB::table('comments')->where('id', $id)->delete();

                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }elseif($database == 'company_categories')
        {
            $company_categories = DB::table('company_categories')->where('id', $id)->first();
            if($company_categories)
            {
                $this->DeleteImage($company_categories->logo);
                DB::table('company_categories')->where('id', $id)->delete();
                DB::table('subcategories')->where('cat_id', $id)->delete();

                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }elseif($database == 'subcategories')
        {
            $subcategories = DB::table('subcategories')->where('id', $id)->first();
            if($subcategories)
            {
                DB::table('subcategories')->where('id', $id)->delete();

                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }
        elseif($database == 'adverts')
        {
            $adverts = DB::table('adverts')->where('id', $id)->first();
            if($adverts)
            {

                $this->DeleteImage($adverts->logo);
                DB::table('adverts')->where('id', $id)->delete();


                Session::flash('deletesuccess', 'Record has been Deleted');
                return redirect()->back();
            }
        }
        else{
                $company = DB::table('companies')->where('id', $id)->first();
                $products = DB::table('products')->where('company_id', $id)->get();
                if($company)
                {
                    if($products){
                        foreach($products as $product)
                        {
                            $this->DeleteImage($product->logo);
                            DB::table('comments')->where('product_id', $product->id)->delete();
                        }
                        DB::table('products')->where('company_id', $id)->delete();
                    }


                    $this->DeleteUserImage($company->logo,'picture/');
                    $this->DeleteUserImage($company->header,'header/');

                    DB::table('companies')->where('id', $id)->delete();


                    Session::flash('deletesuccess', 'Record has been Deleted');
                    return redirect()->back();
                }
        }

        return redirect()->back();
    }

    public function DeActivateUser(Request $request)
    {
        if($request->database == 'products')
        {
            $users = DB::table('products')->where('id', $request->id)->first();

            if($users->status == 1){
                DB::table('products')->where('id', $request->id)->update(['status' => 0]);
                return 'success';
            }else{
                DB::table('products')->where('id', $request->id)->update(['status' => 1]);
                return 'failed';
            }
        }elseif ($request->database == 'adverts'){
            $users = DB::table('adverts')->where('id', $request->id)->first();

            if($users->active == 1){
                DB::table('adverts')->where('id', $request->id)->update(['active' => 0]);
                return 'success';
            }else{
                DB::table('adverts')->where('id', $request->id)->update(['active' => 1]);
                return 'failed';
            }
        }elseif ($request->database == 'comments'){
            $users = DB::table('comments')->where('id', $request->id)->first();

            if($users->active == 1){
                DB::table('comments')->where('id', $request->id)->update(['active' => 0]);
                return 'success';
            }else{
                DB::table('comments')->where('id', $request->id)->update(['active' => 1]);
                return 'failed';
            }
        }elseif ($request->database == 'companies'){
            $users = DB::table('companies')->where('id', $request->id)->first();

            if($users->active == 1){
                DB::table('companies')->where('id', $request->id)->update(['active' => 0]);
                return 'success';
            }else{
                DB::table('companies')->where('id', $request->id)->update(['active' => 1]);
                return 'failed';
            }
        }
        else{
            $users = DB::table('users')->where('id', $request->id)->first();

            if($users->active == 1){
                DB::table('users')->where('id', $request->id)->update(['active' => 0]);
                return 'success';
            }else{
                DB::table('users')->where('id', $request->id)->update(['active' => 1]);
                return 'failed';
            }
        }


    }

    public function DeActivateFeature(Request $request)
    {
        if($request->database == 'products')
        {
            $users = DB::table('products')->where('id', $request->id)->first();

            if($users->feature == 1){
                DB::table('products')->where('id', $request->id)->update(['feature' => 0]);
                return 'success';
            }else{
                DB::table('products')->where('id', $request->id)->update(['feature' => 1]);
                return 'failed';
            }
        }
        else{
            $users = DB::table('companies')->where('id', $request->id)->first();

            if($users->feature == 1){
                DB::table('companies')->where('id', $request->id)->update(['feature' => 0]);
                return 'success';
            }else{
                DB::table('companies')->where('id', $request->id)->update(['feature' => 1]);
                return 'failed';
            }
        }


    }

    public function DeActivateBest(Request $request)
    {

            $users = DB::table('companies')->where('id', $request->id)->first();

            if($users->best == 1){
                DB::table('companies')->where('id', $request->id)->update(['best' => 0]);
                return 'success';
            }else{
                DB::table('companies')->where('id', $request->id)->update(['best' => 1]);
                return 'failed';
            }

    }

    public function DeleteUserImage($filename,$type){
        $filepath = 'uploads/'.$type.$filename;
        if(glob($filepath)){
            unlink($filepath);
        }
    }

    public function DeleteImage($filename)
    {
        if(Storage::disk('local')->has($filename)){
            Storage::delete($filename);
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
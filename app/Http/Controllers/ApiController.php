<?php
/**
 * Created by PhpStorm.
 * User: Ayomide
 * Date: 6/23/2019
 * Time: 6:56 AM
 */

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;


class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
    */

    public function comp()
    {
        $comp = DB::table('apiusers')->get();

        return response()->json($comp,200);
    }

    public function staff()
    {
        $staff = DB::table('apistaff')->get();

        return response()->json($staff,200);
    }

    public function drugList()
    {
        $staff = DB::table('apidrug')->get();

        return response()->json($staff,200);
    }

    public function getSingleDrug($nafdac)
    {
        $drug = DB::table('apidrug')->where('nafdac',$nafdac)->first();

        return response()->json($drug,200);
    }

    public function getAllEgg()
    {
        $egg = DB::table('apiegg')->get();

        return response()->json($egg,200);
    }

    public function getAllChicken()
    {
        $chicken = DB::table('apichicken')->get();

        return response()->json($chicken,200);
    }

    public function getAllOrder()
    {
        $order = DB::table('apiorders')->get();

        return response()->json($order,200);
    }

    public function saveUSerInformation(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required',
                'password' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apiusers')->insert([
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'email' => $data['email'],
                        'password' => $data['password']]
                );
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
        }
        return response()->json(['message' => 'record not found'],200);
    }

    public function login(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'email' => 'required',
                'password' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
                $login = DB::table('apiusers')->where('email',$data['email'])->where('password', $data['password'])->first();

                if ($login) {
                    return response()->json(['message' => 'Login Successfully','user' => $login],200);

                }
            }
            return response()->json(['message' => 'invalid email or Password'], 300);
        }
        return response()->json(['message' => 'invalid email or Password'], 300);

    }
    public function saveStaffInformation(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required',
                'gender' => 'required',
                'address' => 'required',
                'role' => 'required',
                'phonenumber' => 'required',
                'salary' => 'required']);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apistaff')->insert([
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'email' => $data['email'],
                        'gender' => $data['gender'],
                        'address' => $data['address'],
                        'phonenumber' => $data['phonenumber'],
                        'role' => $data['role'],
                        'salary' => $data['salary']
                    ]);
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
            return response()->json(['message' => 'Please fill all the field'],200);
        }
        return response()->json(['message' => 'Record not found'],300);
    }

    public function saveDrug(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'name' => 'required',
                'price' => 'required',
                'manufactureby' => 'required',
                'manufacturedfor' => 'required',
                'batchnumber' => 'required',
                'nafdac' => 'required',
                'manufacturedate' => 'required',
                'expiredate' => 'required',
                'description' => 'required',
                'validatecode' => 'required',
                ]);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apidrug')->insert([
                    'name' => $data['name'],
                    'price' => $data['price'],
                    'manufactureby' => $data['manufactureby'],
                    'manufacturedfor' => $data['manufacturedfor'],
                    'nafdac' => $data['nafdac'],
                    'manufacturedate' => $data['manufacturedate'],
                    'expiredate' => $data['expiredate'],
                    'description' => $data['description'],
                    'batchnumber' => $data['batchnumber'],
                    'validatecode' => $data['validatecode']
                ]);
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
        }
        return response()->json(['message' => 'record not found'],200);
    }

    public function searchDrug(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'validatecode' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
                $drug = DB::table('apidrug')->where('validatecode',$data['validatecode'])->first();

                if ($drug) {
                    return response()->json(['message' => 'Search Succesful','drug' => $drug],200);
                }
            }
            return response()->json(['message' => 'Invalid Nafdac Number1'], 200);
        }
        return response()->json(['message' => 'Invalid Nafdac Number'], 200);

    }

    public function saveEgg(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, ['egg' => 'required']);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apiegg')->insert([
                    'total' => $data['egg'],
                    'date' => date('Y-m-d H:i:s'),
                ]);
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
        }
        return response()->json(['message' => 'record not found'],200);
    }

    public function saveChicken(Request $request)
    {

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'chicken' => 'required',
                'chicken_type' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apichicken')->insert([
                    'total' => $data['chicken'],
                    'chicken_type' => $data['chicken_type'],
                    'date' => date('Y-m-d H:i:s'),
                ]);
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
        }
        return response()->json(['message' => 'record not found'],200);
    }

    public function saveOrder(Request $request)
    {

        

        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  Validator::make($data, [
                'fullname' => 'required',
                'phone' => 'required',
                'type' => 'required',
                'quantity' => 'required',
                'price' => 'required',
                'date' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
                $result = DB::table('apiorders')->insert([
                    'name' => $data['fullname'],
                    'phone' => $data['phone'],
                    'type' => $data['type'],
                    'quantity' => $data['quantity'],
                    'price' => $data['price'],
                    'date' => $data['date'],
                    'chicken_type' => $data['chicken_type'],
                    'size' => $data['size'],
                    'layers' => $data['layers'],
                ]);
                if ($result) {
                    return response()->json(['message' => 'Saved Succesfully'],200);
                }
            }
        }
        return response()->json(['message' => 'record not found'],200);
    }

 public function DeleteRecord()
    {
        
        $req = Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();

        $data =  json_decode($data, true);
        if ($data) {
            $validator =  $v =  Validator::make($data, [
                'id' => 'required',
                'table' => 'required',
            ]);
            if (!$validator->fails() && !empty($data)) {
            
                switch($data['table'])
                {
                    case 'apistaff':
                            DB::table('apistaff')->where('id', $data['id'])->delete();
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;
                    case 'apiorders':
                            DB::table('apiorders')->where('id', $data['id'])->delete();
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;
                    case 'apiegg':
                            DB::table('apiorders')->where('id', $data['id'])->delete();
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;
                    case 'apichicken':
                            DB::table('apichicken')->where('id', $data['id'])->delete();
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;
                    case 'apidrug':
                            DB::table('apidrug')->where('id', $data['id'])->delete();
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;        
                    default :
                            return response()->json(['message' => 'Record Deleted succesfully'],200);
                            break;

                }
            }
            return response()->json(['message' => 'invalid email or Password'], 300);
        }
        return response()->json(['message' => 'invalid email or Password'], 300);
    }

    public function homeSearch(Request $request)
    {

        $req =  Request::Instance();
        $data = $req->getContent();
        //$data = $this->request->all();
        //dd($data);
        $data =  json_decode($data, true);
        $company = DB::table('companies')->where('category', $data['category'])
            ->orWhere('type', $data['service'])
            //->orWhere('type', $request['search'])
            //->orWhere('city', $request['search'])
            ->orWhere('state', $data['state'])->get();
            //->orWhere('address', $request['search'])
            //->count();

        return response()->json(['company' => $company],200);
    }

}

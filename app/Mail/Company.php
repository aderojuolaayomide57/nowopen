<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Company extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $comp;


    public function __construct($comp)
    {
        $this->comp = $comp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('NowOpen'.$this->comp->name)->view('emails.company')->with([
            'name'  => $this->comp->name,
            'company_name'  => $this->comp->company_name,
        ]);
    }
}

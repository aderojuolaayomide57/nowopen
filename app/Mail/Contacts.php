<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contacts extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Contacts
     */
    public $contacts;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->contacts->subject)->view('emails.contact')->with([
            'name'  => $this->contacts->name,
            'email'  => $this->contacts->email,
            'message'  => $this->contacts->message
        ]);
    }
}

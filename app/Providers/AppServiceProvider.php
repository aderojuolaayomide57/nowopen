<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Notifications\VerifyEmail;
use App\Mail\EmailVerification;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Override the email notification for verifying email
      /*   VerifyEmail::toMailUsing(function ($notifiable){
            $verifyUrl = URL::temporarySignedRoute(
                'verification.verify',
                Carbon::now()->addMinutes(60),
                ['id' => $notifiable->getKey()]
            );
            return new EmailVerification($verifyUrl, $notifiable);
        }); */



         view()->composer('*', function ($view)
        {
            $cats = DB::table('company_categories')->get();

            $country = DB::table('countries')->get();

            $local = DB::table('local_governments')->inRandomOrder()->paginate(18);
            $state = DB::table('states')->where('country_id',160)->get();

            $subcategory = DB::table('subcategories')->orderBy('cat_id', 'asc')->get();

            if(!empty(Auth::user()->id))
            {
                $company1 = DB::table('companies')->where('user_id',Auth::user()->id)->get();
            }else{
                $company1 = "";
            }


            //...with this variable
            $view->with(['cats'=> $cats,'company1s' => $company1,'subcats' => $subcategory,
                'countries' => $country,'locals' => $local,'states' => $state] );
        });
    }
}

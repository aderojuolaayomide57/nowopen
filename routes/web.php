<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Auth::routes(['verify' => true]);
Auth::routes(['resend' => true]);
Route::get('notify', 'NotifyController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/send', 'HomeController@contact')->name('send');

Route::get('/', function () {
    return view('welcome1');
});


Route::get('/createaccount',[
    'uses' => 'HomeController@getMyAccount',
    'as' => 'createaccount'
]);
Route::get('/categories',[
    'uses' => 'HomeController@getCategories',
    'as' => 'categories'
]);
Route::get('/location',[
    'uses' => 'HomeController@getLocation',
    'as' => 'location'
]);

Route::get('/myprofile/{id?}',[
    'uses' => 'HomeController@getProfile',
    'as' => 'myprofile'
]);

Route::get('/profile_picture',[
    'uses' => 'HomeController@getProfilePicture',
    'as' => 'profile_picture'
]);

Route::get('/regcompany',[
    'uses' => 'HomeController@getRegisterCompany',
    'as' => 'regcompany'
]);

Route::get('/admins',[
    'uses' => 'AdminController@getadmin',
    'as' => 'admins'
]);

Route::get('/adminlogin',[
    'uses' => 'AdminController@getadminLogin',
    'as' => 'adminlogin'
]);

Route::post('/loginadmin',[
    'uses' => 'AdminController@Loginadmin',
    'as' => 'loginadmin'
]);

//saveadmin
Route::post('/saveadmin',[
    'uses' => 'AdminController@adminRegistration',
    'as' => 'saveadmin'
]);


Route::get('/adminreg',[
    'uses' => 'AdminController@getadminReg',
    'as' => 'adminreg'
]);

Route::get('/category',[
    'uses' => 'AdminController@getCategory',
    'as' => 'category'
]);

Route::get('/subcategory',[
    'uses' => 'AdminController@getSubCategory',
    'as' => 'subcategory'
]);

Route::get('/viewalluser',[
    'uses' => 'AdminController@getViewAllUser',
    'as' => 'viewalluser'
]);

Route::get('/viewalladmin',[
    'uses' => 'AdminController@getViewAllAdmin',
    'as' => 'viewalladmin'
]);

Route::post('/savecategory',[
    'uses' => 'AdminController@SaveCategory',
    'as' => 'savecategory'
]);

Route::post('/savesubcategory',[
    'uses' => 'AdminController@SaveSubCategory',
    'as' => 'savesubcategory'
]);

Route::post('/savecompany',[
    'uses' => 'HomeController@SaveCompany',
    'as' => 'savecompany'
]);

Route::post('/updatecompany/{id}',[
    'uses' => 'HomeController@UpdateCompany',
    'as' => 'updatecompany'
]);

Route::post('/listsubcat',[
    'uses' => 'HomeController@Listsubcategory',
    'as' => 'listsubcat'
]);

Route::get('/listsubcat2',[
    'uses' => 'HomeController@Listsubcategory2',
    'as' => 'listsubcat2'
]);


Route::get('/liststate',[
    'uses' => 'HomeController@ListState',
    'as' => 'liststate'
]);

Route::get('/listcity',[
    'uses' => 'HomeController@ListCity',
    'as' => 'listcity'
]);

Route::get('/viewallcompany',[
    'uses' => 'AdminController@getViewAllCompany',
    'as' => 'viewallcompany'
]);
Route::get('/viewonecompany/{id}',[
    'uses' => 'AdminController@getViewOneCompany',
    'as' => 'viewonecompany'
]);

Route::post('/uploadcompanyimage',[
    'uses' => 'HomeController@UploadCompanyPicture',
    'as' => 'uploadcompanyimage'
]);
Route::post('/uploadcompanyheader',[
    'uses' => 'HomeController@UploadCompanyHeader',
    'as' => 'uploadcompanyheader'
]);

Route::get('/product/{id}',[
    'uses' => 'HomeController@getProfileProduct',
    'as' => 'product'
]);

Route::get('/addproduct/{id}',[
    'uses' => 'HomeController@getAddProduct',
    'as' => 'addproduct'
]);

Route::post('/saveproduct/{id}',[
    'uses' => 'HomeController@SaveProduct',
    'as' => 'saveproduct'
]);

Route::get('/company/{id}',[
    'uses' => 'HomeController@getCompany',
    'as' => 'company'
]);

Route::get('/settings/{id}',[
    'uses' => 'HomeController@getSettings',
    'as' => 'settings'
]);

Route::get('/educationemployment/{id}',[
    'uses' => 'HomeController@getEducationAndEmployment',
    'as' => 'educationemployment'
]);

Route::get('/productdetails/{id}',[
    'uses' => 'HomeController@getProductDetails',
    'as' => 'productdetails'
]);

Route::get('/AllImage/{filename}',[
    'uses' => 'HomeController@getResponseImage',
    'as' => 'AllImage'
]);

Route::post('/SaveEducation/{id}',[
    'uses' => 'HomeController@SaveEducation',
    'as' => 'SaveEducation'
]);

Route::post('/SaveJob/{id}',[
    'uses' => 'HomeController@SaveJob',
    'as' => 'SaveJob'
]);

Route::post('/updateEducation/{id}',[
    'uses' => 'HomeController@UpdateEducation',
    'as' => 'updateEducation'
]);

Route::post('/UpdateJob/{id}',[
    'uses' => 'HomeController@UpdateJob',
    'as' => 'UpdateJob'
]);

Route::get('/changepassword/{id}',[
    'uses' => 'HomeController@ChangePassword',
    'as' => 'changepassword'
]);

Route::post('/updatepassword/{id}',[
    'uses' => 'HomeController@UpdateChangePassword',
    'as' => 'updatepassword'
]);

Route::get('/myaccount',[
    'uses' => 'HomeController@getAccount',
    'as' => 'myaccount'
]);

Route::post('/updateaccount',[
    'uses' => 'HomeController@UpdateAccount',
    'as' => 'updateaccount'
]);

Route::get('/companysearch/{search}',[
    'uses' => 'HomeController@getSearch2',
    'as' => 'companysearch'
]);

Route::post('/homesearch',[
    'uses' => 'HomeController@homeSearch',
    'as' => 'homesearch'
]);

Route::get('/productsearch/{search}',[
    'uses' => 'HomeController@getProductSearch',
    'as' => 'productsearch'
]);

Route::get('/search',[
    'uses' => 'HomeController@getSearch',
    'as' => 'search'
]);

Route::get('/allsearch',[
    'uses' => 'HomeController@getAllSearch',
    'as' => 'allsearch'
]);

Route::get('/contact',[
    'uses' => 'HomeController@getContact',
    'as' => 'contact'
]);

Route::post('/sendmail',[
    'uses' => 'HomeController@contact',
    'as' => 'sendmail'
]);

Route::get('/addadvert',[
    'uses' => 'AdminController@getAddAdvert',
    'as' => 'addadvert'
]);

Route::get('/delete/{id}/{database}',[
    'uses' => 'HomeController@UltimateDelete',
    'as' => 'delete'
]);

Route::get('/ultimatedelete/{id}/{database}',[
    'uses' => 'AdminController@UltimateDelete',
    'as' => 'ultimatedelete'
]);

Route::post('/saveadvert',[
    'uses' => 'AdminController@getSaveAdvert',
    'as' => 'saveadvert'
]);

Route::post('/savecomment/{id}',[
    'uses' => 'HomeController@getSaveComment',
    'as' => 'savecomment'
]);
Route::get('/allproducts/{id}',[
    'uses' => 'AdminController@getAllProduct',
    'as' => 'allproducts'
]);
Route::get('/productcomment/{id}',[
    'uses' => 'AdminController@getAllProductComment',
    'as' => 'productcomment'
]);

Route::get('/viewallcategory',[
    'uses' => 'AdminController@getAllCategory',
    'as' => 'viewallcategory'
]);
Route::get('/viewallsubcat/{id}',[
    'uses' => 'AdminController@getAllSubCategory',
    'as' => 'viewallsubcat'
]);

Route::get('/viewalladvert',[
    'uses' => 'AdminController@getAllAdvert',
    'as' => 'viewalladvert'
]);
Route::get('/test',[
    'uses' => 'HomeController@getTest',
    'as' => 'test'
]);

Route::post('/disable',[
    'as'      => 'disable',
    'uses'    => 'AdminController@DeActivateUser'
]);

Route::post('/feature',[
    'as'      => 'feature',
    'uses'    => 'AdminController@DeActivateFeature'
]);

Route::post('/best',[
    'as'      => 'best',
    'uses'    => 'AdminController@DeActivateBest'
]);

Route::get('/services/{id}',[
    'uses' => 'HomeController@getServices',
    'as' => 'services'
]);

Route::get('/addservices/{id}',[
    'uses' => 'HomeController@getAddServices',
    'as' => 'addservices'
]);

Route::post('/saveservices/{id}',[
    'as'      => 'saveservices',
    'uses'    => 'HomeController@SaveServices'
]);

Route::get('/allservices/{id}',[
    'uses' => 'AdminController@getAllServices',
    'as' => 'services'
]);

Route::get('/servicedetails/{id}',[
    'uses' => 'HomeController@getServiceDetails',
    'as' => 'servicedetails'
]);

Route::get('/delete/{id}/{database}',[
    'uses' => 'HomeController@homeSearch',
    'as' => 'delete'
]);

Route::get('/send',[
    'as'      => 'send',
    'uses'    => 'HomeController@sendmail'
]);

